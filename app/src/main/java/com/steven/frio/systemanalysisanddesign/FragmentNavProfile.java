package com.steven.frio.systemanalysisanddesign;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.steven.frio.systemanalysisanddesign.configs.StringConfig;
import com.steven.frio.systemanalysisanddesign.interfaces.UpdateProfileInfoInterface;
import com.steven.frio.systemanalysisanddesign.sharedpreferences.SharedPreferencesManager;
import com.steven.frio.systemanalysisanddesign.volley.ProfileSettings;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentNavProfile extends Fragment {


    @BindView(R2.id.btn_profileUpdate)
    Button btn_profileUpdate;
    @BindView(R2.id.btn_profilePassword)
    Button btn_profilePassword;
    @BindView(R2.id.btn_profilePointing)
    Button btn_profilePointing;
    @BindView(R2.id.et_profile_fstName)
    EditText et_profile_fstName;
    @BindView(R2.id.et_profile_lstName)
    EditText et_profile_lstName;
    @BindView(R2.id.et_profile_mobileNum)
    EditText et_profile_mobileNum;
    @BindView(R2.id.et_profile_points)
    EditText et_profile_points;
    @BindView(R2.id.srl_navProfile)
    SwipeRefreshLayout srl_navProfile;

    String phone_number;

    SharedPreferences sharedPreferences;
    SharedPreferencesManager sharedPreferencesManager;

    UpdateProfileInfoInterface updateProfileInfoInterface;

    public FragmentNavProfile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_nav_profile, container, false);

        ButterKnife.setDebug(true);
        ButterKnife.bind(this, view);

        sharedPreferences = getContext().getSharedPreferences(StringConfig.SHAREDPREF_NAME, 0);
        sharedPreferencesManager = new SharedPreferencesManager(getContext());
        phone_number = sharedPreferences.getString("phone_number", null);

        final ProfileSettings profileSettings = new ProfileSettings(getContext());
        profileSettings.displayProfilePoints();

        displayProfileInfo();
        displayPointsToTextView();



        srl_navProfile.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                srl_navProfile.setRefreshing(true);

                profileSettings.displayProfilePoints();
                displayProfileInfo();
                displayPointsToTextView();

                srl_navProfile.setRefreshing(false);
            }
        });


        updateProfileInfoInterface = new UpdateProfileInfoInterface() {
            @Override
            public void updateDisplayedInfo(SharedPreferences sharedPreferences, String sharedPrefFstName, String sharedPrefLstName, String sharedPrefMobileNum) {
                Log.d(StringConfig.LOG_TAG, "fstName : " + sharedPrefFstName);
                Log.d(StringConfig.LOG_TAG, "lstName : " + sharedPrefLstName);
                Log.d(StringConfig.LOG_TAG, "mobNum : " + sharedPrefMobileNum);

                et_profile_fstName.setText(sharedPrefFstName);
                et_profile_lstName.setText(sharedPrefLstName);
                et_profile_mobileNum.setText(sharedPrefMobileNum);
            }
        };

        btn_profileUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (phone_number.startsWith("000000")) {
                    Toast.makeText(getContext(), "You are not allowed to modify the following.", Toast.LENGTH_SHORT).show();
                } else {
                    FragmentManager fragmentManager = getFragmentManager();
                    DialogFragmentProfile dialogFragmentProfile = new DialogFragmentProfile();
                    dialogFragmentProfile.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
                    dialogFragmentProfile.show(fragmentManager, "DialogFragmentProfileUpdate");
                }
            }
        });

        btn_profilePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (phone_number.startsWith("000000")) {
                    Toast.makeText(getContext(), "You are not allowed to modify the following.", Toast.LENGTH_SHORT).show();
                } else {
                    FragmentManager fragmentManager = getFragmentManager();
                    DialogFragmentPassword dialogFragmentPassword = new DialogFragmentPassword();
                    dialogFragmentPassword.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
                    dialogFragmentPassword.show(fragmentManager, "DialogFragmentPasswordUpdate");
                }

            }
        });

        btn_profilePointing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (phone_number.startsWith("000000")) {
                    Toast.makeText(getContext(), "You don't have the privilege to redeem an item.", Toast.LENGTH_SHORT).show();
                } else {
                    FragmentManager fragmentManager = getFragmentManager();

                    DialogFragmentRedeem dialogFragmentRedeem = new DialogFragmentRedeem();

                    Bundle bundle = new Bundle();
                    bundle.putString("points", et_profile_points.getText().toString());
                    dialogFragmentRedeem.setArguments(bundle);


                    dialogFragmentRedeem.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
                    dialogFragmentRedeem.show(fragmentManager, "DialogFragmentRedeem");
                }
            }
        });

        return view;
    }

    public void displayPointsToTextView(){
        if (sharedPreferencesManager.getSharedPrefMobileNumber() != null &&
                sharedPreferencesManager.getSharedPrefMobileNumber().startsWith("000000")) {

            et_profile_points.setText("Sign-up to get points.");

        } else{
            et_profile_points.setText(sharedPreferencesManager.getSharedPrefPoints().toString());

            //Log.d(StringConfig.LOG_TAG, "POINTS !!!" + sharedPreferencesManager.getSharedPrefPoints());
        }
    }

    public void displayProfileInfo() {


        if (phone_number.startsWith("00000")) {
            et_profile_fstName.setText("Guest");
            et_profile_lstName.setText("Guest");
            et_profile_mobileNum.setText("Guest");

        } else {
            et_profile_fstName.setText(sharedPreferencesManager.getSharedPrefFirstName());
            et_profile_lstName.setText(sharedPreferencesManager.getSharedPrefLastName());
            et_profile_mobileNum.setText(sharedPreferencesManager.getSharedPrefMobileNumber());
        }

    }
}
