package com.steven.frio.systemanalysisanddesign.recyclerparsers;

/**
 * Created by steven on 10/7/17.
 */

public class RParserCommentsSuggestions {
    private Integer id;
    private String comments;
    private Integer rating;

    public RParserCommentsSuggestions(Integer id, String comments, Integer rating) {
        this.id = id;
        this.comments = comments;
        this.rating = rating;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }
}
