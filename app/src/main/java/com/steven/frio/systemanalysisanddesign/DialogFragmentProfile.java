package com.steven.frio.systemanalysisanddesign;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.steven.frio.systemanalysisanddesign.configs.StringConfig;
import com.steven.frio.systemanalysisanddesign.interfaces.UpdateProfileInfoInterface;
import com.steven.frio.systemanalysisanddesign.volley.ProfileSettings;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class DialogFragmentProfile extends DialogFragment {

    @BindView(R2.id.til_updateMobile)
    TextInputLayout til_updateMobile;
    @BindView(R2.id.til_updateFirstName)
    TextInputLayout til_updateFirstName;
    @BindView(R2.id.til_updateLastName)
    TextInputLayout til_updateLastName;
    @BindView(R2.id.tet_updateMobile)
    TextInputEditText tet_updateMobile;
    @BindView(R2.id.tet_updateFirstName)
    TextInputEditText tet_updateFirstName;
    @BindView(R2.id.tet_updateLastName)
    TextInputEditText tet_updateLastName;
    @BindView(R2.id.btn_updateDismissProfile)
    Button btn_updateDismissProfile;
    @BindView(R2.id.btn_updateSubmitProfile)
    Button btn_updateSubmitProfile;
    String strUpdateFirstName, strUpdateLastName;
    SharedPreferences sharedPreferences;

    public DialogFragmentProfile() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.dialog_fragment_profile, container, false);

        ButterKnife.setDebug(true);
        ButterKnife.bind(this, view);

        getDialog().setTitle("Update Profile");

        sharedPreferences = getContext().getSharedPreferences(StringConfig.SHAREDPREF_NAME, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.apply();

        tet_updateMobile.setFocusable(false);


        tet_updateMobile.setText(sharedPreferences.getString("phone_number", null));

        btn_updateDismissProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        btn_updateSubmitProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strUpdateFirstName = tet_updateFirstName.getText().toString();
                strUpdateLastName = tet_updateLastName.getText().toString();

                if (!isUpdateProfileValid()) {
                    Toast.makeText(getContext(), "Please enter a valid name.", Toast.LENGTH_SHORT).show();
                } else {

                    ProfileSettings profileSettings = new ProfileSettings(getContext());
                    profileSettings.updateProfile(strUpdateFirstName, strUpdateLastName);
                    dismiss();

                }

            }
        });

        return view;

    }

    private boolean isUpdateProfileValid() {
        boolean valid = true;

        if (strUpdateFirstName.length() <= 1) {
            tet_updateFirstName.setError("Check your name");
            valid = false;
        }

        if (strUpdateLastName.length() == 0) {
            tet_updateLastName.setError("Check your name");
            valid = false;
        }

        return valid;
    }

    @Override
    public void dismiss() {
        super.dismiss();
        Log.d(StringConfig.LOG_TAG, "Dismissed called.");
    }

    @Override
    public void onStop() {
        super.onStop();

        Log.d(StringConfig.LOG_TAG, "onStop called");
        String sharedPrefFstName = sharedPreferences.getString("first_name", null);
        String sharedPrefLstName = sharedPreferences.getString("last_name", null);
        String sharedPrefMobileNum = sharedPreferences.getString("phone_number", null);


        //updateProfileInterface.updateDisplayedInfo(sharedPreferences, sharedPrefFstName, sharedPrefLstName, sharedPrefMobileNum);

    }

    interface UpdateProfileInterface extends UpdateProfileInfoInterface {

    }
}
