package com.steven.frio.systemanalysisanddesign;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.steven.frio.systemanalysisanddesign.sharedpreferences.SharedPreferencesManager;
import com.steven.frio.systemanalysisanddesign.volley.ProfileSettings;

public class LoginActivity extends AppCompatActivity {

    TextInputEditText tet_loginMobile, tet_loginPassword;
    TextInputLayout til_loginMobile, til_loginPassword;
    TextView tv_forgotPassword;
    ImageView iv_loginImage;
    Button btn_login, btn_guest;
    String strLoginMobile, strLoginPassword;
    RequestQueue requestQueue;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    SharedPreferencesManager sharedPreferencesManager;

    FrameLayout frameLayout;
    Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        tet_loginMobile = (TextInputEditText) findViewById(R.id.tet_loginMobile);
        tet_loginPassword = (TextInputEditText) findViewById(R.id.tet_loginPassword);
        tv_forgotPassword = (TextView) findViewById(R.id.tv_forgotPassword);
        iv_loginImage = (ImageView) findViewById(R.id.iv_loginImage);
        btn_login = (Button) findViewById(R.id.btn_login);
        btn_guest = (Button) findViewById(R.id.btn_guest);
        requestQueue = Volley.newRequestQueue(this);
        sharedPreferencesManager = new SharedPreferencesManager(LoginActivity.this);

        til_loginMobile = (TextInputLayout) findViewById(R.id.til_loginMobile);
        til_loginPassword = (TextInputLayout) findViewById(R.id.til_loginPassword);

        frameLayout = (FrameLayout) findViewById(R.id.loginActivity);

        sharedPreferences = getApplicationContext().getSharedPreferences("CBLCSharedPref", 0);
        editor = sharedPreferences.edit();

        snackbar = Snackbar.make(frameLayout, "Press back again to exit.", Snackbar.LENGTH_SHORT);

        tet_loginMobile.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (til_loginMobile.isErrorEnabled()) {
                    til_loginMobile.setErrorEnabled(false);
                }
                return false;
            }
        });

        tet_loginPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (til_loginPassword.isErrorEnabled()) {
                    til_loginPassword.setErrorEnabled(false);
                }
                return false;
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String rootUser = "671101";
                String rootPass = "cblcadmin";

                if (tet_loginMobile.getText().toString().equals(rootUser) && tet_loginPassword.getText().toString().equals(rootPass)) {

                    Intent intent = new Intent(LoginActivity.this, AdminActivity.class);
                    startActivity(intent);
                    finish();

                } else {
                    validateLogin();
                    hideInputMethod();
                }
            }
        });
        btn_guest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar snackbar = Snackbar.make(frameLayout, "Sign-up to get points.", Snackbar.LENGTH_LONG)
                        .setActionTextColor(getResources().getColor(R.color.colorPrimary))
                        .setAction("No, thanks !", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                FragmentManager fragmentManager = getSupportFragmentManager();
                                DialogFragmentGuestLogin dialogFragmentGuestLogin = new DialogFragmentGuestLogin();
                                dialogFragmentGuestLogin.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
                                dialogFragmentGuestLogin.show(fragmentManager, "DialogFragmentPasswordUpdate");

                            }
                        });
                snackbar.show();
            }
        });

        tv_forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                DialogFragmentForgotPassword dialogFragmentForgotPassword = new DialogFragmentForgotPassword();
                dialogFragmentForgotPassword.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
                dialogFragmentForgotPassword.show(fragmentManager, "DialogFragmentForgotPassword");
            }
        });

    }

    public void tv_registerhere(View view) {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);

    }

    private void validateLogin() {

        strLoginMobile = tet_loginMobile.getText().toString();
        strLoginPassword = tet_loginPassword.getText().toString();

        if (!isValidLogin()) {
            Toast.makeText(LoginActivity.this, "Invalid credentials", Toast.LENGTH_SHORT).show();

        } else {

            ProfileSettings profileSettings = new ProfileSettings(this);
            profileSettings.loginUser(strLoginMobile, strLoginPassword);

        }

    }

    private boolean isValidLogin() {
        boolean valid = true;

        if (strLoginMobile.length() < 11) {
            til_loginMobile.setError("Not a valid mobile number");
            valid = false;
        }

        if (strLoginPassword.length() < 8) {
            til_loginPassword.setError("Not a valid password");
            valid = false;
        }

        if (strLoginMobile.isEmpty()) {
            til_loginMobile.setError("This field is required.");
            valid = false;
        }

        if (strLoginPassword.isEmpty()) {
            til_loginPassword.setError("This field is required.");
            valid = false;
        }

        return valid;
    }

    @Override
    public void onBackPressed() {

        if (snackbar.isShown()) {
            super.onBackPressed();
        } else {
            snackbar.show();
        }
    }

    public void hideInputMethod() {

        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}
