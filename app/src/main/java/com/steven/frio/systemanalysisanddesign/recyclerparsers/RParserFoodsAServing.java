package com.steven.frio.systemanalysisanddesign.recyclerparsers;

/**
 * Created by steven on 8/10/17.
 */

public class RParserFoodsAServing {


    private Integer foodCategoryId;
    private String foodCategorySize;
    private String foodCategoryImageURL;

    public RParserFoodsAServing(String foodCategorySize, Integer foodCategoryId, String foodCategoryImageURL) {
        this.foodCategorySize = foodCategorySize;
        this.foodCategoryId = foodCategoryId;
        this.foodCategoryImageURL = foodCategoryImageURL;

    }


    public Integer getFoodCategoryId() {
        return foodCategoryId;
    }

    public String getFoodCategoryName() {
        return foodCategorySize;
    }

    public String getFoodCategoryImageURL() {
        return foodCategoryImageURL;
    }


}
