package com.steven.frio.systemanalysisanddesign;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.steven.frio.systemanalysisanddesign.configs.StringConfig;


//FragmentNavOrdering -- FRAGMENT FOR HOLDING TABS : FOOD AND DRINK TAB
//FragmentFoods -- Fragment serving as a container for FragmentFoodsMain and FragmentFoodsSub
//FragmentFoodsMain -- Fragment responsible for inflating RecyclerView containing main categories
//FragmentFoodsSub -- Fragment that will be inflated during onItemCLick on RecyclerViewHolder


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentFoods extends Fragment {

    View view;

    public FragmentFoods() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_foods, container, false);

        Log.d(StringConfig.LOG_TAG, "fragmentFoods created");

        loadFragmentFoodServing();

        return view;
    }

    public void loadFragmentFoodServing() {
        FragmentFoodsAServing fragmentFoodsAServing = new FragmentFoodsAServing();
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().add(R.id.fragmentContainerFoods, fragmentFoodsAServing).commit();
    }

}
