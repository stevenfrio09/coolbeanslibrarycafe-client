package com.steven.frio.systemanalysisanddesign.recycleradapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.steven.frio.systemanalysisanddesign.R;
import com.steven.frio.systemanalysisanddesign.R2;
import com.steven.frio.systemanalysisanddesign.recyclerparsers.RParserFoodsMain;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by steven on 8/5/17.
 */

public class RAdapterFoodsMain extends RecyclerView.Adapter<RAdapterFoodsMain.ViewHolder> {

    Context context;
    List<RParserFoodsMain> rParserFoodsList;
    RAdapterFoodsOnItemClick rAdapterFoodsOnItemClick;
    int lastPosition = -1;

    public RAdapterFoodsMain(Context context, List<RParserFoodsMain> rParserFoodsList, RAdapterFoodsOnItemClick rAdapterFoodsOnItemClick) {
        this.context = context;
        this.rParserFoodsList = rParserFoodsList;
        this.rAdapterFoodsOnItemClick = rAdapterFoodsOnItemClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cardview_holder_foods_main, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final RParserFoodsMain getRParserFoodsMain = rParserFoodsList.get(position);
        //Picasso.with(context).load(StringConfig.BASE_URL + getRParserFoodsMain.getDrinkCategoryImageURL()).into(holder.iv_cardrow_holder_foods);

        holder.tv_cardrowName_foods.setText(getRParserFoodsMain.getFoodCategoryName());
        holder.iv_cardrow_holder_foods.setTag(getRParserFoodsMain.getFoodCategoryImageURL());
        Picasso.with(context).load(getRParserFoodsMain.getFoodCategoryImageURL()).resize(1080, 600).centerCrop().into(holder.iv_cardrow_holder_foods);
        holder.iv_cardrow_holder_foods.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rAdapterFoodsOnItemClick.onItemClick(holder, position, holder.tv_cardrowName_foods, String.valueOf(getRParserFoodsMain.getFoodCategoryId()));

            }
        });

        setAnimation(holder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return rParserFoodsList.size();
    }

    private void setAnimation(View itemView, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
            itemView.setAnimation(animation);
            lastPosition = position;
        }
    }

    public interface RAdapterFoodsOnItemClick {
        void onItemClick(RecyclerView.ViewHolder holder, int position, TextView tv_cardrowName_foods, String s);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public
        @BindView(R2.id.iv_cardrow_holder_foods)
        ImageView iv_cardrow_holder_foods;

        public
        @BindView(R2.id.tv_cardrowName_foods)
        TextView tv_cardrowName_foods;

        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.setDebug(true);
            ButterKnife.bind(this, itemView);

        }
    }

}
