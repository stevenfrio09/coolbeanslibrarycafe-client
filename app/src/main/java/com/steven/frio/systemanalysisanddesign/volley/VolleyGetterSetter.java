package com.steven.frio.systemanalysisanddesign.volley;

import android.content.Context;

/**
 * Created by steven on 8/5/17.
 */

public class VolleyGetterSetter {

    Context context;
    private String token;
    private String mobile_number;


    public VolleyGetterSetter(Context context) {
        this.context = context;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }


}
