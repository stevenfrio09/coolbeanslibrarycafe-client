package com.steven.frio.systemanalysisanddesign.recycleradapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.steven.frio.systemanalysisanddesign.R;
import com.steven.frio.systemanalysisanddesign.R2;
import com.steven.frio.systemanalysisanddesign.recyclerparsers.RParserRedeemWithPoints;
import com.steven.frio.systemanalysisanddesign.sharedpreferences.SharedPreferencesManager;
import com.steven.frio.systemanalysisanddesign.sqlite.SQLiteCBLCAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by steven on 10/9/17.
 */

public class RAdapterRedeemWithPoints extends RecyclerView.Adapter<RAdapterRedeemWithPoints.ViewHolder> {

    Context context;
    List<RParserRedeemWithPoints> rParserRedeemWithPointsList;
    SQLiteCBLCAdapter sqLiteCBLCAdapter;

    public RAdapterRedeemWithPoints(Context context, List<RParserRedeemWithPoints> rParserRedeemWithPointsList) {
        this.context = context;
        this.rParserRedeemWithPointsList = rParserRedeemWithPointsList;
    }

    @Override
    public RAdapterRedeemWithPoints.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cardview_holder_redeem, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RAdapterRedeemWithPoints.ViewHolder holder, int position) {
        final RParserRedeemWithPoints rParserRedeemWithPoints = rParserRedeemWithPointsList.get(position);

        final String imageUrl = rParserRedeemWithPoints.getRedeemOrderImageUrl();
        final String orderName = rParserRedeemWithPoints.getRedeemOrderName();
        final String orderPrice = String.valueOf(rParserRedeemWithPoints.getRedeemOrderPrice());
        String orderDetails = rParserRedeemWithPoints.getRedeemOrderDetails();
        final String orderId = rParserRedeemWithPoints.getRedeemOrderId();
        final String orderServing = String.valueOf(rParserRedeemWithPoints.getRedeemServingId());
        final String orderCategory = String.valueOf(rParserRedeemWithPoints.getRedeemCategoryId());

        final String points = rParserRedeemWithPoints.getPoints();

        Picasso.with(context).load(imageUrl).resize(200, 200).centerInside().into(holder.civ_cardrowImage_redeem_sub);
        holder.tv_cardrowName_redeem_sub.setText(orderName);
        holder.tv_cardrowDesc_redeem_sub.setText(orderDetails);
        holder.tv_cardrowPrice_redeem_sub.setText(orderPrice);

        SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(context);
        final String userName = sharedPreferencesManager.getSharedPrefFirstName();

        sqLiteCBLCAdapter = new SQLiteCBLCAdapter(context);

        holder.btn_cardrowAdd_redeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                float pointsPoints = Float.parseFloat(rParserRedeemWithPoints.getPoints());

                int pointsPointsPoints = (int) pointsPoints;

                if (pointsPointsPoints < 10) {

                    int pointsNeeded = 10 - pointsPointsPoints;

                    Toast.makeText(context, "You need " + pointsNeeded + " more points to redeem this item.", Toast.LENGTH_SHORT).show();

                } else {

                    if (sqLiteCBLCAdapter.selectQuantityOrderWithPointsIsTrue(orderId).isEmpty()) {

                        int initialQuantity = 1;
                        long id = sqLiteCBLCAdapter.insertData(userName, orderName, Integer.parseInt(orderPrice), initialQuantity, imageUrl, "2", orderId, orderServing, orderCategory, "true");
                        if (id < 0) {
                            Toast.makeText(v.getContext(), "An error has occured", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(v.getContext(), orderName + " added to cart.", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        int quantity = Integer.parseInt(sqLiteCBLCAdapter.selectQuantityOrderWithPointsIsTrue(orderId));
                        quantity++;
                        sqLiteCBLCAdapter.updateQuantityOrderWithPointsIsTrue(orderId, String.valueOf(quantity));


                        int selectBasePriceFromDb = Integer.parseInt(sqLiteCBLCAdapter.selectBasePriceWithPointsIsTrue(orderId));
                        String newPrice = String.valueOf(quantity * selectBasePriceFromDb);
                        sqLiteCBLCAdapter.updateCurrentPriceWithPointsIsTrue(orderId, newPrice);

                    }

                }
            }
        });


    }

    @Override
    public int getItemCount() {

        return rParserRedeemWithPointsList.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R2.id.civ_cardrowImage_redeem_sub)
        CircleImageView civ_cardrowImage_redeem_sub;

        @BindView(R2.id.tv_cardrowName_redeem_sub)
        TextView tv_cardrowName_redeem_sub;

        @BindView(R2.id.tv_cardrowDesc_redeem_sub)
        TextView tv_cardrowDesc_redeem_sub;

        @BindView(R2.id.tv_cardrowPrice_redeem_sub)
        TextView tv_cardrowPrice_redeem_sub;

        @BindView(R2.id.btn_cardrowAdd_redeem_sub)
        Button btn_cardrowAdd_redeem;

        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.setDebug(true);
            ButterKnife.bind(this, itemView);

        }
    }
}
