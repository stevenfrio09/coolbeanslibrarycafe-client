package com.steven.frio.systemanalysisanddesign;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.steven.frio.systemanalysisanddesign.configs.StringConfig;
import com.steven.frio.systemanalysisanddesign.recycleradapters.RAdapterFoodsMain;
import com.steven.frio.systemanalysisanddesign.recyclerparsers.RParserFoodsMain;
import com.steven.frio.systemanalysisanddesign.volley.VolleyGetterSetter;
import com.steven.frio.systemanalysisanddesign.volley.VolleyNetworkRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentFoodsMain extends Fragment {

    @BindView(R2.id.srl_foodsMain)
    SwipeRefreshLayout srl_foodsMain;
    @BindView(R2.id.rv_foodsMain)
    RecyclerView rv_foodsMain;
    RecyclerView.LayoutManager layoutManager;
    @BindView(R2.id.fragmentFoodsMain)
    LinearLayout linearLayout;

    CoordinatorLayout cl_mainactivity;

    NetworkResponse networkResponse;

    View view;

    VolleyGetterSetter volleyGetterSetter;
    List<RParserFoodsMain> rParserFoodsMainList = new ArrayList<>();
    RAdapterFoodsMain rAdapterFoodsMain;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    String strGetServingValue, strGetServingValueIntValue;

    public FragmentFoodsMain() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_foods_main, container, false);

        ButterKnife.setDebug(true);
        ButterKnife.bind(this, view);

        cl_mainactivity = (CoordinatorLayout) getActivity().findViewById(R.id.cl_mainactivity);

        volleyGetterSetter = new VolleyGetterSetter(getContext());

        Bundle bundleServing = this.getArguments();

        if (bundleServing != null) {
            strGetServingValue = bundleServing.getString("setSelectedServingValue");
            strGetServingValueIntValue = bundleServing.getString("setSelectedServingValueIntValue");
            Log.d(StringConfig.LOG_TAG, "FRAGMENTMAIN" + strGetServingValue);
        }


        loadJSONFoodsMain();

        srl_foodsMain.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                srl_foodsMain.setRefreshing(true);
                loadJSONFoodsMain();
                srl_foodsMain.setRefreshing(false);

            }
        });

        return view;
    }


    private void loadJSONFoodsMain() {

        rParserFoodsMainList.clear();

        volleyGetFoodsMainCategory();
        initRecyclerViews();
        initRecyclerViewFood();

    }

    private void volleyGetFoodsMainCategory() {
        //final ProgressDialog progressDialog = new ProgressDialogMethods(getContext()).fetchingDataFromServer();
        srl_foodsMain.setRefreshing(true);

        String strLoadFoodMainCatURL = StringConfig.BASE_URL + StringConfig.ROUTE_FOODS_SHOWALL;
        //String strLoadFoodMainCatURL = "http://192.168.22.6/" + "Food.json";

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());

        StringRequest stringRequest = new StringRequest(strLoadFoodMainCatURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        JSONObject jsonObjectFoodServ = jsonObject.getJSONObject("food_serving_size");
                        String strJsonFoodServName = jsonObjectFoodServ.getString("name");


                        if (strGetServingValue.equals(strJsonFoodServName)) {

                            JSONObject jsonObjectFoodCat = jsonObject.getJSONObject("food_category");
                            int intJsonFoodCatId = jsonObjectFoodCat.getInt("id");
                            String strJsonFoodCatName = jsonObjectFoodCat.getString("name");

                            JSONObject jsonObjectFoodCatImage = jsonObjectFoodCat.getJSONObject("image");
                            String strJsonFoodCatImageUrl = StringConfig.BASE_URL + jsonObjectFoodCatImage.getString("url");


                            Log.d(StringConfig.LOG_TAG, "strJsonFoodCatName : " + strJsonFoodCatName);

                            RParserFoodsMain rParserFoodsMain = new RParserFoodsMain(intJsonFoodCatId, strJsonFoodCatName, strJsonFoodCatImageUrl);
                            rParserFoodsMainList.add(rParserFoodsMain);
                            Set<RParserFoodsMain> set = new LinkedHashSet<>();
                            set.addAll(rParserFoodsMainList);
                            rParserFoodsMainList.clear();
                            rParserFoodsMainList.addAll(set);
                            rAdapterFoodsMain.notifyDataSetChanged();

                        } else {
                            continue;
                        }

                        //progressDialog.dismiss();
                        srl_foodsMain.setRefreshing(false);
                    }

                } catch (JSONException e) {

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //progressDialog.dismiss();

                srl_foodsMain.setRefreshing(false);
                new VolleyNetworkRequest().volleyErrorLogger(cl_mainactivity, error);
            }
        });

        requestQueue.add(stringRequest);


    }


    private void initRecyclerViews() {
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rv_foodsMain.setLayoutManager(layoutManager);
    }

    private void initRecyclerViewFood() {
        rAdapterFoodsMain = new RAdapterFoodsMain(getContext(), rParserFoodsMainList, new RAdapterFoodsMain.RAdapterFoodsOnItemClick() {
            @Override
            public void onItemClick(RecyclerView.ViewHolder holder, final int position, TextView tv_cardrowName_foods, String s) {
                //Toast.makeText(getContext(), "POSITION + 1 : " + (position + 1), Toast.LENGTH_SHORT).show();

                String strSelectedServingValue = tv_cardrowName_foods.getText().toString();

                FragmentFoodsSub fragmentFoodsSub = new FragmentFoodsSub();

                Bundle bundleMain = new Bundle();
                bundleMain.putString("setSelectedCategoryValue", strSelectedServingValue);
                bundleMain.putString("setSelectedServingValue", strGetServingValue);
                bundleMain.putString("setSelectedCategoryValueIntValue", s);
                bundleMain.putString("setSelectedServingValueIntValue", strGetServingValueIntValue);

                fragmentFoodsSub.setArguments(bundleMain);

                getFragmentManager().beginTransaction().replace(R.id.fragmentContainerFoods, fragmentFoodsSub).addToBackStack(null).commit();

            }
        });
        rv_foodsMain.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        rv_foodsMain.setAdapter(rAdapterFoodsMain);

    }

}
