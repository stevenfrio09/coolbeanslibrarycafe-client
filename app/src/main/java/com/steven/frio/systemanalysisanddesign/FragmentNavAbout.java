package com.steven.frio.systemanalysisanddesign;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.steven.frio.systemanalysisanddesign.recycleradapters.RAdapterAbout;
import com.steven.frio.systemanalysisanddesign.recyclerparsers.RParserAbout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentNavAbout extends Fragment {

    @BindView(R2.id.rv_about)
    RecyclerView rv_about;
    List<RParserAbout> rParserAboutList;
    RAdapterAbout rAdapterAbout;

    public FragmentNavAbout() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_nav_about, container, false);
        ButterKnife.setDebug(true);
        ButterKnife.bind(this, view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

        rParserAboutList = new ArrayList<>();

        rAdapterAbout = new RAdapterAbout(getContext(), rParserAboutList);
        rv_about.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        rv_about.setLayoutManager(layoutManager);
        rv_about.setAdapter(rAdapterAbout);

        RParserAbout rParserAbout = new RParserAbout("Mobile App Developer", "John Steven Frio");
        rParserAboutList.add(rParserAbout);

        rParserAbout = new RParserAbout("Back-End Web Developer", "Anthony Hernaez");
        rParserAboutList.add(rParserAbout);

        rParserAbout = new RParserAbout("GUI Designer", "Jun Joie Ruiz Jr.");
        rParserAboutList.add(rParserAbout);

        rParserAbout = new RParserAbout("Document Writers", "Andrelaine Asinas \n Lemuel Basalo \n Darwin Borines \n Krizza Mae Cueto \n Christian Edward Dantes \n Ann Rio Pantaleon");
        rParserAboutList.add(rParserAbout);

        rAdapterAbout.notifyDataSetChanged();

        return view;
    }

}
