package com.steven.frio.systemanalysisanddesign.interfaces;

import android.content.SharedPreferences;

/**
 * Created by steven on 8/31/17.
 */

public interface UpdateProfileInfoInterface {
    void updateDisplayedInfo(SharedPreferences sharedPreferences, String sharedPrefFstName, String sharedPrefLstName, String sharedPrefMobileNum);
}
