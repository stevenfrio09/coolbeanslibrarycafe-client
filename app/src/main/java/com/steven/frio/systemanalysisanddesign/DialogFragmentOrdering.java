package com.steven.frio.systemanalysisanddesign;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.steven.frio.systemanalysisanddesign.configs.StringConfig;
import com.steven.frio.systemanalysisanddesign.sharedpreferences.SharedPreferencesManager;
import com.steven.frio.systemanalysisanddesign.sqlite.SQLiteCBLCAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class DialogFragmentOrdering extends DialogFragment {

    @BindView(R2.id.iv_df_image)
    ImageView iv_df_image;

    @BindView(R2.id.tv_df_priceCurrent)
    TextView tv_df_priceCurrent;

    @BindView(R2.id.iv_df_reduce)
    ImageView iv_df_reduce;

    @BindView(R2.id.iv_df_increase)
    ImageView iv_df_increase;

    @BindView(R2.id.et_df_quantity)
    EditText et_df_quantity;

    @BindView(R2.id.btn_df_addToCart)
    Button btn_df_addToCart;

    @BindView(R2.id.tv_df_details)
    TextView tv_df_details;

    @BindView(R2.id.tv_df_price)
    TextView tv_df_price;

    String orderName, orderDetails, orderPrice, imageUrl, orderId, orderServing, orderCategory, orderType;

    SQLiteCBLCAdapter sqLiteCBLCAdapter;

    public DialogFragmentOrdering() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.dialog_fragment_ordering, container, false);

        ButterKnife.setDebug(true);
        ButterKnife.bind(this, view);


        Bundle bundle = this.getArguments();
        if (bundle != null) {
            orderName = bundle.getString("name");
            orderDetails = bundle.getString("details");
            orderPrice = bundle.getString("price");
            imageUrl = bundle.getString("imageUrl");
            orderId = bundle.getString("orderId");
            orderType = bundle.getString("orderType");
            orderServing = bundle.getString("serving");
            orderCategory = bundle.getString("category");


            Log.d(StringConfig.LOG_TAG, "orderName : " + orderName);
            Log.d(StringConfig.LOG_TAG, "orderPrice : " + orderPrice);
            Log.d(StringConfig.LOG_TAG, "orderDetails : " + orderDetails);
            Log.d(StringConfig.LOG_TAG, "imageUrl : " + imageUrl);
            Log.d(StringConfig.LOG_TAG, "orderId : " + orderId);

        }
        getDialog().setTitle(orderName);

        //tv_df_name.setText(orderName);
        tv_df_details.setText(orderDetails);
        tv_df_price.setText(orderPrice);

        Picasso.with(getContext()).load(imageUrl).resize(200, 200).centerInside().into(iv_df_image);

        Log.d(StringConfig.LOG_TAG, "orderName : " + orderName);

        et_df_quantity.setText(String.valueOf(1));
        tv_df_priceCurrent.setText(tv_df_price.getText().toString());


        iv_df_increase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                incrementDecrement("increment");
            }
        });

        iv_df_reduce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                incrementDecrement("decrement");
            }
        });

        et_df_quantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                //int priceCurrent = Integer.parseInt(tv_df_price.getText().toString()) * Integer.parseInt(s.toString());
                try {
                    int quantity = Integer.parseInt(s.toString());

                    setPrice(quantity);

                } catch (NumberFormatException e) {

                    int quantity = 1;
                    et_df_quantity.setText(String.valueOf(1));
                    setPrice(quantity);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(getContext());
        final String userName = sharedPreferencesManager.getSharedPrefFirstName();

        sqLiteCBLCAdapter = new SQLiteCBLCAdapter(getContext());

        btn_df_addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int quantity = Integer.parseInt(et_df_quantity.getText().toString());
                int basePrice = Integer.parseInt(tv_df_price.getText().toString());

                if (sqLiteCBLCAdapter.selectQuantity(orderId).isEmpty()) {

                    long id = sqLiteCBLCAdapter.insertData(userName, orderName, Integer.parseInt(orderPrice), quantity, imageUrl, orderType, orderId, orderServing, orderCategory, "false");
                    String newPrice = String.valueOf(quantity * basePrice);
                    sqLiteCBLCAdapter.updateCurrentPrice(orderId, newPrice);

                    if (id < 0) {
                        Toast.makeText(getContext(), "An error has occured.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), orderName + " added to cart.", Toast.LENGTH_SHORT).show();
                        dismiss();
                    }

                } else {

                    int selectQuantity = Integer.parseInt(sqLiteCBLCAdapter.selectQuantity(orderId));
                    int updateQuantity = selectQuantity + quantity;

                    sqLiteCBLCAdapter.updateQuantity(orderId, String.valueOf(updateQuantity));

                    int selectBasePriceFromDb = Integer.parseInt(sqLiteCBLCAdapter.selectBasePrice(orderId));
                    String newPrice = String.valueOf(updateQuantity * selectBasePriceFromDb);
                    sqLiteCBLCAdapter.updateCurrentPrice(orderId, newPrice);

                    Toast.makeText(getContext(), orderName + " quantity updated.", Toast.LENGTH_SHORT).show();

                    dismiss();

                }


            }
        });

        return view;
    }

    private void incrementDecrement(String incrementDecrement) {
        int quantity = Integer.parseInt(et_df_quantity.getText().toString());

        if (incrementDecrement.equals("increment")) {

            quantity++;

        } else {

            if (!(quantity <= 1)) {
                quantity--;

            }
        }

        et_df_quantity.setText(String.valueOf(quantity));

        setPrice(quantity);

    }

    private void setPrice(int quantity) {

        int priceCurrent = Integer.parseInt(tv_df_price.getText().toString()) * quantity;

        tv_df_priceCurrent.setText(String.valueOf(priceCurrent));

    }


}
