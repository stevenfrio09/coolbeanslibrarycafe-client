package com.steven.frio.systemanalysisanddesign;


import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.steven.frio.systemanalysisanddesign.configs.StringConfig;
import com.steven.frio.systemanalysisanddesign.recycleradapters.RAdapterDrinksMain;
import com.steven.frio.systemanalysisanddesign.recyclerparsers.RParserDrinksMain;
import com.steven.frio.systemanalysisanddesign.volley.VolleyNetworkRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentDrinksMain extends Fragment {
    @BindView(R2.id.srl_drinksMain)
    SwipeRefreshLayout srl_drinksMain;
    @BindView(R2.id.rv_drinksMain)
    RecyclerView rv_drinksMain;
    @BindView(R2.id.fragmentDrinksMain)
    LinearLayout linearLayout;

    CoordinatorLayout cl_mainactivity;

    String strGetServingValue;
    String strGetServingValueIntValue;

    List<RParserDrinksMain> rParserDrinksMainList = new ArrayList<>();
    RAdapterDrinksMain rAdapterDrinksMain;
    RecyclerView.LayoutManager layoutManager;
    NetworkResponse networkResponse;

    View view;

    public FragmentDrinksMain() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_drinks_main, container, false);

        ButterKnife.setDebug(true);
        ButterKnife.bind(this, view);

        cl_mainactivity = (CoordinatorLayout) getActivity().findViewById(R.id.cl_mainactivity);

        Bundle bundleServing = this.getArguments();

        if (bundleServing != null) {
            strGetServingValue = bundleServing.getString("setSelectedTempValueDrinks");
            strGetServingValueIntValue = bundleServing.getString("setSelectedTempValueDrinksIntValue");
            Log.d(StringConfig.LOG_TAG, "FRAGMENTMAIN" + strGetServingValue);
        }

        loadJSONDrinksMain();

        srl_drinksMain.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                srl_drinksMain.setRefreshing(true);
                loadJSONDrinksMain();
                srl_drinksMain.setRefreshing(false);
            }
        });

        return view;
    }

    private void loadJSONDrinksMain() {
        rParserDrinksMainList.clear();
        volleyGetDrinksMainCategory();
        initReyclerViews();
        initRecyclerViewDrinks();


    }


    public void volleyGetDrinksMainCategory() {

        //final ProgressDialog progressDialog = new ProgressDialogMethods(getContext()).fetchingDataFromServer();
        srl_drinksMain.setRefreshing(true);

        String strLoadDrinksMainCatURL = StringConfig.BASE_URL + StringConfig.ROUTE_DRINKS_SHOWALL;

        //String strLoadDrinksMainCatURL = "http://192.168.22.6/Drink.json";

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());

        StringRequest stringRequest = new StringRequest(strLoadDrinksMainCatURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.d(StringConfig.LOG_TAG, "calledLoadJSONDrinks : " + response);
                    for (int i = 0; i <= jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        JSONObject jsonDrinkTemp = jsonObject.getJSONObject("drink_serving_temperature");
                        String strJsonDrinkTempName = jsonDrinkTemp.getString("name");
                        //Log.d(StringConfig.LOG_TAG, "strJsonDrinkTemp : " + strJsonDrinkTempId);

                        if (strJsonDrinkTempName.equals(strGetServingValue)) {

                            JSONObject jsonDrinkCategory = jsonObject.getJSONObject("drink_category");

                            int intJsonDrinkId = jsonDrinkCategory.getInt("id");
                            String strJsonDrinkName = jsonDrinkCategory.getString("name");

                            JSONObject jsonDrinkCategoryImage = jsonDrinkCategory.getJSONObject("image");

                            String strJsonDrinkImageUrl = StringConfig.BASE_URL + jsonDrinkCategoryImage.getString("url");

                            Log.d(StringConfig.LOG_TAG, "strJsonDrinkName : " + strJsonDrinkName);
                            Log.d(StringConfig.LOG_TAG, "intJsonDrinkId : " + intJsonDrinkId);


                            RParserDrinksMain rParserDrinksMain = new RParserDrinksMain(intJsonDrinkId, strJsonDrinkName, strJsonDrinkImageUrl);
                            rParserDrinksMainList.add(rParserDrinksMain);
                            Set<RParserDrinksMain> set = new LinkedHashSet<>();
                            set.addAll(rParserDrinksMainList);
                            rParserDrinksMainList.clear();
                            rParserDrinksMainList.addAll(set);

                            rAdapterDrinksMain.notifyDataSetChanged();

                        } else {
                            continue;
                        }


                        //progressDialog.dismiss();
                        srl_drinksMain.setRefreshing(false);

                    }
                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //progressDialog.dismiss();
                srl_drinksMain.setRefreshing(false);
                new VolleyNetworkRequest().volleyErrorLogger(cl_mainactivity, error);

            }
        });

        requestQueue.add(stringRequest);

    }

    private void initReyclerViews() {
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rv_drinksMain.setLayoutManager(layoutManager);
    }

    private void initRecyclerViewDrinks() {
        rAdapterDrinksMain = new RAdapterDrinksMain(getContext(), rParserDrinksMainList, new RAdapterDrinksMain.RAdapterDrinksMainOnItemClick() {
            @Override
            public void onItemClick(RecyclerView.ViewHolder holder, int position, TextView tv_cardrowName_drinks_main, String s) {

                String strSelectedCategoryValue = tv_cardrowName_drinks_main.getText().toString();

                FragmentDrinksSub fragmentDrinksSub = new FragmentDrinksSub();

                Bundle bundleMain = new Bundle();
                bundleMain.putString("setSelectedCategoryValueDrinks", strSelectedCategoryValue);
                bundleMain.putString("setSelectedServingValueDrinks", strGetServingValue);
                bundleMain.putString("setSelectCategoryValueDrinksIntValue", s);
                bundleMain.putString("setSelectedServingValueDrinksIntValue", strGetServingValueIntValue);
                Log.d(StringConfig.LOG_TAG, "the value : " + s);

                fragmentDrinksSub.setArguments(bundleMain);

                getFragmentManager().beginTransaction().replace(R.id.fragmentContainerDrinks, fragmentDrinksSub).addToBackStack(null).commit();

            }
        });

        rv_drinksMain.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        rv_drinksMain.setAdapter(rAdapterDrinksMain);
    }


}
