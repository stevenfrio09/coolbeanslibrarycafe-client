package com.steven.frio.systemanalysisanddesign.recyclerparsers;

/**
 * Created by steven on 8/10/17.
 */

public class RParserFoodsSub {

    private Integer foodCategoryId;
    private String foodCategoryName;
    private String foodCategoryImageURL;
    private Integer foodCategoryPrice;
    private String foodCategoryDetails;
    private String foodOrderId;
    private String foodOrderServing;
    private String foodOrderCategory;

    public RParserFoodsSub(String foodCategoryName, Integer foodCategoryPrice, String foodCategoryDetails, String foodCategoryImageURL, String foodOrderId, String foodOrderServing, String foodOrderCategory) {
        this.foodCategoryName = foodCategoryName;
        this.foodCategoryImageURL = foodCategoryImageURL;
        this.foodCategoryPrice = foodCategoryPrice;
        this.foodCategoryDetails = foodCategoryDetails;
        this.foodOrderId = foodOrderId;
        this.foodOrderServing = foodOrderServing;
        this.foodOrderCategory = foodOrderCategory;
    }


    public Integer getFoodCategoryId() {
        return foodCategoryId;
    }

    public String getFoodCategoryName() {
        return foodCategoryName;
    }

    public String getFoodCategoryImageURL() {
        return foodCategoryImageURL;
    }

    public Integer getFoodCategoryPrice() {
        return foodCategoryPrice;
    }

    public String getFoodCategoryDetails() {
        return foodCategoryDetails;
    }


    public String getFoodOrderId() {
        return foodOrderId;
    }

    public String getFoodOrderServing() {
        return foodOrderServing;
    }

    public void setFoodOrderServing(String foodOrderServing) {
        this.foodOrderServing = foodOrderServing;
    }

    public String getFoodOrderCategory() {
        return foodOrderCategory;
    }

    public void setFoodOrderCategory(String foodOrderCategory) {
        this.foodOrderCategory = foodOrderCategory;
    }
}
