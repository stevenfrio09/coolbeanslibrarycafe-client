package com.steven.frio.systemanalysisanddesign;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.steven.frio.systemanalysisanddesign.sharedpreferences.SharedPreferencesManager;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //Log.d(StringConfig.LOG_TAG, strSharedPrefTokenValue);

        final SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(SplashScreen.this);

        int SPLASH_DISPLAY_LENGTH = 2000;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (sharedPreferencesManager.getSharedPrefToken() != null) {
                    Intent intent = new Intent(SplashScreen.this, NavigationDrawer.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(SplashScreen.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }

            }
        }, SPLASH_DISPLAY_LENGTH);

    }

}
