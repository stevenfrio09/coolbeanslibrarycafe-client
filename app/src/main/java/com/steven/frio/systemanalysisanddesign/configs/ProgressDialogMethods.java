package com.steven.frio.systemanalysisanddesign.configs;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by steven on 9/3/17.
 */

public class ProgressDialogMethods {

    Context context;

    public ProgressDialogMethods(Context context) {
        this.context = context;
    }

    public ProgressDialog fetchingDataFromServer() {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Fetching data from server...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        return progressDialog;
    }

    public ProgressDialog pleaseWait() {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        return progressDialog;
    }

}
