package com.steven.frio.systemanalysisanddesign;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.steven.frio.systemanalysisanddesign.configs.StringConfig;
import com.steven.frio.systemanalysisanddesign.sharedpreferences.SharedPreferencesManager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdminActivity extends AppCompatActivity {

    @BindView(R2.id.tet_adminSetIP)
    TextInputEditText tet_adminSetIP;

    @BindView(R2.id.btn_adminApply)
    Button btn_adminApply;

    @BindView(R2.id.ll_admin)
    LinearLayout ll_admin;

    Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        ButterKnife.setDebug(true);
        ButterKnife.bind(this);

        snackbar = Snackbar.make(ll_admin,"Press back again to exit.", Snackbar.LENGTH_SHORT );

        btn_adminApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String getIP = tet_adminSetIP.getText().toString().trim();
                String ipaddress = "http://" + getIP + ":3000/";

                Log.d(StringConfig.LOG_TAG, "ip : " + ipaddress);

                SharedPreferencesManager.setSharedPrefIP(ipaddress);

                Toast.makeText(AdminActivity.this, "Success. Please restart the app.", Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public void onBackPressed() {

        if(snackbar.isShown()){

            super.onBackPressed();

        }else {
            snackbar.show();
        }

    }
}
