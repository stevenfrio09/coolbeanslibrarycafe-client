package com.steven.frio.systemanalysisanddesign.recyclerparsers;

/**
 * Created by steven on 10/9/17.
 */

public class RParserRedeemWithPoints {


    private Integer redeemServingId;
    private Integer redeemCategoryId;
    private String redeemOrderId;
    private String redeemOrderName;
    private Integer redeemOrderPrice;
    private String redeemOrderDetails;
    private String redeemOrderImageUrl;
    private String points;

    public RParserRedeemWithPoints(Integer redeemServingId, Integer redeemCategoryId, String redeemOrderId, String redeemOrderName, Integer redeemOrderPrice, String redeemOrderDetails, String redeemOrderImageUrl, String points) {
        this.redeemServingId = redeemServingId;
        this.redeemCategoryId = redeemCategoryId;
        this.redeemOrderId = redeemOrderId;
        this.redeemOrderName = redeemOrderName;
        this.redeemOrderPrice = redeemOrderPrice;
        this.redeemOrderDetails = redeemOrderDetails;
        this.redeemOrderImageUrl = redeemOrderImageUrl;
        this.points = points;
    }

    public Integer getRedeemServingId() {
        return redeemServingId;
    }

    public void setRedeemServingId(Integer redeemServingId) {
        this.redeemServingId = redeemServingId;
    }

    public Integer getRedeemCategoryId() {
        return redeemCategoryId;
    }

    public void setRedeemCategoryId(Integer redeemCategoryId) {
        this.redeemCategoryId = redeemCategoryId;
    }

    public String getRedeemOrderId() {
        return redeemOrderId;
    }

    public void setRedeemOrderId(String redeemOrderId) {
        this.redeemOrderId = redeemOrderId;
    }

    public String getRedeemOrderName() {
        return redeemOrderName;
    }

    public void setRedeemOrderName(String redeemOrderName) {
        this.redeemOrderName = redeemOrderName;
    }

    public Integer getRedeemOrderPrice() {
        return redeemOrderPrice;
    }

    public void setRedeemOrderPrice(Integer redeemOrderPrice) {
        this.redeemOrderPrice = redeemOrderPrice;
    }

    public String getRedeemOrderDetails() {
        return redeemOrderDetails;
    }

    public void setRedeemOrderDetails(String redeemOrderDetails) {
        this.redeemOrderDetails = redeemOrderDetails;
    }

    public String getRedeemOrderImageUrl() {
        return redeemOrderImageUrl;
    }

    public void setRedeemOrderImageUrl(String redeemOrderImageUrl) {
        this.redeemOrderImageUrl = redeemOrderImageUrl;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }
}
