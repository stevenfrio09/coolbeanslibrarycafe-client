package com.steven.frio.systemanalysisanddesign;


import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.steven.frio.systemanalysisanddesign.configs.StringConfig;
import com.steven.frio.systemanalysisanddesign.volley.ProfileSettings;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class DialogFragmentGuestLogin extends DialogFragment {

    @BindView(R2.id.btn_loginGuest)
    Button btn_loginGuest;

    @BindView(R2.id.til_loginGuest)
    TextInputLayout til_loginGuest;

    @BindView(R2.id.tet_loginGuest)
    TextInputEditText tet_loginGuest;

    public DialogFragmentGuestLogin() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.dialog_fragment_guest, container, false);
        ButterKnife.setDebug(true);
        ButterKnife.bind(this, view);

        getDialog().setTitle("Guest");


        btn_loginGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String strGuestName = tet_loginGuest.getText().toString();

                if (!strGuestName.isEmpty()) {

                    ProfileSettings profileSettings = new ProfileSettings(getContext());
                    profileSettings.saveAllCustomerIdWithPendingOrders();
                    profileSettings.selectValidGuestLogin(StringConfig.GUEST_PASSWORD, strGuestName);

                    //profileSettings.checkIfUserHasPendingOrder();
                } else {
                    til_loginGuest.setError("This field is required.");
                }

                /*Intent intent = new Intent(getContext(), NavigationDrawer.class);
                startActivity(intent);*/

            }
        });

        tet_loginGuest.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (til_loginGuest.isErrorEnabled()) {
                    til_loginGuest.setErrorEnabled(false);
                }
                return false;
            }
        });

        return view;
    }


}
