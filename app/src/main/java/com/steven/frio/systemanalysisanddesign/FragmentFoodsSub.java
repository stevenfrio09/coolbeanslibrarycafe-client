package com.steven.frio.systemanalysisanddesign;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.steven.frio.systemanalysisanddesign.configs.StringConfig;
import com.steven.frio.systemanalysisanddesign.recycleradapters.RAdapterFoodsSub;
import com.steven.frio.systemanalysisanddesign.recyclerparsers.RParserFoodsSub;
import com.steven.frio.systemanalysisanddesign.volley.VolleyGetterSetter;
import com.steven.frio.systemanalysisanddesign.volley.VolleyNetworkRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentFoodsSub extends Fragment {

    @BindView(R2.id.srl_foodsSub)
    SwipeRefreshLayout srl_foodsSub;

    @BindView(R2.id.rv_foodsSub)
    RecyclerView rv_foodsSub;

    @BindView(R2.id.fragmentFoodsSub)
    LinearLayout linearLayout;

    CoordinatorLayout cl_mainactivity;

    List<RParserFoodsSub> rParserFoodsSubList = new ArrayList<>();
    RAdapterFoodsSub rAdapterFoodsSub;
    RecyclerView.LayoutManager layoutManager;
    NetworkResponse networkResponse;

    VolleyGetterSetter volleyGetterSetter;
    String sharedPrefValueToken;
    SharedPreferences sharedPreferences;
    String getSelectedServingValue, getSelectedCategoryValue;
    String getSelectedServingValueIntValue, getSelectedCategoryValueIntValue;

    View view;


    public FragmentFoodsSub() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_foods_sub, container, false);

        ButterKnife.setDebug(true);
        ButterKnife.bind(this, view);

        cl_mainactivity = (CoordinatorLayout) getActivity().findViewById(R.id.cl_mainactivity);

        sharedPreferences = getContext().getSharedPreferences(StringConfig.SHAREDPREF_NAME, 0);
        sharedPrefValueToken = sharedPreferences.getString("token", null);
        volleyGetterSetter = new VolleyGetterSetter(getContext());
        volleyGetterSetter.setToken(sharedPrefValueToken);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            getSelectedServingValue = bundle.getString("setSelectedServingValue");
            getSelectedCategoryValue = bundle.getString("setSelectedCategoryValue");
            getSelectedServingValueIntValue = bundle.getString("setSelectedServingValueIntValue");
            getSelectedCategoryValueIntValue = bundle.getString("setSelectedCategoryValueIntValue");

            Log.d(StringConfig.LOG_TAG, "servingVal : " + getSelectedServingValue);
            Log.d(StringConfig.LOG_TAG, "categoryVal : " + getSelectedCategoryValue);

            loadJSONFoodsSub();

            srl_foodsSub.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    srl_foodsSub.setRefreshing(true);
                    loadJSONFoodsSub();
                    srl_foodsSub.setRefreshing(false);

                }
            });

        }

        return view;
    }

    private void loadJSONFoodsSub() {
        rParserFoodsSubList.clear();
        volleyGetFoodsSubCategory();
        initRecyclerViews();
        initRecyclerViewFood();
    }

    private void volleyGetFoodsSubCategory() {
        //final ProgressDialog progressDialog = new ProgressDialogMethods(getContext()).fetchingDataFromServer();

        srl_foodsSub.setRefreshing(true);

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());

        String strLoadFoodSubCatURL = StringConfig.BASE_URL + StringConfig.ROUTE_FOODS_SHOWALL;

        //String strLoadFoodSubCatURL = "http://192.168.22.6/" + "Food.json";


        StringRequest stringRequest = new StringRequest(strLoadFoodSubCatURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        JSONObject jsonObjectFoodServ = jsonObject.getJSONObject("food_serving_size");
                        String strJsonFoodServName = jsonObjectFoodServ.getString("name");

                        JSONObject jsonObjectFoodCat = jsonObject.getJSONObject("food_category");
                        String strJsonFoodCatName = jsonObjectFoodCat.getString("name");

                        if (getSelectedServingValue.equals(strJsonFoodServName)) {
                            if (getSelectedCategoryValue.equals(strJsonFoodCatName)) {


                                String strJsonFoodMainId = String.valueOf(jsonObject.getInt("id"));
                                String strJsonFoodMainName = jsonObject.getString("name");
                                int strJsonFoodMainPrice = jsonObject.getInt("price");
                                String strJsonFoodMainDetail = jsonObject.getString("detail");

                                JSONObject jsonObjectFoodMainImage = jsonObject.getJSONObject("image");
                                String strJsonFoodMainImageUrl = jsonObjectFoodMainImage.getString("url");


                                Log.d(StringConfig.LOG_TAG, "strJsonDrinkMainId : " + strJsonFoodMainId);
                                Log.d(StringConfig.LOG_TAG, "strJsonDrinkMainName : " + strJsonFoodMainName);
                                Log.d(StringConfig.LOG_TAG, "strJsonDrinkMainPrice : " + strJsonFoodMainPrice);
                                Log.d(StringConfig.LOG_TAG, "strJsonDrinkMainDetail : " + strJsonFoodMainDetail);

                                RParserFoodsSub rParserFoodsSub = new RParserFoodsSub(strJsonFoodMainName, strJsonFoodMainPrice, strJsonFoodMainDetail, strJsonFoodMainImageUrl, strJsonFoodMainId, getSelectedServingValueIntValue, getSelectedCategoryValueIntValue);
                                rParserFoodsSubList.add(rParserFoodsSub);
                                rAdapterFoodsSub.notifyDataSetChanged();


                            } else {
                                continue;

                            }

                        } else {
                            continue;
                        }

                        //progressDialog.dismiss();
                        srl_foodsSub.setRefreshing(false);
                    }

                } catch (
                        JSONException e)

                {

                }


            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {

                //progressDialog.dismiss();

                srl_foodsSub.setRefreshing(false);
                new VolleyNetworkRequest().volleyErrorLogger(cl_mainactivity, error);
            }
        });

        requestQueue.add(stringRequest);

    }

/*
    private void volleyGetFoodsSubCategory(final String getSelectedServingValue, final String getSelectedCategoryValue) throws JSONException {

        final ProgressDialog progressDialog = new ProgressDialogMethods(getContext()).fetchingDataFromServer();

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());

        String strLoadFoodSubCatURL = StringConfig.BASE_URL + StringConfig.ROUTE_FOODS_SUBCATEGORY;

        Map<String, String> mapOrderList = new HashMap<String, String>();
        mapOrderList.put("food_serving_size_id", getSelectedServingValue);
        mapOrderList.put("food_category_id", getSelectedCategoryValue);

        CustomRequest customRequest = new CustomRequest(Request.Method.POST, strLoadFoodSubCatURL, (mapOrderList != null) ? new JSONObject(mapOrderList) : null, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
                Log.d(StringConfig.LOG_TAG, "json response : " + response);
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject jsonObject = response.getJSONObject(i);
                        int jsonObjectOrderId = jsonObject.getInt("id");
                        String jsonObjectName = jsonObject.getString("name");
                        int jsonObjectPrice = jsonObject.getInt("price");
                        String jsonObjectDetail = jsonObject.getString("detail");

                        Log.d(StringConfig.LOG_TAG, "jsonObjectId : " + jsonObjectOrderId);
                        Log.d(StringConfig.LOG_TAG, "jsonObjectName : " + jsonObjectName);
                        Log.d(StringConfig.LOG_TAG, "jsonObjectPrice : " + jsonObjectPrice);

                        JSONObject jsonObjectImage = jsonObject.getJSONObject("image");
                        String jsonOObjectImageUrl = jsonObjectImage.getString("url");

                        RParserFoodsSub rParserFoodsSub = new RParserFoodsSub(jsonObjectName, jsonObjectPrice, jsonObjectDetail, jsonOObjectImageUrl, jsonObjectOrderId);
                        rParserFoodsSubList.add(rParserFoodsSub);
                        rAdapterFoodsSub.notifyDataSetChanged();

                        progressDialog.dismiss();

                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }


                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();

                Snackbar snackbar = Snackbar.make(linearLayout, error.toString(), Snackbar.LENGTH_SHORT);
                snackbar.show();

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> mapHeader = new HashMap<String, String>();
                mapHeader.put("token", volleyGetterSetter.getToken());
                Log.d(StringConfig.LOG_TAG, "TOKEN VALUE : " + volleyGetterSetter.getToken());
                return mapHeader;

            }
        };

        requestQueue.add(customRequest);

    }*/

    private void initRecyclerViews() {
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rv_foodsSub.setLayoutManager(layoutManager);
    }

    private void initRecyclerViewFood() {
        rAdapterFoodsSub = new RAdapterFoodsSub(getContext(), rParserFoodsSubList, new RAdapterFoodsSub.DisplayFragmentDialog() {
            @Override
            public void displayDialog(int position, String orderName, String orderDetails, String orderPrice, String strImageUrl, String orderId) {
                FragmentManager fragmentManager = getFragmentManager();
                DialogFragmentOrdering dialogFragmentOrdering = new DialogFragmentOrdering();
                dialogFragmentOrdering.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);

                Bundle bundle = new Bundle();
                bundle.putString("name", orderName);
                bundle.putString("details", orderDetails);
                bundle.putString("price", orderPrice);
                bundle.putString("imageUrl", strImageUrl);
                bundle.putString("orderId", orderId);
                bundle.putString("orderType", "1");
                bundle.putString("serving", getSelectedServingValueIntValue);
                bundle.putString("category", getSelectedCategoryValueIntValue);

                dialogFragmentOrdering.setArguments(bundle);

                dialogFragmentOrdering.show(fragmentManager, "DialogFragmentOrdering");

            }
        });
        //rv_foodsSub.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        rv_foodsSub.setAdapter(rAdapterFoodsSub);

    }

    private void displayNetworkError(VolleyError error) {

        networkResponse = error.networkResponse;

        if (networkResponse != null && networkResponse.statusCode == 500) {
            Snackbar snackbar = Snackbar.make(linearLayout, "Error 500. Internal server error.", Snackbar.LENGTH_SHORT);
            snackbar.show();
        }

        if (networkResponse != null && networkResponse.statusCode == 400) {
            Snackbar snackbar = Snackbar.make(linearLayout, "Check your credentials.", Snackbar.LENGTH_SHORT);
            snackbar.show();
        }

        if (networkResponse != null && networkResponse.statusCode == 404) {
            Snackbar snackbar = Snackbar.make(linearLayout, "Error 404. Server not found", Snackbar.LENGTH_SHORT);
            snackbar.show();
        }

        if (error instanceof NoConnectionError) {
            Snackbar snackbar = Snackbar.make(linearLayout, "No Connection.", Snackbar.LENGTH_SHORT);
            snackbar.show();
        }

        if (error instanceof TimeoutError) {
            Snackbar snackbar = Snackbar.make(linearLayout, "Connection Timed Out.", Snackbar.LENGTH_SHORT);
            snackbar.show();
        }

    }
}
