package com.steven.frio.systemanalysisanddesign.recycleradapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.steven.frio.systemanalysisanddesign.R;
import com.steven.frio.systemanalysisanddesign.R2;
import com.steven.frio.systemanalysisanddesign.recyclerparsers.RParserDrinksAServing;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by steven on 8/16/17.
 */

public class RAdapterDrinksAServing extends RecyclerView.Adapter<RAdapterDrinksAServing.ViewHolder> {

    Context context;
    List<RParserDrinksAServing> rParserDrinksAServingList;
    RAdapterDrinksAServingOnItemClick rAdapterDrinksAServingOnItemClick;
    int lastPosition = -1;

    public RAdapterDrinksAServing(Context context, List<RParserDrinksAServing> rParserDrinksAServingList, RAdapterDrinksAServingOnItemClick rAdapterDrinksAServingOnItemClick) {
        this.context = context;
        this.rParserDrinksAServingList = rParserDrinksAServingList;
        this.rAdapterDrinksAServingOnItemClick = rAdapterDrinksAServingOnItemClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cardview_holder_drinks_aserving, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final RParserDrinksAServing rParserDrinksAServing = rParserDrinksAServingList.get(position);

        holder.tv_cardrowName_drinks_aserving.setText(rParserDrinksAServing.getDrinkCategoryTemp());

        Picasso.with(context).load(rParserDrinksAServing.getDrinkCategoryImageURL()).resize(1080, 600).centerCrop().into(holder.iv_cardrow_holder_drinks_aserving);

        holder.iv_cardrow_holder_drinks_aserving.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rAdapterDrinksAServingOnItemClick.onItemClick(holder, position, holder.tv_cardrowName_drinks_aserving, String.valueOf(rParserDrinksAServing.getDrinkCategoryId()));
            }
        });

        setAnimation(holder.itemView, position);

    }

    @Override
    public int getItemCount() {
        return rParserDrinksAServingList.size();
    }

    private void setAnimation(View itemView, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
            itemView.startAnimation(animation);
            lastPosition = position;

        }
    }

    public interface RAdapterDrinksAServingOnItemClick {
        void onItemClick(RecyclerView.ViewHolder holder, int position, TextView tv_cardrowName_drinks_aserving, String s);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R2.id.iv_cardrow_holder_drinks_aserving)
        ImageView iv_cardrow_holder_drinks_aserving;

        @BindView(R2.id.tv_cardrowName_drinks_aserving)
        TextView tv_cardrowName_drinks_aserving;

        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

        }
    }
}
