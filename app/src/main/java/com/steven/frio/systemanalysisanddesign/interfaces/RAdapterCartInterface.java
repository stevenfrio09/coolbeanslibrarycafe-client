package com.steven.frio.systemanalysisanddesign.interfaces;

import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by steven on 8/26/17.
 */

public interface RAdapterCartInterface {
    void addButtonClick(TextView tv_cardrowName_atc, TextView tv_currentPrice_atc, EditText et_quantity_atc, int position, String orderId, String transactionType);

    void subButtonClick(TextView tv_cardrowName_atc, TextView tv_currentPrice_atc, EditText et_quantity_atc, int position, String orderId, String transactionType);

    void editTextWatcher(int basePrice, String orderName, TextView tv_currentPrice_atc, EditText et_quantity_atc, String orderId, String transactionType, TextView tv_transactionType_atc);

    void removeItem(int position, String orderName, String orderId);

}
