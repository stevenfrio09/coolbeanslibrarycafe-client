package com.steven.frio.systemanalysisanddesign.recycleradapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.steven.frio.systemanalysisanddesign.R;
import com.steven.frio.systemanalysisanddesign.R2;
import com.steven.frio.systemanalysisanddesign.recyclerparsers.RParserFoodsAServing;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by steven on 8/10/17.
 */

public class RAdapterFoodsAServing extends RecyclerView.Adapter<RAdapterFoodsAServing.ViewHolder> {

    Context context;
    List<RParserFoodsAServing> rParserFoodsAServingList;
    RAdapterFoodsAServingOnItemClick rAdapterFoodsAServingOnItemClick;

    int lastPosition = -1;

    public RAdapterFoodsAServing(Context context, List<RParserFoodsAServing> rParserFoodsAServingList, RAdapterFoodsAServingOnItemClick rAdapterFoodsAServingOnItemClick) {
        this.context = context;
        this.rParserFoodsAServingList = rParserFoodsAServingList;
        this.rAdapterFoodsAServingOnItemClick = rAdapterFoodsAServingOnItemClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cardview_holder_foods_aserving, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final RParserFoodsAServing rParserFoodsAServing = rParserFoodsAServingList.get(position);

        holder.tv_cardrowName_foods_aserving.setText(rParserFoodsAServing.getFoodCategoryName());
        Picasso.with(context).load(rParserFoodsAServing.getFoodCategoryImageURL()).resize(1080, 600).centerCrop().into(holder.iv_cardrow_holder_foods_aserving);
        holder.iv_cardrow_holder_foods_aserving.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rAdapterFoodsAServingOnItemClick.onItemClick(holder, position, holder.tv_cardrowName_foods_aserving, String.valueOf(rParserFoodsAServing.getFoodCategoryId()));
            }
        });


        setAnimation(holder.itemView, position);

    }

    @Override
    public int getItemCount() {
        return rParserFoodsAServingList.size();
    }

    private void setAnimation(View itemView, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
            itemView.setAnimation(animation);
            lastPosition = position;
        }
    }

    public interface RAdapterFoodsAServingOnItemClick {
        void onItemClick(RecyclerView.ViewHolder holder, int position, TextView tv_cardrowName_foods_aserving, String s);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R2.id.iv_cardrow_holder_foods_aserving)
        ImageView iv_cardrow_holder_foods_aserving;

        @BindView(R2.id.tv_cardrowName_foods_aserving)
        TextView tv_cardrowName_foods_aserving;

        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.setDebug(true);
            ButterKnife.bind(this, itemView);


        }
    }

}
