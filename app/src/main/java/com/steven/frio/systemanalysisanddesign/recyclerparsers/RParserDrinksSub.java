package com.steven.frio.systemanalysisanddesign.recyclerparsers;

/**
 * Created by steven on 8/19/17.
 */

public class RParserDrinksSub {

    private Integer drinkCategoryId;
    private String drinkCategoryName;
    private String drinkCategoryImageURL;
    private Integer drinkCategoryPrice;
    private String drinkCategoryDetails;
    private String drinkOrderId;
    private String drinkOrderServing;
    private String drinkOrderCategory;

    public RParserDrinksSub(String drinkCategoryName, Integer drinkCategoryPrice, String drinkCategoryDetails, String drinkCategoryImageURL, String drinkOrderId, String drinkOrderServing, String drinkOrderCategory) {
        this.drinkCategoryName = drinkCategoryName;
        this.drinkCategoryImageURL = drinkCategoryImageURL;
        this.drinkCategoryPrice = drinkCategoryPrice;
        this.drinkCategoryDetails = drinkCategoryDetails;
        this.drinkOrderId = drinkOrderId;
        this.drinkOrderServing = drinkOrderServing;
        this.drinkOrderCategory = drinkOrderCategory;
    }


    public Integer getDrinkCategoryId() {
        return drinkCategoryId;
    }

    public String getDrinkCategoryName() {
        return drinkCategoryName;
    }

    public String getDrinkCategoryImageURL() {
        return drinkCategoryImageURL;
    }

    public Integer getDrinkCategoryPrice() {
        return drinkCategoryPrice;
    }

    public String getDrinkCategoryDetails() {
        return drinkCategoryDetails;
    }


    public String getDrinkOrderId() {
        return drinkOrderId;
    }


    public String getDrinkOrderServing() {
        return drinkOrderServing;
    }

    public void setDrinkOrderServing(String drinkOrderServing) {
        this.drinkOrderServing = drinkOrderServing;
    }

    public String getDrinkOrderCategory() {
        return drinkOrderCategory;
    }

    public void setDrinkOrderCategory(String drinkOrderCategory) {
        this.drinkOrderCategory = drinkOrderCategory;
    }
}
