package com.steven.frio.systemanalysisanddesign;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentDrinks extends Fragment {

    View view;

    public FragmentDrinks() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_drinks, container, false);

        loadFragmentDrinkServing();

        return view;
    }

    public void loadFragmentDrinkServing() {
        FragmentDrinksAServing fragmentDrinksAServing = new FragmentDrinksAServing();
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().add(R.id.fragmentContainerDrinks, fragmentDrinksAServing).commit();
    }

}
