package com.steven.frio.systemanalysisanddesign.recycleradapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.steven.frio.systemanalysisanddesign.R;
import com.steven.frio.systemanalysisanddesign.R2;
import com.steven.frio.systemanalysisanddesign.recyclerparsers.RParserAbout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by steven on 10/18/17.
 */

public class RAdapterAbout extends RecyclerView.Adapter<RAdapterAbout.ViewHolder>{

    Context context;
    List<RParserAbout> rParserAboutList;


    public RAdapterAbout(Context context, List<RParserAbout> rParserAboutList){
        this.context = context;
        this.rParserAboutList = rParserAboutList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cardview_holder_about, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        RParserAbout rParserAbout = rParserAboutList.get(position);
        holder.tv_aboutTitle.setText(rParserAbout.getTitle());
        holder.tv_aboutName.setText(rParserAbout.getName());

    }

    @Override
    public int getItemCount() {
        return rParserAboutList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R2.id.tv_aboutTitle)
        TextView tv_aboutTitle;

        @BindView(R2.id.tv_aboutName)
        TextView tv_aboutName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.setDebug(true);
            ButterKnife.bind(this, itemView);

        }
    }
}
