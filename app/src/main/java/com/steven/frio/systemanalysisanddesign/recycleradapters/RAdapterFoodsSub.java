package com.steven.frio.systemanalysisanddesign.recycleradapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.steven.frio.systemanalysisanddesign.R;
import com.steven.frio.systemanalysisanddesign.R2;
import com.steven.frio.systemanalysisanddesign.configs.StringConfig;
import com.steven.frio.systemanalysisanddesign.recyclerparsers.RParserFoodsSub;
import com.steven.frio.systemanalysisanddesign.sharedpreferences.SharedPreferencesManager;
import com.steven.frio.systemanalysisanddesign.sqlite.SQLiteCBLCAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by steven on 8/10/17.
 */

public class RAdapterFoodsSub extends RecyclerView.Adapter<RAdapterFoodsSub.ViewHolder> {

    Context context;
    List<RParserFoodsSub> rParserFoodsSubList;
    SQLiteCBLCAdapter sqLiteCBLCAdapter;
    DisplayFragmentDialog displayFragmentDialog;

    private int lastPosition = -1;

    public RAdapterFoodsSub(Context context, List<RParserFoodsSub> rParserFoodsSubList, DisplayFragmentDialog displayFragmentDialog) {
        this.context = context;
        this.rParserFoodsSubList = rParserFoodsSubList;
        this.displayFragmentDialog = displayFragmentDialog;
    }

    @Override
    public RAdapterFoodsSub.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cardview_holder_foods_sub, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RAdapterFoodsSub.ViewHolder holder, final int position) {
        final RParserFoodsSub rParserFoodsSub = rParserFoodsSubList.get(position);
        holder.tv_cardrowName_foods_sub.setText(rParserFoodsSub.getFoodCategoryName());
        holder.tv_cardrowDesc_foods_sub.setText(rParserFoodsSub.getFoodCategoryDetails());
        holder.tv_cardrowPrice_foods_sub.setText(rParserFoodsSub.getFoodCategoryPrice().toString());

        final String orderName = rParserFoodsSub.getFoodCategoryName();
        final String orderDetails = rParserFoodsSub.getFoodCategoryDetails();
        final String orderPrice = rParserFoodsSub.getFoodCategoryPrice().toString();
        final String orderId = rParserFoodsSub.getFoodOrderId();

        SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(context);

        final String name = sharedPreferencesManager.getSharedPrefFirstName();

        final String strImageUrl = StringConfig.PICASSO_URL + rParserFoodsSub.getFoodCategoryImageURL();

        if (!strImageUrl.contains("null")) {
            Log.d(StringConfig.LOG_TAG, "imageUrl : " + strImageUrl);
            holder.civ_cardrowImage_foods_sub.setBackgroundResource(0);
        }

        Picasso.with(context).load(strImageUrl).resize(200, 200).centerInside().into(holder.civ_cardrowImage_foods_sub);

        sqLiteCBLCAdapter = new SQLiteCBLCAdapter(context);

        holder.btn_cardrowAdd_foods.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sqLiteCBLCAdapter.selectQuantity(orderId).isEmpty()) {

                    Log.d(StringConfig.LOG_TAG, "sharedPrefName : " + name);
                    int initialQuantity = 1;
                    long id = sqLiteCBLCAdapter.insertData(name, rParserFoodsSub.getFoodCategoryName(), rParserFoodsSub.getFoodCategoryPrice(), initialQuantity, strImageUrl, "1", rParserFoodsSub.getFoodOrderId(), rParserFoodsSub.getFoodOrderServing(), rParserFoodsSub.getFoodOrderCategory(), "false");
                    if (id < 0) {
                        Toast.makeText(v.getContext(), "An error has occured", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(v.getContext(), rParserFoodsSub.getFoodCategoryName() + " added to cart.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    int quantity = Integer.parseInt(sqLiteCBLCAdapter.selectQuantity(orderId));
                    quantity++;
                    sqLiteCBLCAdapter.updateQuantity(orderId, String.valueOf(quantity));

                    /*
                    * update currentPrice column on DB
                    */
                    int selectBasePriceFromDb = Integer.parseInt(sqLiteCBLCAdapter.selectBasePrice(orderId));
                    String newPrice = String.valueOf(quantity * selectBasePriceFromDb);
                    sqLiteCBLCAdapter.updateCurrentPrice(orderId, newPrice);

                }

            }
        });

        holder.civ_cardrowImage_foods_sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayFragmentDialog.displayDialog(position, orderName, orderDetails, orderPrice, strImageUrl, orderId);
            }
        });

        setAnimation(holder.itemView, position);

    }

    @Override
    public int getItemCount() {
        return rParserFoodsSubList.size();
    }

    private void setAnimation(View itemView, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            itemView.startAnimation(animation);
            lastPosition = position;

        }
    }

    public interface DisplayFragmentDialog {
        void displayDialog(int position, String orderName, String orderDetails, String orderPrice, String strImageUrl, String orderId);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R2.id.civ_cardrowImage_foods_sub)
        CircleImageView civ_cardrowImage_foods_sub;

        @BindView(R2.id.tv_cardrowName_foods_sub)
        TextView tv_cardrowName_foods_sub;

        @BindView(R2.id.tv_cardrowDesc_foods_sub)
        TextView tv_cardrowDesc_foods_sub;

        @BindView(R2.id.tv_cardrowPrice_foods_sub)
        TextView tv_cardrowPrice_foods_sub;

        @BindView(R2.id.btn_cardrowAdd_foods_sub)
        Button btn_cardrowAdd_foods;

        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.setDebug(true);
            ButterKnife.bind(this, itemView);

        }
    }

}
