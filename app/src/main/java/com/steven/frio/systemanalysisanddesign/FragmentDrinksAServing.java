package com.steven.frio.systemanalysisanddesign;


import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.steven.frio.systemanalysisanddesign.configs.StringConfig;
import com.steven.frio.systemanalysisanddesign.recycleradapters.RAdapterDrinksAServing;
import com.steven.frio.systemanalysisanddesign.recyclerparsers.RParserDrinksAServing;
import com.steven.frio.systemanalysisanddesign.volley.VolleyNetworkRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentDrinksAServing extends Fragment {

    @BindView(R2.id.srl_drinksAServing)
    SwipeRefreshLayout srl_drinksAServing;
    @BindView(R2.id.rv_drinksAServing)
    RecyclerView rv_drinksAServing;
    RecyclerView.LayoutManager layoutManager;
    @BindView(R2.id.fragmentDrinksAServing)
    LinearLayout linearLayout;

    CoordinatorLayout cl_mainactivity;


    NetworkResponse networkResponse;

    List<RParserDrinksAServing> rParserDrinksAServingList = new ArrayList<>();
    RAdapterDrinksAServing rAdapterDrinksAServing;

    View view;

    public FragmentDrinksAServing() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_drinks_aserving, container, false);

        ButterKnife.setDebug(true);
        ButterKnife.bind(this, view);

        cl_mainactivity = (CoordinatorLayout) getActivity().findViewById(R.id.cl_mainactivity);

        loadJSONDrinksAServing();

        srl_drinksAServing.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                srl_drinksAServing.setRefreshing(true);
                loadJSONDrinksAServing();
                srl_drinksAServing.setRefreshing(false);

            }
        });

        return view;
    }

    private void loadJSONDrinksAServing() {
        rParserDrinksAServingList.clear();

        volleyGetDrinksAServingCategory();
        initRecyclerViews();
        initRecyclerViewDrinks();
    }

    private void volleyGetDrinksAServingCategory() {
        //final ProgressDialog progressDialog = new ProgressDialogMethods(getContext()).fetchingDataFromServer();
        srl_drinksAServing.setRefreshing(true);

        String strLoadDrinksAServingTempURL = StringConfig.BASE_URL + StringConfig.ROUTE_DRINKS_SERVING;

        //String strLoadDrinksAServingTempURL = "http://192.168.22.6/" + "atempvalues.json";

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());

        StringRequest stringRequest = new StringRequest(Request.Method.GET, strLoadDrinksAServingTempURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(StringConfig.LOG_TAG, "fragmentDrinkAServing " + response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        int jsonDrinkId = jsonObject.getInt("id");
                        String jsonDrinkTemp = jsonObject.getString("name");

                        JSONObject jsonObjectImage = jsonObject.getJSONObject("image");
                        String jsonImageUrl = StringConfig.BASE_URL + jsonObjectImage.getString("url");

                        Log.d(StringConfig.LOG_TAG, "" + jsonDrinkTemp);

                        RParserDrinksAServing rParserDrinksAServing = new RParserDrinksAServing(jsonDrinkTemp, jsonDrinkId, jsonImageUrl);
                        rParserDrinksAServingList.add(rParserDrinksAServing);
                        rAdapterDrinksAServing.notifyDataSetChanged();

                        //progressDialog.dismiss();
                        srl_drinksAServing.setRefreshing(false);

                    }

                } catch (JSONException e) {

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //progressDialog.dismiss();
                srl_drinksAServing.setRefreshing(false);

                new VolleyNetworkRequest().volleyErrorLogger(cl_mainactivity, error);

            }
        });
        requestQueue.add(stringRequest);

    }

    private void initRecyclerViews() {
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rv_drinksAServing.setLayoutManager(layoutManager);
    }

    private void initRecyclerViewDrinks() {
        rAdapterDrinksAServing = new RAdapterDrinksAServing(getContext(), rParserDrinksAServingList, new RAdapterDrinksAServing.RAdapterDrinksAServingOnItemClick() {
            @Override
            public void onItemClick(RecyclerView.ViewHolder holder, int position, TextView tv_cardrowName_drinks_aserving, String s) {

                String strSelectedTempValue = tv_cardrowName_drinks_aserving.getText().toString();

                Log.d(StringConfig.LOG_TAG, "strSelectedTempValue : " + strSelectedTempValue);
                FragmentDrinksMain fragmentDrinksMain = new FragmentDrinksMain();

                Bundle bundle = new Bundle();
                bundle.putString("setSelectedTempValueDrinks", strSelectedTempValue);
                bundle.putString("setSelectedTempValueDrinksIntValue", s);
                fragmentDrinksMain.setArguments(bundle);
                Log.d(StringConfig.LOG_TAG, "SSSSSSS VALUEEEE : " + s);
                getFragmentManager().beginTransaction().replace(R.id.fragmentContainerDrinks, fragmentDrinksMain).addToBackStack(null).commit();

            }
        });

        rv_drinksAServing.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        rv_drinksAServing.setAdapter(rAdapterDrinksAServing);
    }/*

    private void displayNetworkError(VolleyError error) {

        networkResponse = error.networkResponse;

        if(networkResponse != null && networkResponse.statusCode == 500){
            Snackbar snackbar = Snackbar.make(linearLayout, "Error 500. Internal server error.", Snackbar.LENGTH_SHORT);
            snackbar.show();
        }

        if (networkResponse != null && networkResponse.statusCode == 400) {
            Snackbar snackbar = Snackbar.make(linearLayout, "Check your credentials.", Snackbar.LENGTH_SHORT);
            snackbar.show();
        }

        if (networkResponse != null && networkResponse.statusCode == 404) {
            Snackbar snackbar = Snackbar.make(linearLayout, "Error 404. Server not found", Snackbar.LENGTH_SHORT);
            snackbar.show();
        }

        if (error instanceof NoConnectionError) {
            Snackbar snackbar = Snackbar.make(linearLayout, "No Connection.", Snackbar.LENGTH_SHORT);
            snackbar.show();
        }

        if (error instanceof TimeoutError) {
            Snackbar snackbar = Snackbar.make(linearLayout, "Connection Timed Out.", Snackbar.LENGTH_SHORT);
            snackbar.show();
        }

    }
*/
}
