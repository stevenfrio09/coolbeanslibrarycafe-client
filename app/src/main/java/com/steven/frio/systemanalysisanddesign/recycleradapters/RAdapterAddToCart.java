package com.steven.frio.systemanalysisanddesign.recycleradapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.steven.frio.systemanalysisanddesign.R;
import com.steven.frio.systemanalysisanddesign.configs.StringConfig;
import com.steven.frio.systemanalysisanddesign.recyclerparsers.AddToCartParser;
import com.steven.frio.systemanalysisanddesign.sqlite.SQLiteCBLCAdapter;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by steven on 7/30/17.
 */

public class RAdapterAddToCart extends RecyclerView.Adapter<RAdapterAddToCart.ViewHolder> {

    com.steven.frio.systemanalysisanddesign.interfaces.RAdapterCartInterface rAdapterCartInterface;
    SQLiteCBLCAdapter sqliteCBLCAdapter;
    private Context context;
    private List<AddToCartParser> addToCartParserList;

    public RAdapterAddToCart(Context context, List<AddToCartParser> addToCartParserList, com.steven.frio.systemanalysisanddesign.interfaces.RAdapterCartInterface rAdapterCartInterface) {
        this.context = context;
        this.addToCartParserList = addToCartParserList;
        this.rAdapterCartInterface = rAdapterCartInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cardview_holder_addtocart, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final AddToCartParser addToCartParser = addToCartParserList.get(position);

        sqliteCBLCAdapter = new SQLiteCBLCAdapter(context);

        final String orderName = addToCartParser.getOrderFromDb();
        final int quantity = addToCartParser.getQuantityFromDb();
        final String orderId = addToCartParser.getOrderIdFromDb();
        final String transactionType = addToCartParser.getTransactionType();

        if(transactionType.equals("true")){
            holder.et_quantity_atc.setEnabled(false);
            holder.tv_redeemedWithPoints.setText("Redeemed with points.");
        }

        holder.tv_cardrowName_atc.setText(addToCartParser.getOrderFromDb());
        holder.tv_currentPrice_atc.setText(addToCartParser.getPriceFromDb().toString());
        holder.et_quantity_atc.setText(addToCartParser.getQuantityFromDb().toString());
        holder.tv_transactionType_atc.setText(transactionType);
        holder.et_quantity_atc.setSelection(holder.et_quantity_atc.getText().length());

        Log.d(StringConfig.LOG_TAG, "transactionType holder : " + holder.tv_transactionType_atc.getText().toString());

        if (!addToCartParser.getImgResource().contains("null")) {
            holder.civ_cardrow_holder_atc.setBackgroundResource(0);
        }

        Picasso.with(context).load(addToCartParser.getImgResource()).into(holder.civ_cardrow_holder_atc);
        Log.d(StringConfig.LOG_TAG, "this url : " + StringConfig.PICASSO_URL + addToCartParser.getImgResource());


        holder.btn_orderAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rAdapterCartInterface.addButtonClick(holder.tv_cardrowName_atc, holder.tv_currentPrice_atc, holder.et_quantity_atc, position, orderId, transactionType);
            }
        });

        holder.btn_orderMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rAdapterCartInterface.subButtonClick(holder.tv_cardrowName_atc, holder.tv_currentPrice_atc, holder.et_quantity_atc, position, orderId, transactionType);
            }
        });

        holder.iv_cardRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rAdapterCartInterface.removeItem(position, orderName, orderId);
            }
        });

        try {
            final int basePrice = Integer.parseInt(sqliteCBLCAdapter.selectBasePrice(orderId));
            //editTextWatcher(holder, basePrice, orderName);
            rAdapterCartInterface.editTextWatcher(basePrice, orderName, holder.tv_currentPrice_atc, holder.et_quantity_atc, orderId, transactionType, holder.tv_transactionType_atc);
        } catch (NumberFormatException e) {

        }

    }

    @Override
    public int getItemCount() {
        return addToCartParserList.size();
    }

    public interface RAdapterCartInterface extends com.steven.frio.systemanalysisanddesign.interfaces.RAdapterCartInterface {

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView civ_cardrow_holder_atc;
        public ImageView iv_cardRemove;
        public TextView tv_cardrowName_atc, tv_currentPrice_atc, tv_transactionType_atc, tv_redeemedWithPoints;
        public EditText et_quantity_atc;
        public Button btn_orderMinus, btn_orderAdd;


        public ViewHolder(View itemView) {
            super(itemView);

            civ_cardrow_holder_atc = (CircleImageView) itemView.findViewById(R.id.civ_cardrow_holder_atc);
            tv_cardrowName_atc = (TextView) itemView.findViewById(R.id.tv_cardrowName_atc);
            tv_currentPrice_atc = (TextView) itemView.findViewById(R.id.tv_currentPrice_atc);
            iv_cardRemove = (ImageView) itemView.findViewById(R.id.iv_cardRemove);
            et_quantity_atc = (EditText) itemView.findViewById(R.id.et_quantity_atc);
            btn_orderMinus = (Button) itemView.findViewById(R.id.btn_orderMinus);
            btn_orderAdd = (Button) itemView.findViewById(R.id.btn_orderAdd);
            tv_transactionType_atc = (TextView)itemView.findViewById(R.id.tv_transactionType_atc);
            tv_redeemedWithPoints = (TextView)itemView.findViewById(R.id.tv_redeemedWithPoints);

        }
    }
}

