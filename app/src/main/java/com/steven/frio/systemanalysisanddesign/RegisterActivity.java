package com.steven.frio.systemanalysisanddesign;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.steven.frio.systemanalysisanddesign.configs.ProgressDialogMethods;
import com.steven.frio.systemanalysisanddesign.configs.StringConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class RegisterActivity extends AppCompatActivity {

    @BindView(R2.id.tet_regMobile)
    TextInputEditText tet_regMobile;

    @BindView(R2.id.tet_regPassword)
    TextInputEditText tet_regPassword;

    @BindView(R2.id.tet_regPasswordConfirm)
    TextInputEditText tet_regPasswordConfirm;

    @BindView(R2.id.tet_firstName)
    TextInputEditText tet_firstName;

    @BindView(R2.id.tet_lastName)
    TextInputEditText tet_lastName;

    @BindView(R2.id.btn_register)
    Button btn_register;

    @BindView(R2.id.tv_alreadyRegistered)
    TextView tv_alreadyRegistered;

    @BindView(R2.id.layout_register)
    FrameLayout layout_register;

    @BindView(R2.id.til_regMobile)
    TextInputLayout til_regMobile;

    @BindView(R2.id.til_regPassword)
    TextInputLayout til_regPassword;

    @BindView(R2.id.til_regPasswordConfirm)
    TextInputLayout til_regPasswordConfirm;

    @BindView(R2.id.til_firstName)
    TextInputLayout til_firstName;

    @BindView(R2.id.til_lastName)
    TextInputLayout til_lastName;


    String strRegMobile, strRegPassword, strRegPasswordConfirm, strFirstName, strLastName, strRegMobileMail;
    RequestQueue requestQueue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        ButterKnife.setDebug(true);
        ButterKnife.bind(this);

        tv_alreadyRegistered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });


        requestQueue = Volley.newRequestQueue(this);


        btn_register.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    validateForm();
                }
                return true;
            }
        });

        tet_regMobile.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (til_regMobile.isErrorEnabled()) {
                    til_regMobile.setErrorEnabled(false);
                }
                return false;
            }
        });

        tet_regPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (til_regPassword.isErrorEnabled()) {
                    til_regPassword.setErrorEnabled(false);
                }
                return false;
            }
        });

        tet_regPasswordConfirm.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (til_regPasswordConfirm.isErrorEnabled()) {
                    til_regPasswordConfirm.setErrorEnabled(false);
                }
                return false;
            }
        });

        tet_firstName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (til_firstName.isErrorEnabled()) {
                    til_firstName.setErrorEnabled(false);
                }
                return false;
            }
        });

        tet_lastName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (til_lastName.isErrorEnabled()) {
                    til_lastName.setErrorEnabled(false);
                }
                return false;
            }
        });

    }


    private void validateForm() {
        strRegMobile = tet_regMobile.getText().toString();
        strRegPassword = tet_regPassword.getText().toString().trim();
        strRegPasswordConfirm = tet_regPasswordConfirm.getText().toString();
        strFirstName = tet_firstName.getText().toString();
        strLastName = tet_lastName.getText().toString();

        strRegMobileMail = strRegMobile + "@yahoo.com";

        if (!isValid()) {
            //Toast.makeText(this, "An error has occurred", Toast.LENGTH_SHORT).show();
        } else {
            registerUser(strRegMobile, strRegPassword, strRegPasswordConfirm, strFirstName, strLastName, strRegMobileMail);
        }


    }

    private boolean isValid() {
        boolean valid = true;
        if (strRegMobile.isEmpty()) {
            til_regMobile.setError("This field is required.");
            valid = false;
        }
        if (strRegPassword.isEmpty()) {
            til_regPassword.setError("This field is required.");
            valid = false;
        }
        if (strRegPasswordConfirm.isEmpty()) {
            til_regPasswordConfirm.setError("This field is required.");
            valid = false;
        }
        if (strFirstName.isEmpty()) {
            til_firstName.setError("This field is required.");
            valid = false;
        }
        if (strLastName.isEmpty()) {
            til_lastName.setError("This field is required.");
            valid = false;
        }
        if (strRegMobile.length() < 11) {
            til_regMobile.setError("Not a valid mobile number.");
            valid = false;
        }
        if (strRegPassword.length() < 8) {
            til_regPassword.setError("Password must be 8 characters long.");
            valid = false;
        }
        if (strRegPasswordConfirm.length() < 8) {
            til_regPasswordConfirm.setError("Password must be 8 characters long.");
            valid = false;
        }
        if (!strRegPasswordConfirm.matches(strRegPassword)) {
            til_regPasswordConfirm.setError("Passwords does not match.");
            valid = false;
        }

        return valid;

    }


    private void registerUser(final String strRegMobile, final String strRegPassword, final String strRegPasswordConfirm, final String strFirstName, final String strLastName, final String strRegMobileMail) {
        Log.d(StringConfig.LOG_TAG, StringConfig.BASE_URL + StringConfig.ROUTE_REGISTER);

        final ProgressDialog progressDialog = new ProgressDialogMethods(this).pleaseWait();


        Map<String, String> mapRegister = new HashMap<String, String>();

        mapRegister.put("email", strRegMobileMail);
        mapRegister.put("phone_number", strRegMobile);
        mapRegister.put("first_name", strFirstName);
        mapRegister.put("last_name", strLastName);
        mapRegister.put("password", strRegPassword);
        mapRegister.put("password_confirmation", strRegPasswordConfirm);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, StringConfig.BASE_URL + StringConfig.ROUTE_REGISTER, new JSONObject(mapRegister), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    Log.d(StringConfig.LOG_TAG, "registerResponse : " + response.getString("result"));
                    String jsonObjectResultSuccess = response.getString("result");

                    if (jsonObjectResultSuccess.equalsIgnoreCase("success")) {
                        Toast.makeText(RegisterActivity.this, "Success", Toast.LENGTH_SHORT).show();

                        progressDialog.dismiss();

                        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                NetworkResponse networkResponse = error.networkResponse;

                if (networkResponse != null && networkResponse.statusCode == 400) {
                    Snackbar snackbar = Snackbar.make(layout_register, "Check your mobile number.", Snackbar.LENGTH_LONG);
                    snackbar.setAction("Clear fields.", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            tet_regMobile.setText("");
                            tet_regPassword.setText("");
                            tet_regPasswordConfirm.setText("");
                        }
                    });
                    snackbar.show();
                }

                if (networkResponse != null && networkResponse.statusCode == 404) {
                    Toast.makeText(RegisterActivity.this, StringConfig.ERROR_404, Toast.LENGTH_SHORT).show();
                }

                if (error instanceof NoConnectionError) {
                    Toast.makeText(RegisterActivity.this, StringConfig.ERROR_NO_CONNECTION, Toast.LENGTH_SHORT).show();
                }

                if (error instanceof TimeoutError) {
                    Toast.makeText(RegisterActivity.this, StringConfig.ERROR_CONNECTION_TIMED_OUT, Toast.LENGTH_SHORT).show();
                }


            }
        });
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}