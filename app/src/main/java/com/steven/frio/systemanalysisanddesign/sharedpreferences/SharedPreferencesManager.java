package com.steven.frio.systemanalysisanddesign.sharedpreferences;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.steven.frio.systemanalysisanddesign.LoginActivity;
import com.steven.frio.systemanalysisanddesign.configs.StringConfig;

/**
 * Created by steven on 8/2/17.
 */

public class SharedPreferencesManager {
    private static final String KEY_TOKEN = "Token";
    SharedPreferences sharedPreferences, sharedPreferencesOrdering, sharedPreferencesCustomerId;

    static SharedPreferences sharedPreferencesAdmin;
    static SharedPreferences.Editor editorAdmin;

    SharedPreferences.Editor editor, editorOrdering, editorCustomerId;
    Context context;

    public SharedPreferencesManager(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences("CBLCSharedPref", 0);
        sharedPreferencesOrdering = context.getSharedPreferences("CBLCOrderingSharedPref", 0);
        sharedPreferencesCustomerId = context.getSharedPreferences(StringConfig.SHAREDPREF_CUSTOMERID, 0);
        sharedPreferencesAdmin = context.getSharedPreferences(StringConfig.SHAREDPREF_ADMIN, 0);
        editor = sharedPreferences.edit();
        editorOrdering = sharedPreferencesOrdering.edit();
        editorCustomerId = sharedPreferencesCustomerId.edit();
        editorAdmin = sharedPreferencesAdmin.edit();

    }

    public void logoutUser() {

        editor.clear();
        editor.commit();

        editorOrdering.clear();
        editorOrdering.commit();

        editorCustomerId.clear();
        editorCustomerId.commit();

        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
    }

    public int getSharedPrefId() {
        return sharedPreferences.getInt("cust_id", 0);
    }

    public String getSharedPrefToken() {
        return sharedPreferences.getString("token", null);
    }

    public String getSharedPrefFirstName() {
        return sharedPreferences.getString("first_name", null);
    }

    public String getSharedPrefLastName() {
        return sharedPreferences.getString("last_name", null);
    }

    public String getSharedPrefMobileNumber() {
        return sharedPreferences.getString("phone_number", null);
    }

    public Float getSharedPrefPoints(){
        return sharedPreferences.getFloat("points", 0.0f);
    }

    public static String getSharedPrefIP(){

        Log.d(StringConfig.LOG_TAG, "getIP from sharedpref : " + sharedPreferencesAdmin.getString("ipaddress", null));

        return sharedPreferencesAdmin.getString("ipaddress", null);
    }

    public static void setSharedPrefIP(String ipaddress){
        editorAdmin.putString("ipaddress",ipaddress);
        editorAdmin.apply();
    }
}

