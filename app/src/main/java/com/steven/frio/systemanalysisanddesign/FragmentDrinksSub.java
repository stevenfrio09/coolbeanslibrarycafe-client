package com.steven.frio.systemanalysisanddesign;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.NetworkResponse;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.steven.frio.systemanalysisanddesign.configs.StringConfig;
import com.steven.frio.systemanalysisanddesign.recycleradapters.RAdapterDrinksSub;
import com.steven.frio.systemanalysisanddesign.recyclerparsers.RParserDrinksSub;
import com.steven.frio.systemanalysisanddesign.volley.VolleyGetterSetter;
import com.steven.frio.systemanalysisanddesign.volley.VolleyNetworkRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentDrinksSub extends Fragment {

    @BindView(R2.id.srl_drinksSub)
    SwipeRefreshLayout srl_drinksSub;

    @BindView(R2.id.rv_drinksSub)
    RecyclerView rv_drinksSub;

    @BindView(R2.id.fragmentDrinksSub)
    LinearLayout linearLayout;

    CoordinatorLayout cl_mainactivity;

    SharedPreferences sharedPreferences;
    String sharedPrefValueToken;
    VolleyGetterSetter volleyGetterSetter;
    List<RParserDrinksSub> rParserDrinksSubList = new ArrayList<>();
    RAdapterDrinksSub rAdapterDrinksSub;
    RecyclerView.LayoutManager layoutManager;
    NetworkResponse networkResponse;


    //string bundle
    String getSelectedCategoryValue, getSelectedServingValue;
    String getSelectedServingValueIntValue, getSelectedCategoryValueIntValue;

    View view;

    public FragmentDrinksSub() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_drinks_sub, container, false);

        ButterKnife.setDebug(true);
        ButterKnife.bind(this, view);

        cl_mainactivity = (CoordinatorLayout) getActivity().findViewById(R.id.cl_mainactivity);

        sharedPreferences = getContext().getSharedPreferences(StringConfig.SHAREDPREF_NAME, 0);
        sharedPrefValueToken = sharedPreferences.getString("token", null);
        volleyGetterSetter = new VolleyGetterSetter(getContext());
        volleyGetterSetter.setToken(sharedPrefValueToken);


        Bundle bundle = this.getArguments();
        if (bundle != null) {
            getSelectedServingValue = bundle.getString("setSelectedServingValueDrinks");
            getSelectedCategoryValue = bundle.getString("setSelectedCategoryValueDrinks");
            getSelectedServingValueIntValue = bundle.getString("setSelectedServingValueDrinksIntValue");
            getSelectedCategoryValueIntValue = bundle.getString("setSelectCategoryValueDrinksIntValue");


            Log.d(StringConfig.LOG_TAG, "serving value : " + getSelectedServingValue);
            Log.d(StringConfig.LOG_TAG, "category value : " + getSelectedCategoryValue);
            Log.d(StringConfig.LOG_TAG, "serving value int : " + getSelectedServingValueIntValue);
            Log.d(StringConfig.LOG_TAG, "category value int : " + getSelectedCategoryValueIntValue);


        }
        loadJSONDrinksSub();


        srl_drinksSub.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                srl_drinksSub.setRefreshing(true);
                loadJSONDrinksSub();
                srl_drinksSub.setRefreshing(false);

            }
        });

        return view;
    }

    private void loadJSONDrinksSub() {
        rParserDrinksSubList.clear();
        volleyGetDrinksSubCategory();
        initRecyclerViews();
        initRecyclerViewDrink();
    }


    private void volleyGetDrinksSubCategory() {

        //final ProgressDialog progressDialog = new ProgressDialogMethods(getContext()).fetchingDataFromServer();
        srl_drinksSub.setRefreshing(true);

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());

        String strLoadDrinkSubCatURL = StringConfig.BASE_URL + StringConfig.ROUTE_DRINKS_SHOWALL;

        //String strLoadDrinkSubCatURL = "http://192.168.22.6/Drink.json";

        StringRequest stringRequest = new StringRequest(strLoadDrinkSubCatURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.d(StringConfig.LOG_TAG, "response response : " + response.toString());

                    for (int i = 0; i <= jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);


                        JSONObject jsonObjectDrinkTemp = jsonObject.getJSONObject("drink_serving_temperature");
                        String strJsonDrinkTempName = jsonObjectDrinkTemp.getString("name");


                        JSONObject jsonObjectDrinkCategory = jsonObject.getJSONObject("drink_category");
                        String strJsonDrinkCatName = jsonObjectDrinkCategory.getString("name");

                        Log.d(StringConfig.LOG_TAG, "strJsonDrinkTempId : " + strJsonDrinkTempName);
                        Log.d(StringConfig.LOG_TAG, "strJsonDrinkCatId : " + strJsonDrinkCatName);

                        if (getSelectedServingValue.equals(strJsonDrinkTempName)) {
                            if (getSelectedCategoryValue.equals(strJsonDrinkCatName)) {

                                String strJsonDrinkMainId = String.valueOf(jsonObject.getInt("id"));
                                String strJsonDrinkMainName = jsonObject.getString("name");
                                int strJsonDrinkMainPrice = jsonObject.getInt("price");
                                String strJsonDrinkMainDetail = jsonObject.getString("detail");

                                JSONObject jsonObjectDrinkMainImage = jsonObject.getJSONObject("image");
                                String strJsonDrinkMainImageUrl = jsonObjectDrinkMainImage.getString("url");


                                Log.d(StringConfig.LOG_TAG, "strJsonDrinkTempId : " + strJsonDrinkTempName);
                                Log.d(StringConfig.LOG_TAG, "strJsonDrinkCatId : " + strJsonDrinkCatName);

                                Log.d(StringConfig.LOG_TAG, "strJsonDrinkMainId : " + strJsonDrinkMainId);
                                Log.d(StringConfig.LOG_TAG, "strJsonDrinkMainName : " + strJsonDrinkMainName);
                                Log.d(StringConfig.LOG_TAG, "strJsonDrinkMainPrice : " + strJsonDrinkMainPrice);
                                Log.d(StringConfig.LOG_TAG, "strJsonDrinkMainDetail : " + strJsonDrinkMainDetail);

                                RParserDrinksSub rParserDrinksSub = new RParserDrinksSub(strJsonDrinkMainName, strJsonDrinkMainPrice, strJsonDrinkMainDetail, strJsonDrinkMainImageUrl, strJsonDrinkMainId, getSelectedServingValueIntValue, getSelectedCategoryValueIntValue);
                                rParserDrinksSubList.add(rParserDrinksSub);
                                rAdapterDrinksSub.notifyDataSetChanged();

                            } else {
                                continue;
                            }
                        } else {
                            continue;
                        }


                        //progressDialog.dismiss();
                        srl_drinksSub.setRefreshing(false);

                    }


                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //progressDialog.dismiss();
                srl_drinksSub.setRefreshing(false);
                new VolleyNetworkRequest().volleyErrorLogger(cl_mainactivity, error);
            }
        });


        requestQueue.add(stringRequest);

    }

    private void initRecyclerViewDrink() {
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rv_drinksSub.setLayoutManager(layoutManager);
    }

    private void initRecyclerViews() {
        rAdapterDrinksSub = new RAdapterDrinksSub(getContext(), rParserDrinksSubList, new RAdapterDrinksSub.DisplayFragmentDialog() {
            @Override
            public void displayDialog(int position, String orderName, String orderDetails, String orderPrice, String strImageUrl, String orderId) {
                FragmentManager fragmentManager = getFragmentManager();
                DialogFragmentOrdering dialogFragmentOrdering = new DialogFragmentOrdering();
                dialogFragmentOrdering.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);

                Bundle bundle = new Bundle();
                bundle.putString("name", orderName);
                bundle.putString("details", orderDetails);
                bundle.putString("price", orderPrice);
                bundle.putString("imageUrl", strImageUrl);
                bundle.putString("orderId", orderId);
                bundle.putString("orderType", "2");
                bundle.putString("serving", getSelectedServingValueIntValue);
                bundle.putString("category", getSelectedCategoryValueIntValue);

                dialogFragmentOrdering.setArguments(bundle);

                dialogFragmentOrdering.show(fragmentManager, "DialogFragmentOrdering");
            }
        });

        //rv_drinksSub.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        rv_drinksSub.setAdapter(rAdapterDrinksSub);
    }


}
