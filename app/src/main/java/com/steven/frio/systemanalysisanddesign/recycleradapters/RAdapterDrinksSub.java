package com.steven.frio.systemanalysisanddesign.recycleradapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.steven.frio.systemanalysisanddesign.R;
import com.steven.frio.systemanalysisanddesign.R2;
import com.steven.frio.systemanalysisanddesign.configs.StringConfig;
import com.steven.frio.systemanalysisanddesign.recyclerparsers.RParserDrinksSub;
import com.steven.frio.systemanalysisanddesign.sharedpreferences.SharedPreferencesManager;
import com.steven.frio.systemanalysisanddesign.sqlite.SQLiteCBLCAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by steven on 8/19/17.
 */

public class RAdapterDrinksSub extends RecyclerView.Adapter<RAdapterDrinksSub.ViewHolder> {

    Context context;
    List<RParserDrinksSub> rParserDrinksSubList;
    SQLiteCBLCAdapter sqLiteCBLCAdapter;
    DisplayFragmentDialog displayFragmentDialog;
    int lastPosition = -1;

    public RAdapterDrinksSub(Context context, List<RParserDrinksSub> rParserDrinksSubList, DisplayFragmentDialog displayFragmentDialog) {
        this.context = context;
        this.rParserDrinksSubList = rParserDrinksSubList;
        this.displayFragmentDialog = displayFragmentDialog;
    }

    @Override
    public RAdapterDrinksSub.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cardview_holder_drinks_sub, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RAdapterDrinksSub.ViewHolder holder, final int position) {
        final RParserDrinksSub rParserDrinksSub = rParserDrinksSubList.get(position);
        holder.tv_cardrowName_drinks_sub.setText(rParserDrinksSub.getDrinkCategoryName());
        holder.tv_cardrowDesc_drinks_sub.setText(rParserDrinksSub.getDrinkCategoryDetails());
        holder.tv_cardrowPrice_drinks_sub.setText(rParserDrinksSub.getDrinkCategoryPrice().toString());

        final String orderName = rParserDrinksSub.getDrinkCategoryName();
        final String orderDetails = rParserDrinksSub.getDrinkCategoryDetails();
        final String orderPrice = rParserDrinksSub.getDrinkCategoryPrice().toString();
        final String orderId = rParserDrinksSub.getDrinkOrderId().toString();


        final String strImageUrl = StringConfig.PICASSO_URL + rParserDrinksSub.getDrinkCategoryImageURL();


        if (!strImageUrl.contains("null")) {
            Log.d(StringConfig.LOG_TAG, "imageUrl : " + strImageUrl);
            holder.civ_cardrowImage_drinks_sub.setBackgroundResource(0);
        }

        Picasso.with(context).load(strImageUrl).resize(200, 200).centerInside().into(holder.civ_cardrowImage_drinks_sub);

        sqLiteCBLCAdapter = new SQLiteCBLCAdapter(context);
        SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(context);

        final String name = sharedPreferencesManager.getSharedPrefFirstName();

        holder.btn_cardrowAdd_drinks_sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sqLiteCBLCAdapter.selectQuantity(orderId).isEmpty()) {

                    Log.d(StringConfig.LOG_TAG, "sharedPrefName : " + name);
                    int initialQuantity = 1;
                    long id = sqLiteCBLCAdapter.insertData(name, rParserDrinksSub.getDrinkCategoryName(), rParserDrinksSub.getDrinkCategoryPrice(), initialQuantity, strImageUrl, "2", rParserDrinksSub.getDrinkOrderId(), rParserDrinksSub.getDrinkOrderServing(), rParserDrinksSub.getDrinkOrderCategory(), "false");
                    if (id < 0) {
                        Toast.makeText(v.getContext(), "An error has occured", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(v.getContext(), rParserDrinksSub.getDrinkCategoryName() + " added to cart.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    int quantity = Integer.parseInt(sqLiteCBLCAdapter.selectQuantity(orderId));
                    quantity++;
                    sqLiteCBLCAdapter.updateQuantity(orderId, String.valueOf(quantity));

                    /*
                    * update currentPrice column on DB
                    */
                    int selectBasePriceFromDb = Integer.parseInt(sqLiteCBLCAdapter.selectBasePrice(orderId));
                    String newPrice = String.valueOf(quantity * selectBasePriceFromDb);
                    sqLiteCBLCAdapter.updateCurrentPrice(orderId, newPrice);

                }
            }
        });

        holder.civ_cardrowImage_drinks_sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayFragmentDialog.displayDialog(position, orderName, orderDetails, orderPrice, strImageUrl, orderId);
            }
        });

        setAnimation(holder.itemView, position);

    }

    @Override
    public int getItemCount() {
        return rParserDrinksSubList.size();
    }

    private void setAnimation(View itemView, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            itemView.startAnimation(animation);
            lastPosition = position;

        }
    }

    public interface DisplayFragmentDialog {
        void displayDialog(int position, String orderName, String orderDetails, String orderPrice, String strImageUrl, String orderId);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R2.id.civ_cardrowImage_drinks_sub)
        CircleImageView civ_cardrowImage_drinks_sub;
        @BindView(R2.id.tv_cardrowName_drinks_sub)
        TextView tv_cardrowName_drinks_sub;
        @BindView(R2.id.tv_cardrowDesc_drinks_sub)
        TextView tv_cardrowDesc_drinks_sub;
        @BindView(R2.id.tv_cardrowPrice_drinks_sub)
        TextView tv_cardrowPrice_drinks_sub;
        @BindView(R2.id.btn_cardrowAdd_drinks_sub)
        Button btn_cardrowAdd_drinks_sub;

        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            ButterKnife.setDebug(true);
        }
    }
}
