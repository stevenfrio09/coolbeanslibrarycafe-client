package com.steven.frio.systemanalysisanddesign.volley;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.steven.frio.systemanalysisanddesign.configs.StringConfig;

/**
 * Created by steven on 10/8/17.
 */

public class VolleyNetworkRequest {
    public void volleyErrorLogger(CoordinatorLayout cl_mainactivity, VolleyError volleyError) {
        NetworkResponse networkResponse = volleyError.networkResponse;

        if (networkResponse != null && networkResponse.statusCode == 500) {
            Snackbar snackbar = Snackbar.make(cl_mainactivity, StringConfig.ERROR_500, Snackbar.LENGTH_SHORT);
            snackbar.show();
        }

        if (networkResponse != null && networkResponse.statusCode == 400) {
            Snackbar snackbar = Snackbar.make(cl_mainactivity, StringConfig.ERROR_400, Snackbar.LENGTH_SHORT);
            snackbar.show();
        }

        if (networkResponse != null && networkResponse.statusCode == 404) {
            Snackbar snackbar = Snackbar.make(cl_mainactivity, StringConfig.ERROR_404, Snackbar.LENGTH_SHORT);
            snackbar.show();
        }

        if (volleyError instanceof NoConnectionError) {
            Snackbar snackbar = Snackbar.make(cl_mainactivity, StringConfig.ERROR_NO_CONNECTION, Snackbar.LENGTH_SHORT);
            snackbar.show();
        }

        if (volleyError instanceof TimeoutError) {
            Snackbar snackbar = Snackbar.make(cl_mainactivity, StringConfig.ERROR_CONNECTION_TIMED_OUT, Snackbar.LENGTH_SHORT);
            snackbar.show();
        }


    }
}
