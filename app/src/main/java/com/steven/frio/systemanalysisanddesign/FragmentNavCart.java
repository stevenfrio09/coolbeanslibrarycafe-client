package com.steven.frio.systemanalysisanddesign;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.steven.frio.systemanalysisanddesign.configs.StringConfig;
import com.steven.frio.systemanalysisanddesign.recycleradapters.RAdapterAddToCart;
import com.steven.frio.systemanalysisanddesign.recyclerparsers.AddToCartParser;
import com.steven.frio.systemanalysisanddesign.sharedpreferences.SharedPreferencesManager;
import com.steven.frio.systemanalysisanddesign.sqlite.SQLiteCBLCAdapter;
import com.steven.frio.systemanalysisanddesign.volley.ProfileSettings;
import com.steven.frio.systemanalysisanddesign.volley.VolleyNetworkRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentNavCart extends Fragment {

    RecyclerView.LayoutManager layoutManager;
    RAdapterAddToCart rAdapterAddToCart;
    SQLiteCBLCAdapter sqliteCBLCAdapter;
    SQLiteCBLCAdapter.SQLiteCBLC sqLiteCBLC;
    List<AddToCartParser> addToCartParserList = new ArrayList<>();

    //--SQLite related variables
    String getOrder = "";
    int getPrice = 0;
    int getBasePrice = 0;
    int getQuantity = 0;
    int getOrderId = 0;
    String getOrderType = "";
    String getImageResource = "";
    String getTransactionType = "";
    //


    @BindView(R2.id.rv_addToCart)
    RecyclerView rv_addToCart;

    @BindView(R2.id.cv_sendOrder)
    CardView cv_sendOrder;

    @BindView(R2.id.tv_cartAmount)
    TextView tv_cartAmount;

    @BindView(R2.id.tv_yourOrder)
    TextView tv_yourOrder;

    CoordinatorLayout cl_mainactivity;

    SQLiteDatabase sqLiteDatabase;

    SharedPreferences sharedPreferences, sharedPreferencesOrdering;
    SharedPreferences.Editor editor, editorOrdering;
    SharedPreferencesManager sharedPreferencesManager;

    View view;

    public FragmentNavCart() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_nav_cart, container, false);

        ButterKnife.setDebug(true);
        ButterKnife.bind(this, view);

        cl_mainactivity = (CoordinatorLayout) getActivity().findViewById(R.id.cl_mainactivity);

        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        tv_cartAmount.setPaintFlags(tv_cartAmount.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        sqliteCBLCAdapter = new SQLiteCBLCAdapter(getContext());
        sqLiteCBLC = new SQLiteCBLCAdapter.SQLiteCBLC(getContext());
        sqLiteDatabase = sqLiteCBLC.getWritableDatabase();
        sharedPreferences = getContext().getSharedPreferences(StringConfig.SHAREDPREF_NAME, 0);
        sharedPreferencesOrdering = getContext().getSharedPreferences(StringConfig.SHAREDPREF_ORDERING, 0);
        sharedPreferencesManager = new SharedPreferencesManager(getContext());
        editorOrdering = sharedPreferencesOrdering.edit();
        editorOrdering.clear();
        editorOrdering.apply();

        displayOrdersFromDB();
        initRecyclerViewAdapterDB();
        setTotalAmount();
        setYourOrderPlurality();

        cv_sendOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (String.valueOf(getOrderId).equals("") && String.valueOf(getQuantity).equals("") || getOrderId == 0 && getQuantity == 0) {

                    Toast.makeText(getContext(), "You must order first.", Toast.LENGTH_SHORT).show();

                } else {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Finalized order.");
                    builder.setMessage("Are you sure ? ");
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            postToServerDrink();
                            postToServerFood();

                        }
                    }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });

                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }

            }
        });

        return view;
    }


    private void displayOrdersFromDB() {

        String readQuery = "SELECT * FROM " + SQLiteCBLCAdapter.SQLiteCBLC.TABLE_NAME + "";

        Cursor cursor = sqLiteDatabase.rawQuery(readQuery, null);

        while (cursor.moveToNext()) {

            getOrder = cursor.getString(cursor.getColumnIndex(SQLiteCBLCAdapter.SQLiteCBLC.COL_ORDNAME));
            getPrice = cursor.getInt(cursor.getColumnIndex(SQLiteCBLCAdapter.SQLiteCBLC.COL_PRICECURRENT));
            getBasePrice = cursor.getInt(cursor.getColumnIndex(SQLiteCBLCAdapter.SQLiteCBLC.COL_PRICEBASE));
            getQuantity = cursor.getInt(cursor.getColumnIndex(SQLiteCBLCAdapter.SQLiteCBLC.COL_QUANTITY));
            getImageResource = cursor.getString(cursor.getColumnIndex(SQLiteCBLCAdapter.SQLiteCBLC.COL_IMGRESC));
            getOrderType = cursor.getString(cursor.getColumnIndex(SQLiteCBLCAdapter.SQLiteCBLC.COL_ORDERTYPE));
            getOrderId = cursor.getInt(cursor.getColumnIndex(SQLiteCBLCAdapter.SQLiteCBLC.COL_ORDERID));
            getTransactionType = cursor.getString(cursor.getColumnIndex(SQLiteCBLCAdapter.SQLiteCBLC.COL_PAID_WITH_POINTS));

            Log.d(StringConfig.LOG_TAG, "getOrderId : " + getOrderId);
            Log.d(StringConfig.LOG_TAG, "getQuantity : " + getQuantity);
            Log.d(StringConfig.LOG_TAG, "getOrderType : " + getOrderType);
            Log.d(StringConfig.LOG_TAG, "getTransactionType : " + getTransactionType);

            //orderNow(getOrderId, getOrderType, getQuantity);

            int price = getBasePrice * getQuantity;

            AddToCartParser addToCartParser = new AddToCartParser(getOrder, price, getQuantity, getImageResource, String.valueOf(getOrderId), getTransactionType);
            addToCartParserList.add(addToCartParser);

        }

        cursor.close();

    }

    private void initRecyclerViewAdapterDB() {

        rAdapterAddToCart = new RAdapterAddToCart(getContext(), addToCartParserList, new RAdapterAddToCart.RAdapterCartInterface() {
            @Override
            public void addButtonClick(TextView tv_cardrowName_atc, TextView tv_currentPrice_atc, EditText et_quantity_atc, int position, String orderId, String transactionType) {

                String orderName = tv_cardrowName_atc.getText().toString();

                try {
                    if (transactionType.equals("false")) {
                        //update Quantity column on DB
                        int thisQuantity = Integer.parseInt(et_quantity_atc.getText().toString());
                        thisQuantity++;
                        updateDBPriceQuantity(tv_currentPrice_atc, et_quantity_atc, orderName, thisQuantity, orderId, transactionType);

                        setTotalAmount();
                    } else {
                        int points = (int) Float.parseFloat(String.valueOf(sharedPreferencesManager.getSharedPrefPoints()));
                        int quotient = points / 10;

                        int quantity = Integer.parseInt(sqliteCBLCAdapter.selectQuantityOrderWithPointsIsTrue(orderId));

                        if (quantity < quotient) {
                            int thisQuantity = Integer.parseInt(et_quantity_atc.getText().toString());
                            thisQuantity++;
                            updateDBPriceQuantity(tv_currentPrice_atc, et_quantity_atc, orderName, thisQuantity, orderId, transactionType);

                            setTotalAmount();
                        }

                    }

                } catch (NumberFormatException e) {
                    catchNumberFormatException(tv_currentPrice_atc, et_quantity_atc, orderName, orderId, transactionType);
                }

            }

            @Override
            public void subButtonClick(TextView tv_cardrowName_atc, TextView tv_currentPrice_atc, EditText et_quantity_atc, int position, String orderId, String transactionType) {

                String orderName = tv_cardrowName_atc.getText().toString();

                try {
                    int thisQuantity = Integer.parseInt(et_quantity_atc.getText().toString());

                    if (thisQuantity > 1) {
                        //update Quantity column  on DB

                        thisQuantity--;

                        updateDBPriceQuantity(tv_currentPrice_atc, et_quantity_atc, orderName, thisQuantity, orderId, transactionType);

                        setTotalAmount();

                    }
                } catch (NumberFormatException e) {
                    catchNumberFormatException(tv_currentPrice_atc, et_quantity_atc, orderName, orderId, transactionType);
                }
            }

            @Override
            public void editTextWatcher(final int basePrice, final String orderName, final TextView tv_currentPrice_atc, final EditText et_quantity_atc, final String orderId, final String transactionType, final TextView tv_transactionType_atc) {
                et_quantity_atc.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        try {
                            String ifZero = s.toString();

                            if (ifZero.equals("0")) {

                                et_quantity_atc.setText("1");
                                et_quantity_atc.setSelection(et_quantity_atc.getText().length());
                                tv_currentPrice_atc.setText(String.valueOf(basePrice));
                                if (transactionType.equals("false")) {

                                    sqliteCBLCAdapter.updateQuantity(orderId, "1");
                                    sqliteCBLCAdapter.updateCurrentPrice(orderId, String.valueOf(basePrice));
                                } else {
                                    sqliteCBLCAdapter.updateQuantityOrderWithPointsIsTrue(orderId, "1");
                                    sqliteCBLCAdapter.updateCurrentPriceWithPointsIsTrue(orderId, String.valueOf(basePrice));
                                }

                                setTotalAmount();

                            } else {
                                int currentQuantity = Integer.parseInt(s.toString());
                                int finalPrice = basePrice * currentQuantity;
                                tv_currentPrice_atc.setText(String.valueOf(finalPrice));
                                String newQuantity = s.toString();
                                Log.d(StringConfig.LOG_TAG, "newQuantity : " + newQuantity);

                                if (transactionType.equals("false")) {
                                    sqliteCBLCAdapter.updateQuantity(orderId, newQuantity);
                                    sqliteCBLCAdapter.updateCurrentPrice(orderId, String.valueOf(finalPrice));
                                } else {
                                    sqliteCBLCAdapter.updateQuantityOrderWithPointsIsTrue(orderId, newQuantity);
                                    sqliteCBLCAdapter.updateCurrentPriceWithPointsIsTrue(orderId, String.valueOf(finalPrice));
                                }

                                setTotalAmount();
                            }


                        } catch (NumberFormatException e) {
                            catchNumberFormatException(tv_currentPrice_atc, et_quantity_atc, orderName, orderId, transactionType);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });
            }

            @Override
            public void removeItem(int position, String orderName, String orderId) {

                addToCartParserList.remove(position);
                rAdapterAddToCart.notifyItemRemoved(position);
                rAdapterAddToCart.notifyItemRangeChanged(position, addToCartParserList.size());
                if (getTransactionType.equals("false")) {
                    sqliteCBLCAdapter.deleteOrder(orderId);
                } else {
                    sqliteCBLCAdapter.deleteOrderWithPointsIsTrue(orderId);
                }
                setTotalAmount();
                setYourOrderPlurality();
            }

        });

        rv_addToCart.setAdapter(rAdapterAddToCart);
        rv_addToCart.setLayoutManager(layoutManager);
        rAdapterAddToCart.notifyDataSetChanged();

    }

    private void updateDBPriceQuantity(TextView tv_currentPrice_atc, EditText et_quantity_atc, String orderName, int thisQuantity, String orderId, String transactionType) {
        et_quantity_atc.setText(String.valueOf(thisQuantity));
        et_quantity_atc.setSelection(et_quantity_atc.getText().length());

        String newQuantity = String.valueOf(thisQuantity);
        if (transactionType.equals("false")) {
            sqliteCBLCAdapter.updateQuantity(orderId, newQuantity);

            //update CurrentPrice column on DB
            String basePrice = sqliteCBLCAdapter.selectBasePrice(orderId);
            String quantity = sqliteCBLCAdapter.selectQuantity(orderId);
            int newPrice = Integer.parseInt(basePrice) * Integer.parseInt(quantity);
            tv_currentPrice_atc.setText(String.valueOf(newPrice));

            sqliteCBLCAdapter.updateCurrentPrice(orderId, String.valueOf(newPrice));

        } else {
            sqliteCBLCAdapter.updateQuantityOrderWithPointsIsTrue(orderId, newQuantity);

            //update CurrentPrice column on DB
            String basePrice = sqliteCBLCAdapter.selectBasePriceWithPointsIsTrue(orderId);
            String quantity = sqliteCBLCAdapter.selectQuantityOrderWithPointsIsTrue(orderId);
            int newPrice = Integer.parseInt(basePrice) * Integer.parseInt(quantity);
            tv_currentPrice_atc.setText(String.valueOf(newPrice));

            sqliteCBLCAdapter.updateCurrentPriceWithPointsIsTrue(orderId, String.valueOf(newPrice));
        }


    }

    private void catchNumberFormatException(TextView tv_currentPrice_atc, EditText et_quantity_atc, String orderName, String orderId, String transactionType) {
        Log.d(StringConfig.LOG_TAG, "number format exception called");

        if (transactionType.equals("false")) {

            String basePrice = sqliteCBLCAdapter.selectBasePrice(orderId);

            tv_currentPrice_atc.setText(basePrice);
            et_quantity_atc.setText(String.valueOf(1));
            et_quantity_atc.setSelection(et_quantity_atc.getText().length());

            sqliteCBLCAdapter.updateCurrentPrice(orderId, basePrice);
            sqliteCBLCAdapter.updateQuantity(orderId, String.valueOf(1));
            setTotalAmount();
        } else {
            String basePrice = sqliteCBLCAdapter.selectBasePriceWithPointsIsTrue(orderId);

            tv_currentPrice_atc.setText(basePrice);
            et_quantity_atc.setText(String.valueOf(1));
            et_quantity_atc.setSelection(et_quantity_atc.getText().length());

            sqliteCBLCAdapter.updateCurrentPriceWithPointsIsTrue(orderId, basePrice);
            sqliteCBLCAdapter.updateQuantityOrderWithPointsIsTrue(orderId, String.valueOf(1));
            setTotalAmount();
        }
    }

    private void setYourOrderPlurality() {
        if (addToCartParserList.size() <= 1) {
            tv_yourOrder.setText("Your Order");
        } else {
            tv_yourOrder.setText("Your Orders");
        }
    }

    public void setTotalAmount() {
        String currentPrice = String.valueOf(sqliteCBLCAdapter.selectCurrentPrice());
        tv_cartAmount.setText(String.valueOf(currentPrice));
    }

    private void postToServerDrink() {
        String orderTypeIs1Query = "SELECT " + SQLiteCBLCAdapter.SQLiteCBLC.COL_ORDERID + ", " + SQLiteCBLCAdapter.SQLiteCBLC.COL_ORDERTYPE + ", " + SQLiteCBLCAdapter.SQLiteCBLC.COL_ORDERSERVING + ", " + SQLiteCBLCAdapter.SQLiteCBLC.COL_ORDERCATEGORY + ", " + SQLiteCBLCAdapter.SQLiteCBLC.COL_QUANTITY + ", " + SQLiteCBLCAdapter.SQLiteCBLC.COL_PRICECURRENT + ", " + SQLiteCBLCAdapter.SQLiteCBLC.COL_PAID_WITH_POINTS + "" + " FROM " + SQLiteCBLCAdapter.SQLiteCBLC.TABLE_NAME + " WHERE " + SQLiteCBLCAdapter.SQLiteCBLC.COL_ORDERTYPE + " = '2'";
        Cursor cursor = sqLiteDatabase.rawQuery(orderTypeIs1Query, null);
        String orderingParams = "drink_id";

        //orderingParams
        String paramsOrderType = "orderType";
        String paramsCustomerId = "customer_id";
        String paramsOrderId = "drink_id";
        String paramsOrderCategoryId = "drink_category_id";
        String paramsOrderServing = "drink_serving_temperature_id";
        String paramsOrderQuantity = "quantity";
        String paramsIsPaidWithPoints = "is_paid_with_points?";

        String valueOrderId = ""; // always blank
        String valueOrderType = "Drink";
        String valueOrderCategoryId = "";
        String valueOrderServing = "";
        String valueIsPaidWithPoints = "";

        int categoryId = 0;
        String currentPrice = "";
        int valueOrderQuantity = 0;

        while (cursor.moveToNext()) {
            categoryId = cursor.getInt(cursor.getColumnIndex(SQLiteCBLCAdapter.SQLiteCBLC.COL_ORDERTYPE));
            valueOrderId = cursor.getString(cursor.getColumnIndex(SQLiteCBLCAdapter.SQLiteCBLC.COL_ORDERID));
            valueOrderCategoryId = cursor.getString(cursor.getColumnIndex(SQLiteCBLCAdapter.SQLiteCBLC.COL_ORDERCATEGORY));
            valueOrderQuantity = cursor.getInt(cursor.getColumnIndex(SQLiteCBLCAdapter.SQLiteCBLC.COL_QUANTITY));
            valueOrderServing = cursor.getString(cursor.getColumnIndex(SQLiteCBLCAdapter.SQLiteCBLC.COL_ORDERSERVING));
            currentPrice = cursor.getString(cursor.getColumnIndex(SQLiteCBLCAdapter.SQLiteCBLC.COL_PRICECURRENT));
            valueIsPaidWithPoints = cursor.getString(cursor.getColumnIndex(SQLiteCBLCAdapter.SQLiteCBLC.COL_PAID_WITH_POINTS));

            Log.d(StringConfig.LOG_TAG, "categoryID : " + categoryId);
            Log.d(StringConfig.LOG_TAG, "orderID : " + valueOrderId);
            Log.d(StringConfig.LOG_TAG, "valueOrderQuantity : " + valueOrderQuantity);
            Log.d(StringConfig.LOG_TAG, "currentPrice : " + currentPrice);
            Log.d(StringConfig.LOG_TAG, "orderServing : " + valueOrderServing);

            postToServer(paramsOrderType, paramsCustomerId, paramsOrderId, paramsOrderCategoryId, paramsOrderServing, paramsOrderQuantity, paramsIsPaidWithPoints, currentPrice,
                    valueOrderType, valueOrderId, valueOrderCategoryId, valueOrderServing, String.valueOf(valueOrderQuantity), valueIsPaidWithPoints, "drink_id");

        }

        cursor.close();

    }

    private void postToServerFood() {
        String orderTypeIs1Query = "SELECT " + SQLiteCBLCAdapter.SQLiteCBLC.COL_ORDERID + ", " + SQLiteCBLCAdapter.SQLiteCBLC.COL_ORDERTYPE + ", " + SQLiteCBLCAdapter.SQLiteCBLC.COL_ORDERSERVING + ", " + SQLiteCBLCAdapter.SQLiteCBLC.COL_ORDERCATEGORY + ", " + SQLiteCBLCAdapter.SQLiteCBLC.COL_QUANTITY + ", " + SQLiteCBLCAdapter.SQLiteCBLC.COL_PRICECURRENT + ", " + SQLiteCBLCAdapter.SQLiteCBLC.COL_PAID_WITH_POINTS + "" + " FROM " + SQLiteCBLCAdapter.SQLiteCBLC.TABLE_NAME + " WHERE " + SQLiteCBLCAdapter.SQLiteCBLC.COL_ORDERTYPE + " = '1'";
        Cursor cursor = sqLiteDatabase.rawQuery(orderTypeIs1Query, null);

        //String orderingParams = "food_id";

        //orderingParams
        String paramsOrderType = "orderType";
        String paramsCustomerId = "customer_id";
        String paramsOrderId = "food_id";
        String paramsOrderCategoryId = "food_category_id";
        String paramsOrderServing = "food_serving_size_id";
        String paramsOrderQuantity = "quantity";
        String paramsIsPaidWithPoints = "is_paid_with_points?";

        String valueOrderId = ""; // always blank
        String valueOrderType = "Food";
        String valueOrderCategoryId = "";
        String valueOrderServing = "";
        String valueIsPaidWithPoints = "";


        int categoryId = 0;

        int valueOrderQuantity = 0;
        String currentPrice = "";
        while (cursor.moveToNext()) {
            categoryId = cursor.getInt(cursor.getColumnIndex(SQLiteCBLCAdapter.SQLiteCBLC.COL_ORDERTYPE));
            valueOrderId = cursor.getString(cursor.getColumnIndex(SQLiteCBLCAdapter.SQLiteCBLC.COL_ORDERID));
            valueOrderCategoryId = cursor.getString(cursor.getColumnIndex(SQLiteCBLCAdapter.SQLiteCBLC.COL_ORDERCATEGORY));
            valueOrderServing = cursor.getString(cursor.getColumnIndex(SQLiteCBLCAdapter.SQLiteCBLC.COL_ORDERSERVING));
            valueOrderQuantity = cursor.getInt(cursor.getColumnIndex(SQLiteCBLCAdapter.SQLiteCBLC.COL_QUANTITY));
            currentPrice = cursor.getString(cursor.getColumnIndex(SQLiteCBLCAdapter.SQLiteCBLC.COL_PRICECURRENT));
            valueIsPaidWithPoints = cursor.getString(cursor.getColumnIndex(SQLiteCBLCAdapter.SQLiteCBLC.COL_PAID_WITH_POINTS));


            Log.d(StringConfig.LOG_TAG, "categoryID : " + categoryId);
            Log.d(StringConfig.LOG_TAG, "orderID : " + valueOrderId);
            Log.d(StringConfig.LOG_TAG, "valueOrderQuantity : " + valueOrderQuantity);
            Log.d(StringConfig.LOG_TAG, "currentPrice : " + currentPrice);
            Log.d(StringConfig.LOG_TAG, "orderServing : " + valueOrderServing);

            //postToServer(orderingParams, String.valueOf(categoryId), valueOrderId, String.valueOf(valueOrderQuantity), currentPrice);

            postToServer(paramsOrderType, paramsCustomerId, paramsOrderId, paramsOrderCategoryId, paramsOrderServing, paramsOrderQuantity, paramsIsPaidWithPoints, currentPrice,
                    valueOrderType, valueOrderId, valueOrderCategoryId, valueOrderServing, String.valueOf(valueOrderQuantity), valueIsPaidWithPoints, "food_id");

        }
        cursor.close();

    }

    //private void postToServer(String orderingParams, String categoryId, String orderId, String quantity, String currentPrice) {
    private void postToServer(String paramsOrderType, String paramsCustomerId, final String paramsOrderId, String paramsOrderCategoryId, String paramsOrderServing, final String paramsOrderQuantity, String paramsIsPaidWithPoints, String currentPrice,
                              final String valueOrderType, final String valueOrderId, String valueOrderCategoryId, String valueOrderServing, final String valueOrderQuantity, final String valueIsPaidWithPoints, final String inventoryType) {

        Log.d(StringConfig.LOG_TAG, "postToServerLogsssss : orderType : " + valueOrderType);
        Log.d(StringConfig.LOG_TAG, "postToServerLogsssss : valueOrderId : " + valueOrderId);
        Log.d(StringConfig.LOG_TAG, "postToServerLogsssss : valueOrderCategoryId : " + valueOrderCategoryId);
        Log.d(StringConfig.LOG_TAG, "postToServerLogsssss : valueOrderServing : " + valueOrderServing);

        final float pointsGained = Float.parseFloat(currentPrice) / 200;
        final String strCustomerPoint = String.format(Locale.US, "%.2f", pointsGained);

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());

        String strPostToServerURL = StringConfig.BASE_URL + StringConfig.ROUTE_POST_ORDER;

        //return type : strings
        Map<String, String> mapPostToServer = new HashMap<String, String>();
        mapPostToServer.put(paramsOrderType, valueOrderType);
        mapPostToServer.put(paramsCustomerId, String.valueOf(sharedPreferencesManager.getSharedPrefId()));
        mapPostToServer.put(paramsOrderId, valueOrderId);
        mapPostToServer.put(paramsOrderCategoryId, valueOrderCategoryId);
        mapPostToServer.put(paramsOrderServing, valueOrderServing);
        mapPostToServer.put(paramsOrderQuantity, valueOrderQuantity);
        mapPostToServer.put(paramsIsPaidWithPoints, valueIsPaidWithPoints);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strPostToServerURL, new JSONObject(mapPostToServer), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d(StringConfig.LOG_TAG, "orderingResponse : " + response.toString());
                try {
                    JSONObject jsonObject = response.getJSONObject("result");

                    String is_served = String.valueOf(jsonObject.getBoolean("is_served?"));
                    Log.d(StringConfig.LOG_TAG, "ORDERINGRESPONSE BOOL VALUE : " + is_served);

                    //stores a value to sharedPreferences if ordered successfully
                    if (is_served.equals("false")) {
                        if (valueIsPaidWithPoints.equals("false")) {
                            if (sharedPreferencesManager.getSharedPrefMobileNumber() != null && !sharedPreferencesManager.getSharedPrefMobileNumber().startsWith("00000")) {
                                postPointsToServer(strCustomerPoint, valueOrderId);

                            } else {
                                postPointsToServer("0", valueOrderId);

                            }
                        }
                    }

                } catch (JSONException e) {

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                new VolleyNetworkRequest().volleyErrorLogger(cl_mainactivity, error);

            }
        });

        requestQueue.add(jsonObjectRequest);

    }

    public void updateInventory(String inventoryType, String valueOrderType, String paramsOrderId, String valueOrderId, String paramsOrderQuantity, String valueOrderQuantity) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());

        String strUpdateInventoryUrl = StringConfig.BASE_URL + StringConfig.ROUTE_UPDATE_INVENTORY;

        Map<String, String> mapUpdateInventory = new HashMap<String, String>();
        mapUpdateInventory.put(inventoryType, valueOrderType);
        mapUpdateInventory.put(paramsOrderId, valueOrderId);
        mapUpdateInventory.put(paramsOrderQuantity, valueOrderQuantity);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strUpdateInventoryUrl, new JSONObject(mapUpdateInventory), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(StringConfig.LOG_TAG, "response");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(jsonObjectRequest);
    }

    public void postPointsToServer(final String pointsGained, final String valueOrderId) {

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());

        String strPostPointsToServerUrl = StringConfig.BASE_URL + StringConfig.ROUTE_POINTING;

        Map<String, String> mapPostPoints = new HashMap<String, String>();
        mapPostPoints.put("customer_id", String.valueOf(sharedPreferencesManager.getSharedPrefId()));
        mapPostPoints.put("order_points", pointsGained);

        Log.d(StringConfig.LOG_TAG, "customerPoint : " + pointsGained);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strPostPointsToServerUrl, new JSONObject(mapPostPoints), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(StringConfig.LOG_TAG, "pointing response : " + response.toString());
                try {
                    String strJsonResult = response.getString("result");
                    String strJsonMessage = response.getString("message");
                    if (strJsonResult.equals("success")) {
                        //Toast.makeText(getContext(), "You gained " + strJsonMessage + " points.", Toast.LENGTH_SHORT).show();

                        editor = sharedPreferencesOrdering.edit();
                        editor.putString("points" + valueOrderId, pointsGained);
                        editor.apply();

                        int sqliteRowCount = sqliteCBLCAdapter.countNumberOfRows();
                        int sharedPreferencesCount = sharedPreferencesOrdering.getAll().size();

                        if (sqliteRowCount == sharedPreferencesCount) {

                            float sum = 0f;

                            Map<String, ?> keys = sharedPreferencesOrdering.getAll();

                            for (Map.Entry<String, ?> entry : keys.entrySet()) {
                                float sharedPrefValue = Float.parseFloat(entry.getValue().toString());
                                //Log.d(StringConfig.LOG_TAG, "floatfloat : " + index++);
                                Log.d(StringConfig.LOG_TAG, "indexindex : " + sharedPrefValue);

                                sum += sharedPrefValue;
                            }

                            String convertedSumValueTo2DecimalPlaces = String.format(Locale.US, "%.2f", sum);

                            Toast.makeText(getContext(), "You gained " + convertedSumValueTo2DecimalPlaces, Toast.LENGTH_SHORT).show();

                            displayOrderingDialog();
                        }

                    }
                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(jsonObjectRequest);
    }

    private void displayOrderingDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Order success.");
        builder.setMessage("Please proceed to the cashier for payment. Do you still have anything to order ? If no, this app will logged-out.")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        FragmentNavOrdering fragmentNavOrdering = new FragmentNavOrdering();
                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.navdrawer_container, fragmentNavOrdering);
                        fragmentTransaction.commit();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new ProfileSettings(getContext()).logoutUser();
                    }
                });

        clearOrderListAfterSuccessfulOrder();

        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();

    }

    private void clearOrderListAfterSuccessfulOrder() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                addToCartParserList.clear();
                getContext().deleteDatabase(SQLiteCBLCAdapter.SQLiteCBLC.DATABASE_NAME);

                editorOrdering.clear();
                editorOrdering.apply();
                tv_cartAmount.setText(String.valueOf(0));
                rAdapterAddToCart.notifyDataSetChanged();
                setYourOrderPlurality();
            }
        }, 800);

    }

}
