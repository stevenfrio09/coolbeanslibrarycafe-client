package com.steven.frio.systemanalysisanddesign;


import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.steven.frio.systemanalysisanddesign.configs.StringConfig;
import com.steven.frio.systemanalysisanddesign.volley.ProfileSettings;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class DialogFragmentForgotPassword extends DialogFragment {

    @BindView(R2.id.til_forgotPassword)
    TextInputLayout til_forgotPassword;

    @BindView(R2.id.tet_forgotPassword)
    TextInputEditText tet_forgotPassword;

    @BindView(R2.id.btn_forgotPassword)
    Button btn_forgotPassword;

    String userMobileNumber;

    public DialogFragmentForgotPassword() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.dialog_fragment_forgot_password, container, false);
        ButterKnife.setDebug(true);
        ButterKnife.bind(this, view);
        getDialog().setTitle("Forgot Password ?");


        btn_forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userMobileNumber = tet_forgotPassword.getText().toString();

                if (!isValidMobileNumber()) {
                    til_forgotPassword.setError("Invalid mobile number.");
                } else {
                    volleyForgotPassword(userMobileNumber);
                }

            }
        });

        tet_forgotPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (til_forgotPassword.isErrorEnabled()) {
                    til_forgotPassword.setErrorEnabled(false);
                    til_forgotPassword.setHint("Mobile Number");
                }
                return false;
            }
        });

        return view;
    }

    public boolean isValidMobileNumber() {
        boolean valid = true;

        if (userMobileNumber.isEmpty()) {
            valid = false;
        }

        if (userMobileNumber.length() < 11) {
            valid = false;
        }

        return valid;
    }

    public void volleyForgotPassword(final String userMobileNumber) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());

        String strForgotPasswordUrl = StringConfig.BASE_URL + StringConfig.ROUTE_FORGOT_PASSWORD;

        Map<String, String> mapPostPassword = new HashMap<String, String>();
        mapPostPassword.put("phone_number", userMobileNumber);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strForgotPasswordUrl, new JSONObject(mapPostPassword), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.d(StringConfig.LOG_TAG, "forgotPassword : " + response);

                    String result = response.getString("result");
                    if (result.equals("success")) {

                        String message = response.getString("message");
                        String password = response.getString("password");

                        til_forgotPassword.setHint("Your new password for " + userMobileNumber + ".");
                        tet_forgotPassword.setText(password);
                        Toast.makeText(getContext(), "Login and change password immediately using this new password.", Toast.LENGTH_SHORT).show();
                    }

                    if (result.equals("failed")) {
                        til_forgotPassword.setError("Mobile number does not exists.");
                    }
                } catch (JSONException e) {

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                new ProfileSettings(getContext()).displayNetworkError(error);
            }
        });

        requestQueue.add(jsonObjectRequest);
    }


}
