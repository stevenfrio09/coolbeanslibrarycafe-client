package com.steven.frio.systemanalysisanddesign.recycleradapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.steven.frio.systemanalysisanddesign.R;
import com.steven.frio.systemanalysisanddesign.R2;
import com.steven.frio.systemanalysisanddesign.recyclerparsers.RParserCommentsSuggestions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by steven on 10/7/17.
 */

public class RAdapterCommentSuggestions extends RecyclerView.Adapter<RAdapterCommentSuggestions.ViewHolder> {

    Context context;
    List<RParserCommentsSuggestions> rParserCommentsSuggestionsList;

    public RAdapterCommentSuggestions(Context context, List<RParserCommentsSuggestions> rParserCommentsSuggestionsList) {
        this.context = context;
        this.rParserCommentsSuggestionsList = rParserCommentsSuggestionsList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cardview_holder_comments, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        RParserCommentsSuggestions rParserCommentsSuggestions = rParserCommentsSuggestionsList.get(position);
        holder.tv_cardrowComments.setText(rParserCommentsSuggestions.getComments());
        holder.rb_cardrowComments.setRating(Float.parseFloat(rParserCommentsSuggestions.getRating().toString()));
    }

    @Override
    public int getItemCount() {
        return rParserCommentsSuggestionsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R2.id.tv_cardrowComments)
        TextView tv_cardrowComments;


        @BindView(R2.id.rb_cardrowComments)
        RatingBar rb_cardrowComments;

        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.setDebug(true);
            ButterKnife.bind(this, itemView);
        }
    }
}
