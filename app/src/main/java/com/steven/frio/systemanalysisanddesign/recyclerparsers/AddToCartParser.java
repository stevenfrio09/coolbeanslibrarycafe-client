package com.steven.frio.systemanalysisanddesign.recyclerparsers;

/**
 * Created by steven on 7/30/17.
 */

public class AddToCartParser {

    private String customerFromDb;
    private String orderFromDb;
    private Integer quantityFromDb;
    private Integer priceFromDb;
    private String imgResource;
    private String orderIdFromDb;
    private String transactionType;

    public AddToCartParser(String getOrder, Integer priceFromDb, Integer quantityFromDb, String imgResource, String orderIdFromDb, String transactionType) {
        orderFromDb = getOrder;
        this.priceFromDb = priceFromDb;
        this.quantityFromDb = quantityFromDb;
        this.imgResource = imgResource;
        this.orderIdFromDb = orderIdFromDb;
        this.transactionType = transactionType;
    }


    public String getCustomerFromDb() {
        return customerFromDb;
    }

    public void setCustomerFromDb(String customerFromDb) {
        this.customerFromDb = customerFromDb;
    }

    public String getOrderFromDb() {
        return orderFromDb;
    }

    public void setOrderFromDb(String orderFromDb) {
        this.orderFromDb = orderFromDb;
    }

    public Integer getQuantityFromDb() {
        return quantityFromDb;
    }

    public void setQuantityFromDb(Integer quantityFromDb) {
        this.quantityFromDb = quantityFromDb;
    }

    public Integer getPriceFromDb() {
        return priceFromDb;
    }

    public void setPriceFromDB(Integer priceFromDB) {
        this.priceFromDb = priceFromDB;
    }

    public String getImgResource() {
        return imgResource;
    }

    public void setImgResource(String imgResource) {
        this.imgResource = imgResource;
    }

    public String getOrderIdFromDb() {
        return orderIdFromDb;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }
}
