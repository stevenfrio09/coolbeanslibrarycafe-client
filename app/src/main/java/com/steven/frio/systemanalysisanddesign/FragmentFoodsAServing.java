package com.steven.frio.systemanalysisanddesign;


import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.steven.frio.systemanalysisanddesign.configs.StringConfig;
import com.steven.frio.systemanalysisanddesign.recycleradapters.RAdapterFoodsAServing;
import com.steven.frio.systemanalysisanddesign.recyclerparsers.RParserFoodsAServing;
import com.steven.frio.systemanalysisanddesign.volley.VolleyNetworkRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentFoodsAServing extends Fragment {

    @BindView(R2.id.srl_foodsAServing)
    SwipeRefreshLayout srl_foodsAServing;
    @BindView(R2.id.rv_foodsAServing)
    RecyclerView rv_foodsAServing;
    RecyclerView.LayoutManager layoutManager;
    @BindView(R2.id.fragmentFoodsAServing)
    LinearLayout linearLayout;


    CoordinatorLayout cl_mainactivity;

    NetworkResponse networkResponse;

    List<RParserFoodsAServing> rParserFoodsAServingList = new ArrayList<>();
    RAdapterFoodsAServing rAdapterFoodsAServing;

    View view;

    public FragmentFoodsAServing() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_foods_aserving, container, false);

        ButterKnife.setDebug(true);
        ButterKnife.bind(this, view);

        cl_mainactivity = (CoordinatorLayout) getActivity().findViewById(R.id.cl_mainactivity);

        loadJSONFoodsAServing();

        srl_foodsAServing.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                srl_foodsAServing.setRefreshing(true);
                loadJSONFoodsAServing();
                srl_foodsAServing.setRefreshing(false);

            }
        });


        return view;
    }

    private void loadJSONFoodsAServing() {
        rParserFoodsAServingList.clear();

        volleyGetFoodsAServingCategory();
        initRecyclerViews();
        initRecyclerViewFood();

    }

    private void volleyGetFoodsAServingCategory() {

        //final ProgressDialog progressDialog = new ProgressDialogMethods(getContext()).fetchingDataFromServer();
        srl_foodsAServing.setRefreshing(true);
        String strLoadFoodAServingCatURL = StringConfig.BASE_URL + StringConfig.ROUTE_FOODS_SERVING;
        //String strLoadFoodAServingCatURL = "http://192.168.22.6/" + "aservingvalues.json";

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());

        StringRequest stringRequest = new StringRequest(Request.Method.GET, strLoadFoodAServingCatURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(StringConfig.LOG_TAG, "fragmentAServing " + response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        int jsonFoodId = jsonObject.getInt("id");
                        String jsonFoodName = jsonObject.getString("name");

                        JSONObject jsonObjectImage = jsonObject.getJSONObject("image");
                        String jsonImageUrl = StringConfig.BASE_URL + jsonObjectImage.getString("url");

                        Log.d(StringConfig.LOG_TAG, "" + jsonFoodName);

                        RParserFoodsAServing rParserFoodsAServing = new RParserFoodsAServing(jsonFoodName, jsonFoodId, jsonImageUrl);
                        rParserFoodsAServingList.add(rParserFoodsAServing);
                        rAdapterFoodsAServing.notifyDataSetChanged();

                        //progressDialog.dismiss();
                        srl_foodsAServing.setRefreshing(false);

                    }

                } catch (JSONException e) {

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                //progressDialog.dismiss();
                srl_foodsAServing.setRefreshing(false);

                new VolleyNetworkRequest().volleyErrorLogger(cl_mainactivity, error);

            }
        });
        requestQueue.add(stringRequest);


    }

    private void initRecyclerViews() {
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rv_foodsAServing.setLayoutManager(layoutManager);
        Log.d(StringConfig.LOG_TAG, "initRecy views called");
    }

    private void initRecyclerViewFood() {
        rAdapterFoodsAServing = new RAdapterFoodsAServing(getContext(), rParserFoodsAServingList, new RAdapterFoodsAServing.RAdapterFoodsAServingOnItemClick() {
            @Override
            public void onItemClick(RecyclerView.ViewHolder holder, int position, TextView tv_cardrowName_foods_aserving, String s) {

                String strSelectedServingValue = tv_cardrowName_foods_aserving.getText().toString();

                FragmentFoodsMain fragmentFoodsMain = new FragmentFoodsMain();

                Bundle bundle = new Bundle();
                bundle.putString("setSelectedServingValue", strSelectedServingValue);
                bundle.putString("setSelectedServingValueIntValue", s);
                fragmentFoodsMain.setArguments(bundle);

                getFragmentManager().beginTransaction().replace(R.id.fragmentContainerFoods, fragmentFoodsMain).addToBackStack(null).commit();

            }


        });
        rv_foodsAServing.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        rv_foodsAServing.setAdapter(rAdapterFoodsAServing);
    }
/*
    private void displayNetworkError(VolleyError error) {

        networkResponse = error.networkResponse;

        if(networkResponse!= null && networkResponse.statusCode == 500){
            Snackbar snackbar = Snackbar.make(linearLayout, "Error 500. Internal server error.", Snackbar.LENGTH_SHORT);
            snackbar.show();

        }

        if (networkResponse != null && networkResponse.statusCode == 400) {
            Snackbar snackbar = Snackbar.make(linearLayout, "Check your credentials.", Snackbar.LENGTH_SHORT);
            snackbar.show();
        }

        if (networkResponse != null && networkResponse.statusCode == 404) {
            Snackbar snackbar = Snackbar.make(linearLayout, "Error 404. Server not found", Snackbar.LENGTH_SHORT);
            snackbar.show();
        }

        if (error instanceof NoConnectionError) {
            Snackbar snackbar = Snackbar.make(linearLayout, "No Connection.", Snackbar.LENGTH_SHORT);
            snackbar.show();
        }

        if (error instanceof TimeoutError) {
            Snackbar snackbar = Snackbar.make(linearLayout, "Connection Timed Out.", Snackbar.LENGTH_SHORT);
            snackbar.show();
        }

    }*/

}
