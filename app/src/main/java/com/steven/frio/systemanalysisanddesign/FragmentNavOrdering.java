package com.steven.frio.systemanalysisanddesign;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentNavOrdering extends Fragment {


    @BindView(R2.id.tabLayout_ordering)
    TabLayout tabLayout_ordering;
    @BindView(R2.id.viewPager_ordering)
    ViewPager viewPager_ordering;
    Button btn_updateProfile;
    PagerAdapter pagerAdapter;

    View view;

    public FragmentNavOrdering() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment2
        view = inflater.inflate(R.layout.fragment_nav_ordering, container, false);
        ButterKnife.setDebug(true);
        ButterKnife.bind(this, view);

        //hideInputMethod();

        pagerAdapter = new TabPagerAdapterFoodsDrinks(getFragmentManager(), tabLayout_ordering.getTabCount());

        tabLayout_ordering.addTab(tabLayout_ordering.newTab().setText("Foods"));
        tabLayout_ordering.addTab(tabLayout_ordering.newTab().setText("Drinks"));

        viewPager_ordering.setAdapter(pagerAdapter);
        viewPager_ordering.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout_ordering));
        tabLayout_ordering.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                viewPager_ordering.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        return view;
    }

    /*public void hideInputMethod(){
        InputMethodManager inputMethodManager = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if(view==null)
            return;
        inputMethodManager.hideSoftInputFromInputMethod(view.getWindowToken(), 0);
    }*/

}
