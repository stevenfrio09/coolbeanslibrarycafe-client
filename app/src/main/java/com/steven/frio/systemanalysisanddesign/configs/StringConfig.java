package com.steven.frio.systemanalysisanddesign.configs;

import com.steven.frio.systemanalysisanddesign.sharedpreferences.SharedPreferencesManager;

/**
 * Created by steven on 7/26/17.
 */

public class StringConfig {

    public static final String BASE_URL = StringConfig.BASE_URL();


    public static final String BASE_URL(){
        return SharedPreferencesManager.getSharedPrefIP();
    }

    public static final String PICASSO_URL = BASE_URL;

    //public static final String BASE_URL = "http://192.168.22.10/";

    public static final String LOG_TAG = "SysAD";

    //GET PROFILE INFO

    public static final String ROUTE_CUSTOMER_INFO = "api/users/show_all_customer";

    //BACKEND PROFILE ROUTES
    public static final String ROUTE_REGISTER = "api/users/register";

    public static final String ROUTE_LOGIN = "api/users/login";

    public static final String ROUTE_LOGIN_GUEST = "api/users/guest";

    public static final String ROUTE_LOGOUT = "api/users/logout";

    public static final String ROUTE_CHANGENAME = "api/users/update_profile";

    public static final String ROUTE_CHANGEPASSWORD = "api/users/change_password";

    public static final String ROUTE_FORGOT_PASSWORD = "api/users/forgot_password";


    //BACKEND ORDERING ROUTES
    public static final String ROUTE_FOODS = "api/food_category/show";

    public static final String ROUTE_DRINKS = "api/drink_category/show";

    public static final String ROUTE_SHOW_PENDING_ORDERS = "api/order/show";

    //--------------------FOOD ROUTES------------//

    public static final String ROUTE_FOODS_SERVING = "api/food_serving_size/show";

    public static final String ROUTE_FOODS_MAIN_CATEGORY = "api/food_category/show";

    public static final String ROUTE_FOODS_SUBCATEGORY = "api/food/filter";

    public static final String ROUTE_FOODS_SHOWALL = "api/food/show";

    //public static final String ROUTE_FOODS_SHOWALL = "Food.json";


    //--------------------DRINK ROUTES------------//

    public static final String ROUTE_DRINKS_SERVING = "api/drink_serving_temp/show";

    public static final String ROUTE_DRINKS_MAIN_CATEGORY = "api/drink_category/show";

    public static final String ROUTE_DRINKS_SUBCATEGORY = "api/drink/filter";

    public static final String ROUTE_DRINKS_SHOWALL = "api/drink/show_all";

    //public static final String ROUTE_DRINKS_SHOWALL = "Drink.json";

    //-----------------ORDER --------------------------//
    //public static final String ROUTE_POST_ORDER = "api/order/get_order";

    public static final String ROUTE_POST_ORDER = "api/order/fetch_order";


    //------------Comments and Suggestions-------------//

    public static final String ROUTE_COMMENTS_SUGGESTIONS = "api/customer_feedback/comments_suggestions";


    public static final String ROUTE_COMMENTS_SUGGESTIONS_SHOW = "api/customer_feedback/show";

    //------------Pointing-------------//

    public static final String ROUTE_POINTING = "api/order/set_point";

    //------Redeem With Points-------//

    public static final String ROUTE_REDEEM_WITH_POINTS = "api/drink/payable_with_points";


    //------Inventory-------//

    public static final String ROUTE_UPDATE_INVENTORY = "api/inventory/update_inventory";

    //SHARED PREFERENCES NAME
    public static final String SHAREDPREF_NAME = "CBLCSharedPref";
    public static final String SHAREDPREF_ORDERING = "CBLCOrderingSharedPref";
    public static final String SHAREDPREF_CUSTOMERID = "CBLCCustomerId";
    public static final String SHAREDPREF_ADMIN = "CBLCAdmin";

    //GUEST ACCOUNT
    //public static final String GUEST_MOBILE = "00000000000";
    public static final String GUEST_PASSWORD = "12345678";


    //NETWORK ERROR DIALOGS

    public static final String ERROR_500 = "Error 500. Internal server error.";

    public static final String ERROR_400 = "Error 400. Bad Request.";

    public static final String ERROR_404 = "Error 404. Server not found.";

    public static final String ERROR_NO_CONNECTION = "No Connection.";

    public static final String ERROR_CONNECTION_TIMED_OUT = "Connection Timed Out.";


}
