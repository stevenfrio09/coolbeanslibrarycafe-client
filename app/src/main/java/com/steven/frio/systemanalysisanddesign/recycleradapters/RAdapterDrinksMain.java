package com.steven.frio.systemanalysisanddesign.recycleradapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.steven.frio.systemanalysisanddesign.R;
import com.steven.frio.systemanalysisanddesign.R2;
import com.steven.frio.systemanalysisanddesign.recyclerparsers.RParserDrinksMain;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by steven on 8/18/17.
 */

public class RAdapterDrinksMain extends RecyclerView.Adapter<RAdapterDrinksMain.ViewHolder> {

    Context context;
    List<RParserDrinksMain> rParserDrinksMainList;
    RAdapterDrinksMainOnItemClick rAdapterDrinksMainOnItemClick;
    int lastPosition = -1;

    public RAdapterDrinksMain(Context context, List<RParserDrinksMain> rParserDrinksMainList, RAdapterDrinksMainOnItemClick rAdapterDrinksMainOnItemClick) {
        this.context = context;
        this.rParserDrinksMainList = rParserDrinksMainList;
        this.rAdapterDrinksMainOnItemClick = rAdapterDrinksMainOnItemClick;
    }

    @Override
    public RAdapterDrinksMain.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cardview_holder_drinks_main, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RAdapterDrinksMain.ViewHolder holder, final int position) {
        final RParserDrinksMain rParserDrinksMain = rParserDrinksMainList.get(position);
        final String drinkCategoryId = String.valueOf(rParserDrinksMain.getDrinkCategoryId());
        holder.tv_cardrowName_drinks_main.setText(rParserDrinksMain.getDrinkCategoryName());

        Picasso.with(context).load(rParserDrinksMain.getDrinkCategoryImageURL()).resize(1080, 600).centerCrop().into(holder.iv_cardrow_holder_drinks_main);

        holder.iv_cardrow_holder_drinks_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rAdapterDrinksMainOnItemClick.onItemClick(holder, position, holder.tv_cardrowName_drinks_main, drinkCategoryId);
            }
        });

        setAnimation(holder.itemView, position);

    }

    @Override
    public int getItemCount() {
        return rParserDrinksMainList.size();
    }

    private void setAnimation(View itemView, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
            itemView.startAnimation(animation);
            lastPosition = position;

        }
    }

    public interface RAdapterDrinksMainOnItemClick {
        void onItemClick(RecyclerView.ViewHolder holder, int position, TextView tv_cardrowName_drinks_main, String s);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R2.id.iv_cardrow_holder_drinks_main)
        ImageView iv_cardrow_holder_drinks_main;

        @BindView(R2.id.tv_cardrowName_drinks_main)
        TextView tv_cardrowName_drinks_main;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            ButterKnife.setDebug(true);

        }
    }
}
