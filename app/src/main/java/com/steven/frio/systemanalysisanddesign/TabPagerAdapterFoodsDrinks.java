package com.steven.frio.systemanalysisanddesign;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by steven on 7/8/17.
 */

public class TabPagerAdapterFoodsDrinks extends FragmentStatePagerAdapter {
    public TabPagerAdapterFoodsDrinks(FragmentManager fm, int tabCount) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new FragmentFoods();
            case 1:
                return new FragmentDrinks();
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return 2;
    }
}
