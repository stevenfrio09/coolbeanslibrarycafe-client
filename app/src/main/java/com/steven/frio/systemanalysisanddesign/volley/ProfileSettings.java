package com.steven.frio.systemanalysisanddesign.volley;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.steven.frio.systemanalysisanddesign.LoginActivity;
import com.steven.frio.systemanalysisanddesign.NavigationDrawer;
import com.steven.frio.systemanalysisanddesign.configs.ProgressDialogMethods;
import com.steven.frio.systemanalysisanddesign.configs.StringConfig;
import com.steven.frio.systemanalysisanddesign.sharedpreferences.SharedPreferencesManager;
import com.steven.frio.systemanalysisanddesign.sqlite.SQLiteCBLCAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by steven on 8/2/17.
 */

public class ProfileSettings {


    Context context;
    SharedPreferences sharedPreferences, sharedPreferencesCustomerId;
    SharedPreferences.Editor editor, editorCustomerId;
    String sharedPrefValueToken, sharedPrefValueMobile;
    VolleyGetterSetter volleyGetterSetter;
    NetworkResponse networkResponse;
    SharedPreferencesManager sharedPreferencesManager;

    public ProfileSettings(Context context) {
        this.context = context;

        //initialize/get  sharedpreferences values
        sharedPreferences = context.getSharedPreferences(StringConfig.SHAREDPREF_NAME, 0);
        sharedPreferencesCustomerId = context.getSharedPreferences(StringConfig.SHAREDPREF_CUSTOMERID, 0);
        editor = sharedPreferences.edit();
        editorCustomerId = sharedPreferencesCustomerId.edit();

        sharedPrefValueToken = sharedPreferences.getString("token", null);
        sharedPrefValueMobile = sharedPreferences.getString("phone_number", null);

        // initialize gettersetter values
        volleyGetterSetter = new VolleyGetterSetter(context);

        volleyGetterSetter.setToken(sharedPrefValueToken);
        volleyGetterSetter.setMobile_number(sharedPrefValueMobile);

        sharedPreferencesManager = new SharedPreferencesManager(context);


    }


    public void loginUser(final String strLoginMobile, final String strLoginPassword) {

        String urlLogin = StringConfig.BASE_URL + StringConfig.ROUTE_LOGIN;


        RequestQueue requestQueue = Volley.newRequestQueue(context);

        final ProgressDialog progressDialog = new ProgressDialogMethods(context).pleaseWait();

        editor.putString("phone_number", strLoginMobile);
        editor.apply();

        Map<String, String> mapLogin = new HashMap<String, String>();
        mapLogin.put("phone_number", strLoginMobile);
        mapLogin.put("password", strLoginPassword);

        Log.d(StringConfig.LOG_TAG, "sharedPref value : " + volleyGetterSetter.getToken());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, urlLogin, new JSONObject(mapLogin), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.d(StringConfig.LOG_TAG, "LoginUserCalled" + response.toString());

                    String jsonObjectLoginResult = response.getString("result");

                    if (!jsonObjectLoginResult.equals("failed")) {

                        String jsonObjectMessage = response.getString("message");
                        String jsonObjectLoginToken = response.getString("token");
                        String jsonObjectLoginMobile = response.getString("phone_number");
                        String jsonObjectFstName = response.getString("first_name");
                        String jsonObjectLstName = response.getString("last_name");
                        int jsonObjectCustomerId = response.getInt("id");

                        Log.d(StringConfig.LOG_TAG, "calledLoginUser:Result : " + jsonObjectLoginResult);

                        //Storing token to sharedPreferences
                        editor.putString("token", jsonObjectLoginToken);
                        editor.putString("phone_number", jsonObjectLoginMobile);
                        editor.putString("first_name", jsonObjectFstName);
                        editor.putString("last_name", jsonObjectLstName);
                        editor.putInt("cust_id", jsonObjectCustomerId);
                        editor.apply();

                        progressDialog.dismiss();

                        Intent intent = new Intent(context, NavigationDrawer.class);
                        context.startActivity(intent);
                        ((Activity) context).finish();

                        //Toast.makeText(context, response.toString(), Toast.LENGTH_SHORT).show();
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(context, "Invalid mobile number or password.", Toast.LENGTH_SHORT).show();

                    }

                } catch (JSONException e) {

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();

                displayNetworkError(error);

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> mapLogin = new HashMap<String, String>();
                mapLogin.put("phone_number", strLoginMobile);
                mapLogin.put("password", strLoginPassword);

                return mapLogin;

            }
        };

        requestQueue.add(jsonObjectRequest);

    }


    public void saveAllCustomerIdWithPendingOrders() {
        String strPendingOrdersUrl = StringConfig.BASE_URL + StringConfig.ROUTE_SHOW_PENDING_ORDERS;

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        StringRequest stringRequest = new StringRequest(strPendingOrdersUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.d(StringConfig.LOG_TAG, "profilesettings : " + jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String customerId = String.valueOf(jsonObject.getInt("customer_id"));
                        Log.d(StringConfig.LOG_TAG, "customerIDcustomerID : " + customerId);
                        editorCustomerId.putString(customerId, customerId);
                        editorCustomerId.apply();
                    }

                } catch (JSONException e) {

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(stringRequest);
    }


    public void selectValidGuestLogin(final String strLoginPassword, final String strGuestName) {
        String strCustomerInfoUrl = StringConfig.BASE_URL + StringConfig.ROUTE_CUSTOMER_INFO;

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        StringRequest stringRequest = new StringRequest(strCustomerInfoUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.d(StringConfig.LOG_TAG, "guestMode : " + jsonArray);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        String userPhoneNumber = jsonObject.getString("phone_number");

                        if (userPhoneNumber.startsWith("00000")) {
                            String userToken = jsonObject.getString("token");
                            String userLastName = jsonObject.getString("last_name");
                            String userPhoneNumberMain = jsonObject.getString("phone_number");
                            String userId = String.valueOf(jsonObject.getInt("id"));

                            Log.d(StringConfig.LOG_TAG, "userToken : " + userToken);
                            Log.d(StringConfig.LOG_TAG, "userLastName : " + userLastName);
                            Log.d(StringConfig.LOG_TAG, "userPhoneNumber : " + userPhoneNumberMain);

                            String sharedPrefCustomerId = sharedPreferencesCustomerId.getString(userId, null);

                            if (!userId.equals(sharedPrefCustomerId) && (userToken.equals("null") || userToken.isEmpty()) && userLastName.equals("GuestCBLC") && userPhoneNumberMain.startsWith("00000")) {
                                loginGuest(userPhoneNumberMain, strLoginPassword, strGuestName);
                                break;
                            }

                        }
                    }

                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(stringRequest);

    }

    public void loginGuest(final String strLoginMobile, final String strLoginPassword, final String strGuestName) {

        String urlLogin = StringConfig.BASE_URL + StringConfig.ROUTE_LOGIN_GUEST;

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        final ProgressDialog progressDialog = new ProgressDialogMethods(context).pleaseWait();

        editor.putString("phone_number", strLoginMobile);
        editor.apply();

        Map<String, String> mapLogin = new HashMap<String, String>();
        mapLogin.put("phone_number", strLoginMobile);
        mapLogin.put("first_name", strGuestName);

        Log.d(StringConfig.LOG_TAG, "sharedPref value : " + volleyGetterSetter.getToken());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, urlLogin, new JSONObject(mapLogin), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.d(StringConfig.LOG_TAG, response.toString());

                    String jsonObjectLoginResult = response.getString("result");

                    if (!jsonObjectLoginResult.equals("failed")) {

                        String jsonObjectMessage = response.getString("message");
                        String jsonObjectLoginToken = response.getString("token");
                        //String jsonObjectLoginMobile = response.getString("phone_number");
                        //String jsonObjectFstName = response.getString("first_name");
                        //String jsonObjectLstName = response.getString("last_name");
                        int jsonObjectCustomerId = response.getInt("id");
                        Log.d(StringConfig.LOG_TAG, String.valueOf(jsonObjectCustomerId));

                        Log.d(StringConfig.LOG_TAG, "calledLoginUser:Result : " + jsonObjectLoginResult);

                        //Storing token to sharedPreferences
                        editor.putString("token", jsonObjectLoginToken);
                        editor.putString("phone_number", strLoginMobile);
                        //editor.putString("first_name", jsonObjectFstName);
                        //editor.putString("last_name", jsonObjectLstName);
                        editor.putInt("cust_id", jsonObjectCustomerId);
                        editor.apply();

                        progressDialog.dismiss();

                        Intent intent = new Intent(context, NavigationDrawer.class);
                        context.startActivity(intent);
                        ((Activity) context).finish();

                        //Toast.makeText(context, response.toString(), Toast.LENGTH_SHORT).show();
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(context, "An error has occurred", Toast.LENGTH_SHORT).show();

                    }

                } catch (JSONException e) {

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();

                displayNetworkError(error);

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> mapLogin = new HashMap<String, String>();
                mapLogin.put("phone_number", strLoginMobile);
                mapLogin.put("first_name", strGuestName);

                return mapLogin;

            }
        };

        requestQueue.add(jsonObjectRequest);

    }

    public void logoutUser() {
        /*sharedPreferences = context.getSharedPreferences(StringConfig.SHAREDPREF_NAME, 0);
        editor = sharedPreferences.edit();
        sharedPrefValueToken = sharedPreferences.getString("token", null);*/

        final ProgressDialog progressDialog = new ProgressDialogMethods(context).pleaseWait();

        String urlLogout = StringConfig.BASE_URL + StringConfig.ROUTE_LOGOUT;
        RequestQueue requestQueue = Volley.newRequestQueue(context);

        Map<String, String> mapLogout = new HashMap<String, String>();
        mapLogout.put("token", volleyGetterSetter.getToken());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, urlLogout, new JSONObject(mapLogout), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String jsonResponseResult = response.getString("result");

                    if (!jsonResponseResult.equals("success")) {
                        progressDialog.dismiss();
                        Toast.makeText(context, "Error logging out.", Toast.LENGTH_SHORT).show();

                    } else {

                        SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(context);
                        sharedPreferencesManager.logoutUser();

                        context.deleteDatabase(SQLiteCBLCAdapter.SQLiteCBLC.DATABASE_NAME);

                        progressDialog.dismiss();
                        Toast.makeText(context, "Logged out.", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(context, LoginActivity.class);
                        context.startActivity(intent);
                        ((Activity) context).finish();


                    }

                } catch (JSONException e) {

                    progressDialog.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(StringConfig.LOG_TAG, "calledLogoutUser:Error : " + error);
                progressDialog.dismiss();

                displayNetworkError(error);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapHeaderLogout = new HashMap<String, String>();
                mapHeaderLogout.put("token", volleyGetterSetter.getToken());
                return mapHeaderLogout;
            }
        };
        requestQueue.add(jsonObjectRequest);
    }

    public void updateProfile(final String strUpdateFirstName, final String strUpdateLastName) {
        String urlUpdateProfile = StringConfig.BASE_URL + StringConfig.ROUTE_CHANGENAME;
        //sharedPrefValueToken = sharedPreferences.getString("token", null);

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        Map<String, String> mapUpdateProfile = new HashMap<String, String>();
        mapUpdateProfile.put("phone_number", volleyGetterSetter.getMobile_number());
        mapUpdateProfile.put("first_name", strUpdateFirstName);
        mapUpdateProfile.put("last_name", strUpdateLastName);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, urlUpdateProfile, new JSONObject(mapUpdateProfile), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String jsonResponseResult = response.getString("result");
                    if (jsonResponseResult.equals("success")) {
                        Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
                        editor.remove("first_name");
                        editor.remove("last_name");
                        editor.putString("first_name", strUpdateFirstName);
                        editor.putString("last_name", strUpdateLastName);
                        editor.commit();

                    }

                } catch (JSONException e) {

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                displayNetworkError(error);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapHeader = new HashMap<String, String>();
                mapHeader.put("token", volleyGetterSetter.getToken());

                return mapHeader;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> mapUpdateProfile = new HashMap<String, String>();
                mapUpdateProfile.put("phone_number", volleyGetterSetter.getMobile_number());
                mapUpdateProfile.put("first_name", strUpdateFirstName);
                mapUpdateProfile.put("last_name", strUpdateLastName);

                return mapUpdateProfile;

            }
        };


        requestQueue.add(jsonObjectRequest);

    }

    public void updatePassword(final String strUpdatePasswordPrevious, final String strUpdatePasswordNew, final String strUpdatePasswordNewConfirm) {
        String urlUpdatePassword = StringConfig.BASE_URL + StringConfig.ROUTE_CHANGEPASSWORD;
        //sharedPrefValueToken = sharedPreferences.getString("token", null);
        RequestQueue requestQueue = Volley.newRequestQueue(context);

        Map<String, String> mapUpdatePassword = new HashMap<String, String>();
        mapUpdatePassword.put("phone_number", volleyGetterSetter.getMobile_number());
        mapUpdatePassword.put("password", strUpdatePasswordPrevious);
        mapUpdatePassword.put("newpassword", strUpdatePasswordNew);
        mapUpdatePassword.put("newpassword_confirmation", strUpdatePasswordNewConfirm);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, urlUpdatePassword, new JSONObject(mapUpdatePassword), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.d(StringConfig.LOG_TAG, "updatePassword response : " + response);
                    String jsonResponseResult = response.getString("result");
                    String jsonResponseMessage = response.getString("message");
                    if (jsonResponseResult.equals("success")) {
                        Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(context, "" + jsonResponseMessage, Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                displayNetworkError(error);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapHeader = new HashMap<String, String>();
                mapHeader.put("token", volleyGetterSetter.getToken());

                return mapHeader;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> mapUpdatePassword = new HashMap<String, String>();
                mapUpdatePassword.put("phone_number", volleyGetterSetter.getMobile_number());
                mapUpdatePassword.put("password", strUpdatePasswordPrevious);
                mapUpdatePassword.put("newpassword", strUpdatePasswordNew);
                mapUpdatePassword.put("newpassword_confirmation", strUpdatePasswordNewConfirm);

                return mapUpdatePassword;

            }
        };


        requestQueue.add(jsonObjectRequest);

    }

    public void displayProfilePoints() {
        RequestQueue requestQueue = Volley.newRequestQueue(context);

        String userProfileInfoUrl = StringConfig.BASE_URL + StringConfig.ROUTE_CUSTOMER_INFO;

        StringRequest stringRequest = new StringRequest(userProfileInfoUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d(StringConfig.LOG_TAG, "userProfiles : " + response.toString());
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i <= jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        int jsonObjectId = jsonObject.getInt("id");
                        String jsonObjectFirstName = jsonObject.getString("first_name");
                        String jsonObjectLastName = jsonObject.getString("last_name");
                        String jsonObjectMobileNum = jsonObject.getString("phone_number");
                        String jsonObjectToken = jsonObject.getString("token");
                        Log.d(StringConfig.LOG_TAG, "jsonOBjectToken : " + jsonObjectToken);

                        boolean checkValue = jsonObjectId == sharedPreferencesManager.getSharedPrefId() &&
                                jsonObjectFirstName.equals(sharedPreferencesManager.getSharedPrefFirstName()) &&
                                jsonObjectLastName.equals(sharedPreferencesManager.getSharedPrefLastName()) &&
                                jsonObjectMobileNum.equals(sharedPreferencesManager.getSharedPrefMobileNumber());

                        if (checkValue) {
                            String jsonObjectPoints = jsonObject.getString("points");

                            Log.d(StringConfig.LOG_TAG, "jsonObjectFirstName : " + jsonObjectFirstName);
                            Log.d(StringConfig.LOG_TAG, "jsonObjectLastName : " + jsonObjectLastName);
                            Log.d(StringConfig.LOG_TAG, "jsonObjectMobileNum : " + jsonObjectMobileNum);
                            Log.d(StringConfig.LOG_TAG, "jsonObjectPoints : " + jsonObjectPoints);

                            editor.putFloat("points", Float.parseFloat(jsonObjectPoints));
                            editor.apply();

                        }
                    }

                } catch (JSONException e) {

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                displayNetworkError(error);
            }
        });

        requestQueue.add(stringRequest);

    }


    public void displayNetworkError(VolleyError error) {

        networkResponse = error.networkResponse;

        if (networkResponse != null && networkResponse.statusCode == 400) {
            Toast.makeText(context, "Check your credentials.", Toast.LENGTH_SHORT).show();
        }

        if (networkResponse != null && networkResponse.statusCode == 404) {
            Toast.makeText(context, StringConfig.ERROR_404, Toast.LENGTH_SHORT).show();
        }

        if (error instanceof NoConnectionError) {
            Toast.makeText(context, StringConfig.ERROR_NO_CONNECTION, Toast.LENGTH_SHORT).show();
        }

        if (error instanceof TimeoutError) {
            Toast.makeText(context, StringConfig.ERROR_CONNECTION_TIMED_OUT, Toast.LENGTH_SHORT).show();
        }

    }

}