package com.steven.frio.systemanalysisanddesign;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.steven.frio.systemanalysisanddesign.configs.ProgressDialogMethods;
import com.steven.frio.systemanalysisanddesign.configs.StringConfig;
import com.steven.frio.systemanalysisanddesign.recycleradapters.RAdapterCommentSuggestions;
import com.steven.frio.systemanalysisanddesign.recyclerparsers.RParserCommentsSuggestions;
import com.steven.frio.systemanalysisanddesign.sharedpreferences.SharedPreferencesManager;
import com.steven.frio.systemanalysisanddesign.volley.VolleyNetworkRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentNavCommentsSuggestion extends Fragment {

    @BindView(R2.id.btn_comments_suggestions)
    Button btn_comments_suggestions;
    @BindView(R2.id.et_comments_suggestions)
    EditText et_comments_suggestions;
    @BindView(R2.id.rb_comment_suggestions)
    RatingBar rb_comment_suggestions;
    @BindView(R2.id.rv_comments_suggestions)
    RecyclerView rv_comments_suggestions;

    CoordinatorLayout cl_mainactivity;

    List<RParserCommentsSuggestions> rParserCommentsSuggestionsList;
    RAdapterCommentSuggestions rAdapterCommentSuggestions;
    RecyclerView.LayoutManager layoutManager;


    SharedPreferencesManager sharedPreferencesManager;


    String ratingValue;

    public FragmentNavCommentsSuggestion() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_nav_comments_suggestion, container, false);

        ButterKnife.bind(this, view);
        ButterKnife.setDebug(true);

        cl_mainactivity = (CoordinatorLayout) getActivity().findViewById(R.id.cl_mainactivity);

        rParserCommentsSuggestionsList = new ArrayList<>();


        sharedPreferencesManager = new SharedPreferencesManager(getContext());

        ratingBarListener();
        initRecyclerViews();

        btn_comments_suggestions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String comments = et_comments_suggestions.getText().toString();

                if (ratingValue == null || ratingValue.equals("0")/* || comments.length() < 4 || comments.equals("    ") || comments.trim().length() < 4*/) {

                    /*if (comments.equals("    ")) {
                        Toast.makeText(getContext(), "LOL. WHAT A NICE COMMENT. TRY AGAIN", Toast.LENGTH_SHORT).show();
                    }*/

                    if (ratingValue == null || ratingValue.equals("0")) {
                        Toast.makeText(getContext(), "Rate us first.", Toast.LENGTH_SHORT).show();
                    }

                    /*if (comments.isEmpty()) {
                        et_comments_suggestions.setError("This field is required.");
                    }*/

                    /*if (comments.length() < 4 || comments.trim().length() < 4) {
                        et_comments_suggestions.setError("Comment/Suggestion must be 4 characters and above.");
                    }*/

                    //Toast.makeText(getContext(), "Rate and give us a comment first.", Toast.LENGTH_SHORT).show();

                } else {

                    Log.d(StringConfig.LOG_TAG, "rating bar value : " + ratingValue + comments);
                    volleyPostCommentsSuggestions(comments, ratingValue);
                }

            }
        });


        return view;
    }

    private void ratingBarListener() {
        rb_comment_suggestions.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                ratingValue = String.valueOf(rating);
            }
        });
    }

    private void volleyPostCommentsSuggestions(String comments, String ratingValue) {

        final ProgressDialog progressDialog = new ProgressDialogMethods(getContext()).pleaseWait();

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());

        String strLoadFoodSubCatURL = StringConfig.BASE_URL + StringConfig.ROUTE_COMMENTS_SUGGESTIONS;

        Map<String, String> mapPostCommentsSuggestions = new HashMap<String, String>();
        mapPostCommentsSuggestions.put("comments_and_suggestions", comments);
        mapPostCommentsSuggestions.put("rating", ratingValue);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(strLoadFoodSubCatURL, new JSONObject(mapPostCommentsSuggestions), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String result = response.getString("result");
                    Log.d(StringConfig.LOG_TAG, "result : " + result);

                    progressDialog.dismiss();

                    Toast.makeText(getContext(), result.toUpperCase(), Toast.LENGTH_SHORT).show();

                    et_comments_suggestions.setText("");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                new VolleyNetworkRequest().volleyErrorLogger(cl_mainactivity, error);


            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> mapHeader = new HashMap<String, String>();

                mapHeader.put("token", sharedPreferencesManager.getSharedPrefToken());
                //Log.d(StringConfig.LOG_TAG, "TOKEN VALUE : " + volleyGetterSetter.getToken());
                return mapHeader;
            }
        };

        requestQueue.add(jsonObjectRequest);
    }


    private void initRecyclerViews() {

        rAdapterCommentSuggestions = new RAdapterCommentSuggestions(getContext(), rParserCommentsSuggestionsList);
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rv_comments_suggestions.setLayoutManager(layoutManager);
        rv_comments_suggestions.setAdapter(rAdapterCommentSuggestions);
        //rv_comments_suggestions.addItemDecoration(new DividerItemDecoration(getContext()));
        volleyGetCommentsSuggestions();
    }


    private void volleyGetCommentsSuggestions() {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        String strGetCommentsUrl = StringConfig.BASE_URL + StringConfig.ROUTE_COMMENTS_SUGGESTIONS_SHOW;

        StringRequest stringRequest = new StringRequest(strGetCommentsUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i <= jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        int id = jsonObject.getInt("id");
                        String comments = jsonObject.getString("comments_and_suggestions");
                        int rating = jsonObject.getInt("rating");


                        RParserCommentsSuggestions rParserCommentsSuggestions = new RParserCommentsSuggestions(id, comments, rating);
                        rParserCommentsSuggestionsList.add(rParserCommentsSuggestions);
                        rAdapterCommentSuggestions.notifyDataSetChanged();


                    }
                } catch (JSONException e) {


                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(stringRequest);

    }

}
