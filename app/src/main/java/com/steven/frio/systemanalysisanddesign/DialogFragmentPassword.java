package com.steven.frio.systemanalysisanddesign;


import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.steven.frio.systemanalysisanddesign.volley.ProfileSettings;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class DialogFragmentPassword extends DialogFragment {

    @BindView(R2.id.til_updatePasswordPrevious)
    TextInputLayout til_updatePasswordPrevious;
    @BindView(R2.id.til_updatePasswordNew)
    TextInputLayout til_updatePasswordNew;
    @BindView(R2.id.til_updatePasswordNewConfirm)
    TextInputLayout til_updatePasswordNewConfirm;

    @BindView(R2.id.tet_updatePasswordPrevious)
    TextInputEditText tet_updatePasswordPrevious;
    @BindView(R2.id.tet_updatePasswordNew)
    TextInputEditText tet_updatePasswordNew;
    @BindView(R2.id.tet_updatePasswordNewConfirm)
    TextInputEditText tet_updatePasswordNewConfirm;

    @BindView(R2.id.btn_updateDismissPassword)
    Button btn_updateDismissPassword;
    @BindView(R2.id.btn_updateSubmitPassword)
    Button btn_updateSubmitPassword;

    String strUpdatePasswordPrevious, strUpdatePasswordNew, strUpdatePasswordNewConfirm;

    public DialogFragmentPassword() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.dialog_fragment_password, container, false);

        ButterKnife.setDebug(true);
        ButterKnife.bind(this, view);


        getDialog().setTitle("Change Password");


        btn_updateDismissPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        btn_updateSubmitPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strUpdatePasswordPrevious = tet_updatePasswordPrevious.getText().toString();
                strUpdatePasswordNew = tet_updatePasswordNew.getText().toString();
                strUpdatePasswordNewConfirm = tet_updatePasswordNewConfirm.getText().toString();

                if (!isUpdatePasswordValid()) {
                    Toast.makeText(getContext(), "Please enter a  valid password.", Toast.LENGTH_SHORT).show();
                } else {

                    ProfileSettings profileSettings = new ProfileSettings(getContext());
                    profileSettings.updatePassword(strUpdatePasswordPrevious, strUpdatePasswordNew, strUpdatePasswordNewConfirm);
                }
            }
        });

        return view;
    }

    private boolean isUpdatePasswordValid() {
        boolean valid = true;

        if (strUpdatePasswordPrevious.length() < 8) {
            tet_updatePasswordPrevious.setError("Check your password");
            valid = false;
        }

        if (strUpdatePasswordNew.length() < 8) {
            tet_updatePasswordNew.setError("Password length must be 8 and above.");
            valid = false;
        }

        if (strUpdatePasswordNewConfirm.length() < 8 && !strUpdatePasswordNewConfirm.equals(strUpdatePasswordNew)) {
            tet_updatePasswordNewConfirm.setError("Password does not match.");
            valid = false;
        }


        return valid;
    }

}
