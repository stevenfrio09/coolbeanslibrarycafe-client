package com.steven.frio.systemanalysisanddesign.recyclerparsers;

/**
 * Created by steven on 8/5/17.
 */

public class RParserFoodsMain {

    private Integer foodCategoryId;
    private String foodCategoryName;
    private String foodCategoryImageURL;

    public RParserFoodsMain(Integer foodCategoryId, String foodCategoryName, String foodCategoryImageURL) {
        this.foodCategoryId = foodCategoryId;
        this.foodCategoryName = foodCategoryName;
        this.foodCategoryImageURL = foodCategoryImageURL;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;

        RParserFoodsMain rParserFoodsMain = (RParserFoodsMain) obj;

        if (!foodCategoryId.equals(rParserFoodsMain.foodCategoryId))
            return false;
        return foodCategoryName.equals(rParserFoodsMain.foodCategoryName);
    }

    @Override
    public int hashCode() {
        int result = foodCategoryId.hashCode();
        result = 31 * result + foodCategoryName.hashCode();
        return result;
    }

    public Integer getFoodCategoryId() {
        return foodCategoryId;
    }

    public String getFoodCategoryName() {
        return foodCategoryName;
    }

    public String getFoodCategoryImageURL() {
        return foodCategoryImageURL;
    }
}
