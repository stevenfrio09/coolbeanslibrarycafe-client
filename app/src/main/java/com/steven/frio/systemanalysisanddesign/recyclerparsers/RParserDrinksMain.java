package com.steven.frio.systemanalysisanddesign.recyclerparsers;

/**
 * Created by steven on 8/18/17.
 */

public class RParserDrinksMain {
    private Integer drinkCategoryId;
    private String drinkCategoryName;
    private String drinkCategoryImageURL;

    public RParserDrinksMain(Integer drinkCategoryId, String drinkCategoryName, String drinkCategoryImageURL) {
        this.drinkCategoryId = drinkCategoryId;
        this.drinkCategoryName = drinkCategoryName;
        this.drinkCategoryImageURL = drinkCategoryImageURL;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;

        RParserDrinksMain rParserDrinksMain = (RParserDrinksMain) obj;

        if (!drinkCategoryId.equals(rParserDrinksMain.drinkCategoryId))
            return false;
        return drinkCategoryName.equals(rParserDrinksMain.drinkCategoryName);
    }

    @Override
    public int hashCode() {
        int result = drinkCategoryId.hashCode();
        result = 31 * result + drinkCategoryName.hashCode();
        return result;
    }

    public Integer getDrinkCategoryId() {
        return drinkCategoryId;
    }

    public String getDrinkCategoryName() {
        return drinkCategoryName;
    }

    public String getDrinkCategoryImageURL() {
        return drinkCategoryImageURL;
    }
}
