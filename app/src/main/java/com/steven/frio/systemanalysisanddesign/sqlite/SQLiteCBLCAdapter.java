package com.steven.frio.systemanalysisanddesign.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.steven.frio.systemanalysisanddesign.configs.StringConfig;

/**
 * Created by steven on 7/28/17.
 */

public class SQLiteCBLCAdapter {

    SQLiteCBLC sqLiteCBLC;
    SQLiteDatabase sqLiteDatabase;

    public SQLiteCBLCAdapter(Context context) {
        sqLiteCBLC = new SQLiteCBLC(context);
        sqLiteDatabase = sqLiteCBLC.getWritableDatabase();
    }

    public long insertData(String username, String orderName, Integer price, Integer quantity, String imgResource, String orderType, String orderId, String orderServing, String orderCategory, String paidWithPoints) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(SQLiteCBLC.COL_CUSTNAME, username);
        contentValues.put(SQLiteCBLC.COL_ORDNAME, orderName);
        contentValues.put(SQLiteCBLC.COL_PRICEBASE, price);
        contentValues.put(SQLiteCBLC.COL_PRICECURRENT, price);
        contentValues.put(SQLiteCBLC.COL_QUANTITY, quantity);
        contentValues.put(SQLiteCBLC.COL_IMGRESC, imgResource);
        contentValues.put(SQLiteCBLC.COL_ORDERTYPE, orderType);
        contentValues.put(SQLiteCBLC.COL_ORDERID, orderId);
        contentValues.put(SQLiteCBLC.COL_ORDERSERVING, orderServing);
        contentValues.put(SQLiteCBLC.COL_ORDERCATEGORY, orderCategory);
        contentValues.put(SQLiteCBLC.COL_PAID_WITH_POINTS, paidWithPoints);


        long id = sqLiteDatabase.insert(SQLiteCBLC.TABLE_NAME, null, contentValues);
        return id;

    }

    public void updateQuantity(String orderId, String newQuantity) {

        String updateQuery = "UPDATE " + SQLiteCBLC.TABLE_NAME + " SET " + SQLiteCBLC.COL_QUANTITY + " = " + newQuantity + " WHERE " + SQLiteCBLC.COL_ORDERID + " = '" + orderId + "' AND " + SQLiteCBLC.COL_PAID_WITH_POINTS + "= 'false'";
        sqLiteDatabase.execSQL(updateQuery);

    }

    public void updateCurrentPrice(String orderId, String newPrice) {

        String updateCurrentPriceQuery = "UPDATE " + SQLiteCBLC.TABLE_NAME + " SET " + SQLiteCBLC.COL_PRICECURRENT + " = " + newPrice + " WHERE " + SQLiteCBLC.COL_ORDERID + " = '" + orderId + "' AND " + SQLiteCBLC.COL_PAID_WITH_POINTS + "= 'false'";
        sqLiteDatabase.execSQL(updateCurrentPriceQuery);

    }

    public String selectBasePrice(String orderId) {


        String selectQuery = "SELECT " + SQLiteCBLC.COL_PRICEBASE + " FROM " + SQLiteCBLC.TABLE_NAME + " WHERE " + SQLiteCBLC.COL_ORDERID + " = '" + orderId + "' AND " + SQLiteCBLC.COL_PAID_WITH_POINTS + "= 'false'";

        String basePrice = "";
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            basePrice = cursor.getString(0);
            Log.d(StringConfig.LOG_TAG, "selectquery called" + basePrice);
        }

        return basePrice;
    }

    public String selectQuantity(String orderId) {

        String selectQuery = "SELECT " + SQLiteCBLC.COL_QUANTITY + " FROM " + SQLiteCBLC.TABLE_NAME + " WHERE " + SQLiteCBLC.COL_ORDERID + " = '" + orderId + "' AND " + SQLiteCBLC.COL_PAID_WITH_POINTS + "= 'false'";

        String quantity = "";
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            quantity = cursor.getString(0);
            Log.d(StringConfig.LOG_TAG, "selectquery called" + quantity);
        }

        return quantity;
    }

    public int selectCurrentPrice() {
        String selectCurrentPriceQuery = "SELECT " + SQLiteCBLC.COL_PRICECURRENT + " FROM " + SQLiteCBLC.TABLE_NAME + " WHERE " + SQLiteCBLC.COL_PAID_WITH_POINTS + " = 'false'";
        //String selectCurrentPriceQuery = "SELECT " + SQLiteCBLC.COL_PRICECURRENT + " FROM " + SQLiteCBLC.TABLE_NAME + "";

        int currentPrice = 0;
        int value = 0;
        Cursor cursor = sqLiteDatabase.rawQuery(selectCurrentPriceQuery, null);
        while (cursor.moveToNext()) {
            currentPrice = cursor.getInt(cursor.getColumnIndex(SQLiteCBLC.COL_PRICECURRENT));
            value += currentPrice;
        }

        Log.d(StringConfig.LOG_TAG, "current price : " + value);
        return value;
    }

    public void deleteOrder(String orderId) {
        String deleteOrderQuery = "DELETE FROM " + SQLiteCBLC.TABLE_NAME + " WHERE " + SQLiteCBLC.COL_ORDERID + " = '" + orderId + "' AND " + SQLiteCBLC.COL_PAID_WITH_POINTS + " = 'false'";
        sqLiteDatabase.execSQL(deleteOrderQuery);
    }

    public int countNumberOfRows() {
        String rowCount = "SELECT * FROM " + SQLiteCBLC.TABLE_NAME + " WHERE " + SQLiteCBLC.COL_PAID_WITH_POINTS + " = 'false'";

        Cursor cursor = sqLiteDatabase.rawQuery(rowCount, null);
        int value = cursor.getCount();
        cursor.close();
        Log.d(StringConfig.LOG_TAG, "countcount : " + value);
        return value;

    }


    public String selectQuantityOrderWithPointsIsTrue(String orderId) {
        String selectQuery = "SELECT " + SQLiteCBLC.COL_QUANTITY + " FROM " + SQLiteCBLC.TABLE_NAME + " WHERE " + SQLiteCBLC.COL_ORDERID + " = '" + orderId + "' AND " + SQLiteCBLC.COL_PAID_WITH_POINTS + "= 'true'";
        //String selectQuery = "SELECT " + SQLiteCBLC.COL_QUANTITY + " FROM " + SQLiteCBLC.TABLE_NAME + " WHERE " + SQLiteCBLC.COL_ORDERID + " = '" + orderId + "' AND " + SQLiteCBLC.COL_PAID_WITH_POINTS + " = 'true' ";
        String quantity = "";
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            quantity = cursor.getString(0);
            Log.d(StringConfig.LOG_TAG, "selectquery called" + quantity);
        }

        Log.d(StringConfig.LOG_TAG, "seelectQuantity is true : " + quantity);
        return quantity;
    }


    public void updateQuantityOrderWithPointsIsTrue(String orderId, String newQuantity) {

        String updateQuery = "UPDATE " + SQLiteCBLC.TABLE_NAME + " SET " + SQLiteCBLC.COL_QUANTITY + " = " + newQuantity + " WHERE " + SQLiteCBLC.COL_ORDERID + " = '" + orderId + "' AND " + SQLiteCBLC.COL_PAID_WITH_POINTS + " = 'true'";
        sqLiteDatabase.execSQL(updateQuery);

    }

    public String selectBasePriceWithPointsIsTrue(String orderId) {


        String selectQuery = "SELECT " + SQLiteCBLC.COL_PRICEBASE + " FROM " + SQLiteCBLC.TABLE_NAME + " WHERE " + SQLiteCBLC.COL_ORDERID + " = '" + orderId + "' AND " + SQLiteCBLC.COL_PAID_WITH_POINTS + " = 'true'";

        String basePrice = "";
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            basePrice = cursor.getString(0);
            Log.d(StringConfig.LOG_TAG, "selectquery called" + basePrice);
        }

        return basePrice;
    }

    public void updateCurrentPriceWithPointsIsTrue(String orderId, String newPrice) {

        String updateCurrentPriceQuery = "UPDATE " + SQLiteCBLC.TABLE_NAME + " SET " + SQLiteCBLC.COL_PRICECURRENT + " = " + newPrice + " WHERE " + SQLiteCBLC.COL_ORDERID + " = '" + orderId + "' AND " + SQLiteCBLC.COL_PAID_WITH_POINTS + " = 'true'";
        sqLiteDatabase.execSQL(updateCurrentPriceQuery);

    }

    public void deleteOrderWithPointsIsTrue(String orderId) {
        String deleteOrderQuery = "DELETE FROM " + SQLiteCBLC.TABLE_NAME + " WHERE " + SQLiteCBLC.COL_ORDERID + " = '" + orderId + "' AND " + SQLiteCBLC.COL_PAID_WITH_POINTS + " = 'true'";
        sqLiteDatabase.execSQL(deleteOrderQuery);
    }


    public static class SQLiteCBLC extends SQLiteOpenHelper {

        public static final String DATABASE_NAME = "cblc_database.db";
        public static final String TABLE_NAME = "cblc_table";
        public static final String COL_ID = "ID";
        public static final String COL_CUSTNAME = "CustomerName";
        public static final String COL_ORDERID = "OrderID";
        public static final String COL_ORDNAME = "OrderName";
        public static final String COL_PRICEBASE = "BasePrice";
        public static final String COL_PRICECURRENT = "CurrentPrice";
        public static final String COL_QUANTITY = "Quantity";
        public static final String COL_IMGRESC = "ImageResource";
        public static final String COL_ORDERTYPE = "OrderType"; // food or drink
        public static final String COL_ORDERSERVING = "OrderServing"; // half or whole; hot or cold
        public static final String COL_ORDERCATEGORY = "OrderCategory"; // panini, combo meals etc.; juice, shakes etc..
        public static final String COL_PAID_WITH_POINTS = "IsPaidWithPoints";

        //public static final String COL
        private static final int DATABASE_VERSION = 1;
        private static final String CreateQuery = "CREATE TABLE " + TABLE_NAME + "(" + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_CUSTNAME + ", " + COL_ORDNAME + ", " + COL_PRICEBASE + ", " + COL_PRICECURRENT + ", " + COL_QUANTITY + ", " + COL_IMGRESC + ", "
                + COL_ORDERID + ", " + COL_ORDERTYPE + ", " + COL_ORDERSERVING + ", " + COL_ORDERCATEGORY + ", " + COL_PAID_WITH_POINTS + ");";
        private static final String DropQuery = "DROP TABLE IF EXISTS " + TABLE_NAME + "";

        public SQLiteCBLC(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CreateQuery);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(DropQuery);
        }


    }

}
