package com.steven.frio.systemanalysisanddesign;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.steven.frio.systemanalysisanddesign.configs.StringConfig;
import com.steven.frio.systemanalysisanddesign.volley.ProfileSettings;

import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class NavigationDrawer extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);
        ButterKnife.setDebug(true);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/OpenSansCondensed-Light.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());


        setContentView(R.layout.activity_navigation_drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarNavDrawer);
        setSupportActionBar(toolbar);

        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbar);

        FragmentNavOrdering fragmentNavOrdering = new FragmentNavOrdering();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.navdrawer_container, fragmentNavOrdering);
        fragmentTransaction.commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            if (getSupportFragmentManager().getBackStackEntryCount() != 0) {
                getSupportFragmentManager().popBackStack();
            } else {
                logoutDialog();
            }

        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {


        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_ordering) {
            FragmentNavOrdering fragmentNavOrdering = new FragmentNavOrdering();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.navdrawer_container, fragmentNavOrdering);
            fragmentTransaction.commit();

            hideInputMethod();

        } else if (id == R.id.nav_cart) {
            FragmentNavCart fragmentNavCart = new FragmentNavCart();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.navdrawer_container, fragmentNavCart);
            fragmentTransaction.commit();

            hideInputMethod();

        } else if (id == R.id.nav_profile) {

            FragmentNavProfile fragmentNavProfile = new FragmentNavProfile();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.navdrawer_container, fragmentNavProfile);
            fragmentTransaction.commit();

            hideInputMethod();

        } else if (id == R.id.nav_comment_suggestions) {

            FragmentNavCommentsSuggestion fragmentNavCommentsSuggestion = new FragmentNavCommentsSuggestion();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.navdrawer_container, fragmentNavCommentsSuggestion);
            fragmentTransaction.commit();

            hideInputMethod();

        } else if (id == R.id.nav_about) {

            FragmentNavAbout fragmentNavAbout = new FragmentNavAbout();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.navdrawer_container, fragmentNavAbout);
            fragmentTransaction.commit();

            hideInputMethod();

        } else if (id == R.id.nav_logout) {

            logoutDialog();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.appbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.appbar_menu_cart:
                FragmentNavCart fragmentNavCart = new FragmentNavCart();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.navdrawer_container, fragmentNavCart);
                fragmentTransaction.commit();

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void logoutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Log-out");
        builder.setMessage("Are you sure ? This will clear all unfinalized orders.");
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                ProfileSettings profileSettings = new ProfileSettings(NavigationDrawer.this);
                profileSettings.logoutUser();

            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


    public String getFacebookPageURL() {
        PackageManager packageManager = getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            boolean isFacebookEnabled = packageManager.getApplicationInfo("com.facebook.katana", 0).enabled;
            if (isFacebookEnabled) {
                if (versionCode >= 3002580) {
                    Log.d(StringConfig.LOG_TAG, "version code : " + versionCode);
                    return "fb://facewebmodal/f?href=" + "https://facebook.com/coolbeansph/";
                } else {
                    Log.d(StringConfig.LOG_TAG, "else returned : " + versionCode);
                    return "fb://page/" + "";

                }

            } else {
                Log.d(StringConfig.LOG_TAG, "outer else returned : " + versionCode);
                return "https://facebook.com/coolbeansph/";
            }
        } catch (PackageManager.NameNotFoundException e) {
            return "https://facebook.com/coolbeansph/";
        }

    }

    public void tv_fbpage(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        String facebookPageURL = getFacebookPageURL();
        intent.setData(Uri.parse(facebookPageURL));
        startActivity(intent);
    }

    public void hideInputMethod() {

        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    //Uncomment the method below to make the custom font in effect
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
