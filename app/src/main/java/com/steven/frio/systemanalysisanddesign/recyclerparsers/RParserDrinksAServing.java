package com.steven.frio.systemanalysisanddesign.recyclerparsers;

/**
 * Created by steven on 8/16/17.
 */

public class RParserDrinksAServing {

    private Integer drinkCategoryId;
    private String drinkCategoryTemp;
    private String drinkCategoryImageURL;

    public RParserDrinksAServing(String drinkCategoryTemp, Integer drinkCategoryId, String drinkCategoryImageURL) {
        this.drinkCategoryTemp = drinkCategoryTemp;
        this.drinkCategoryId = drinkCategoryId;
        this.drinkCategoryImageURL = drinkCategoryImageURL;


    }


    public Integer getDrinkCategoryId() {
        return drinkCategoryId;
    }

    public String getDrinkCategoryTemp() {
        return drinkCategoryTemp;
    }

    public String getDrinkCategoryImageURL() {
        return drinkCategoryImageURL;
    }
}
