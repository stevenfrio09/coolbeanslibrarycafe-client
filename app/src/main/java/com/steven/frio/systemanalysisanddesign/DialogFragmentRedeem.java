package com.steven.frio.systemanalysisanddesign;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.steven.frio.systemanalysisanddesign.configs.StringConfig;
import com.steven.frio.systemanalysisanddesign.recycleradapters.RAdapterRedeemWithPoints;
import com.steven.frio.systemanalysisanddesign.recyclerparsers.RParserRedeemWithPoints;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class DialogFragmentRedeem extends DialogFragment {

    @BindView(R2.id.srl_redeem)
    SwipeRefreshLayout srl_redeem;

    @BindView(R2.id.rv_redeem)
    RecyclerView rv_redeem;

    @BindView(R2.id.et_filter)
    EditText et_filter;

    RecyclerView.LayoutManager layoutManager;


    List<RParserRedeemWithPoints> rParserRedeemWithPointsList;
    RAdapterRedeemWithPoints rAdapterRedeemWithPoints;


    RequestQueue requestQueue;


    String points;

    public DialogFragmentRedeem() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dialog_redeem, container, false);

        ButterKnife.setDebug(true);
        ButterKnife.bind(this, view);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            points = bundle.getString("points");
        }

        rParserRedeemWithPointsList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rAdapterRedeemWithPoints = new RAdapterRedeemWithPoints(getContext(), rParserRedeemWithPointsList);

        initRecyclerViews();

        srl_redeem.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                srl_redeem.setRefreshing(true);
                initRecyclerViews();
                srl_redeem.setRefreshing(false);
            }
        });


        return view;

    }


    private void initRecyclerViews() {

        rParserRedeemWithPointsList.clear();
        rv_redeem.setLayoutManager(layoutManager);
        rv_redeem.setAdapter(rAdapterRedeemWithPoints);
        loadJSONVolley();

    }

    private void loadJSONVolley() {

        requestQueue = Volley.newRequestQueue(getContext());

        String strRedeemWithPointsUrl = StringConfig.BASE_URL + StringConfig.ROUTE_REDEEM_WITH_POINTS;

        StringRequest stringRequest = new StringRequest(strRedeemWithPointsUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d(StringConfig.LOG_TAG, "responseRedeem : " + response);
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i <= jsonArray.length(); i++) {

                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        String mainId = String.valueOf(jsonObject.getInt("id"));
                        String mainName = jsonObject.getString("name");
                        int mainPrice = jsonObject.getInt("price");
                        String mainDetails = jsonObject.getString("detail");

                        JSONObject mainJsonObjectImage = jsonObject.getJSONObject("image");
                        String mainImageUrl = StringConfig.BASE_URL + mainJsonObjectImage.getString("url");

                        JSONObject jsonObjectCategory = jsonObject.getJSONObject("drink_category");
                        String categoryId = String.valueOf(jsonObjectCategory.getInt("id"));
                        String categoryName = jsonObjectCategory.getString("name");

                        JSONObject jsonObjectServing = jsonObject.getJSONObject("drink_serving_temperature");
                        String servingId = String.valueOf(jsonObjectServing.getInt("id"));
                        String servingName = jsonObjectServing.getString("name");

                        RParserRedeemWithPoints rParserRedeemWithPoints = new RParserRedeemWithPoints(Integer.parseInt(servingId), Integer.parseInt(categoryId), mainId, mainName, mainPrice, mainDetails, mainImageUrl, points);
                        rParserRedeemWithPointsList.add(rParserRedeemWithPoints);
                        rAdapterRedeemWithPoints.notifyDataSetChanged();


                    }
                } catch (JSONException e) {

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(stringRequest);

    }
}
